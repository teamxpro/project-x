'use strict';

(function (module) {
    module.controller('paymentListController', function ($scope, $location, sessionFactory, paymentService, viewbag, config, permissionFactory) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    deletePayments(records);
                    break;
            }
        }

        function onRowClick($event, payment) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    $location.path("/payment/details/" + encodeURIComponent(payment.id));
                    break;
                case 'delete':
                    deletePayments([payment]);
                    break;
            }
        }

        function deletePayments(payments) {
            if (confirm('Are you sure you want delete this ?')) {
                var selected = payments.map(function (payment) {
                    return payment.id;
                });

                paymentService.changeDeleteStatus(selected).then(function () {
                    $scope.gridAdapter.reload();
                }).catch(function (ex) {
                    console.error('Ex :' + ex);
                });
            }
        }

        $scope.init = function () {
            if (!permissionFactory.get().permissions.viewPayment) {
                $location.path('/error/403');
                return;
            }

            $scope.gridAdapter = {
                dataSource: paymentService.searchAll,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/payment/create"
            };
        };

    });
})(angular.module('project-x.payment'));