(function (module) {
    module.service('cashCollectedService', function (config, httpFactory) {

        this.getCashCollection = function (startDate, endDate) {
            var url = config.getBaseApi() + "cash-collection?startDate=" + encodeURIComponent(startDate) + "&endDate=" + encodeURIComponent(endDate);
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.cash-collected'));