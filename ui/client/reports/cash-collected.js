'use strict';

(function (module) {
    module.controller('cashCollectedController', function ($scope, $window, $timeout, $route, $templateRequest, $compile, $toaster, $location, $routeParams, viewbag, sessionFactory, cashCollectedService, permissionFactory) {
        $scope.startDate = new Date();
        $scope.endDate = new Date();
        $scope.cashCollectedList = null;

        $scope.search = function () {
            $scope.isLoadingItems = true;
            cashCollectedService.getCashCollection($scope.startDate.displayFormat(), $scope.endDate.displayFormat()).then(function (response) {
                $scope.cashCollectedList = response;
                $scope.isLoadingItems = false;
            }).catch(function (err) {
                $scope.isLoadingItems = false;
                console.log("Error in controller", err);
            });
        };        

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
                return;
            }
        };
    });
})(angular.module('project-x.cash-collected'));