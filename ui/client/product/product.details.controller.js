'use strict';

(function (module) {
    module.controller('productDetailsController', function ($scope, $route, $toaster, $location, $routeParams, sessionFactory, productService) {

        var productId = null;
        $scope.showLoading = false;
        $scope.product = {};

        function getProductDetails(productId) {
            $scope.showLoading = true;
            productService.getProductById(productId).then(function (response) {
                $scope.product = response;
                $scope.showLoading = false;
            }).catch(function (err) {
                $scope.showLoading = false;
                console.log("Error in controller", err);
            });
        }

        $scope.showMoreDetails = function (items) {
            console.log(items)
        };

        $scope.updateDeleteStatus = function () {
            var product = $scope.product;
            productService.changeDeleteStatus(product.id).then(function (result) {
                $toaster.pop({
                    message: product.code + ' Successfully Deleted',
                    type: 'success'
                });
                $scope.backToList();
            }).catch(function (ex) {
                $toaster.pop({
                    message: 'An error occured, try again',
                    type: 'error'
                });
                console.log(ex);
            });
        };

        $scope.backToList = function () {
            $location.path("/product/list");
        }

        $scope.createNew = function(){
            $location.path("/product/create");
        }

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                productId = $routeParams.productId ? decodeURIComponent($routeParams.productId) : null;
                if (productId) {
                    getProductDetails(productId);
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };

    });
})(angular.module('project-x.product'));