'use strict';

(function (module) {
    module.controller('materialCreateController', function ($scope, $route, $toaster, $timeout, lookupService, productService, sessionFactory) {
        $scope.material = {
            name: null,
            isActive: 1,
            description: null,
            validFrom: null,
            validTo: null,
            isNeverExpire: null,
        };

        $scope.save = function (material) {
            var hasValidationErrors = false;
            if (!$scope.form.$valid) {
                hasValidationErrors = true;
            }
            if (!hasValidationErrors) {
                $scope.isSaving = true;
                productService.createMaterial(material).then(function () {
                    $toaster.success('New material has bean created');
                    reset();
                    $scope.isSaving = false;
                }).catch(function (error) {
                    $toaster.error('Unable to save material information');
                    $scope.isSaving = false;
                });
            }

        }

        function reset() {
            $scope.material = {
                name: null,
                isActive: 1,
                description: null,
                validFrom: null,
                validTo: null,
                isNeverExpire: null,
            };
        }
    });
})(angular.module('project-x.product'));