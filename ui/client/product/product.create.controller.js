'use strict';

(function (module) {
    module.controller('productCreateController', function ($scope, $route, $toaster, $timeout, $location, lookupService, productService, sessionFactory) {

        /*
            product data structure
            (Do not delete this block)

        var product = {
            materialName : '2017 June Edition',
            label : '1200 STU',
            amount : 1200,
            compositeItems :[{
                label : 'A12',
                isFree : false,
                items : [{
                    item : {id:1, code:'itm-1233', name:'6 Glass set'},
                    quantity : 10
                }]
            }]
        };
        */
        $scope.isSaving = false;

        $scope.inputs = {
            materialName: null,
            label: null,
            itemId: null,
            price: 0
        };

        $scope.product = {
            materialName: null,
            label: null,
            amount: 0,
            compositeItems: []
        };

        $scope.productItem = {
            label: null,
            item: null,
            isFree: false,
            quantity: 1
        };

        $scope.command = 'add';

        var draggedIndex = null;
        var droppedIndex = null;

        function getLabelPopup() {
            return angular.element('#productItemLabalingPopup');
        }

        function showLabalingPopup() {
            $scope.labelForm.$setPristine();

            $timeout(function () {
                $scope.productItem.label = '';
                $scope.productItem.quantity = 1;

                $scope.command = 'add';

                getLabelPopup().modal('show');
            }, 100);
        };

        function setEmptyProductItem() {
            $scope.productItem = {
                label: null,
                item: null,
                isFree: false,
                quantity: 1
            };
        }

        function setEmptyInputs() {
            $scope.inputs = {
                materialName: null,
                label: null,
                itemId: null,
                price: 0
            };
        }

        $scope.itemSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return {
                            id: o.code,
                            text: o.name,
                            item: o
                        };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<div><div><small>' + data.id + '</small></div><div><strong>' + data.text + '</strong></div></div>');
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.productItem.item = event.params.data.item;
                if ($scope.productItem.item) {
                    showLabalingPopup();
                }
            }
        };

        $scope.cancelLabeling = function () {
            getLabelPopup().modal('hide');

            setEmptyProductItem();
            $scope.inputs.itemId = null;
            $scope.inputs.price = 0;

            $scope.form.itemId.$setPristine();
        };

        $scope.saveLabelInfo = function (prodcutItem) {
            if ($scope.labelForm.$valid) {
                if ($scope.command === 'add') {
                    var compositeItem = {
                        label: prodcutItem.label,
                        isFree: prodcutItem.isFree,
                        items: [{
                            item: angular.copy(prodcutItem.item),
                            quantity: prodcutItem.quantity
                        }]
                    };

                    $scope.product.compositeItems.push(compositeItem);

                } else if ($scope.command === 'merge') {
                    var dragged = $scope.product.compositeItems[draggedIndex];
                    var dropped = $scope.product.compositeItems[droppedIndex];

                    dropped.label = $scope.productItem.label;
                    dropped.quantity = $scope.productItem.quantity;
                    dropped.isFree = prodcutItem.isFree;

                    dropped.items = dropped.items.concat(dragged.items);

                    $scope.product.compositeItems.splice(draggedIndex, 1);
                }

                $scope.inputs.itemId = null;
                $scope.form.itemId.$setPristine();

                $timeout(setEmptyProductItem, 100);

                getLabelPopup().modal('hide');
                $scope.form.$setPristine();
            }
        };

        $scope.onDropItem = function (event, ui) {
            draggedIndex = ui.draggable.index();
            droppedIndex = ui.droppable.index();

            $scope.command = 'merge';

            $scope.labelForm.$setPristine();
            $timeout(setEmptyProductItem, 100);

            getLabelPopup().modal('show');
        };

        $scope.removeCompositeItem = function (compositeItem, index) {
            if ($scope.product && $scope.product.compositeItems) {
                $scope.product.compositeItems.splice(index, 1);
            }
        };

        $scope.save = function ($event, inputs) {
            if ($scope.form.$valid) {

                if ($scope.product && ($scope.product.compositeItems && $scope.product.compositeItems.length > 0)) {
                    var product = {
                        materialId: inputs.materialId,
                        label: inputs.label,
                        amount: inputs.price,
                        //type: inputs.type,
                        compositeItems: null
                    };

                    product.compositeItems = $scope.product.compositeItems.map(function (ci) {
                        return {
                            label: ci.label,
                            isFree: ci.isFree,
                            items: ci.items.map(function (i) {
                                return {
                                    id: i.item.id,
                                    code: i.item.code,
                                    quantity: i.quantity
                                }
                            })
                        };
                    });

                    $scope.isSaving = true;
                    productService.create(product).then(function (newProduct) {
                        $toaster.success('New product has bean created');
                        $scope.reset();

                        $scope.form.$setPristine();
                        $scope.form.$setUntouched();
                        $scope.isSaving = false;
                    }).catch(function (error) {
                        $toaster.error('Unable to save product information');
                        $scope.isSaving = false;
                    });

                } else {
                    $toaster.warning('Please add items before saving...');
                }
            } else {
                $toaster.warning('There are some required field missing. Please correct them and retry');
            }
        };

        $scope.reset = function () {
            $scope.product = {
                materialName: null,
                label: null,
                amount: 0,
                compositeItems: []
            };

            setEmptyProductItem();
            setEmptyInputs();
        };

        $scope.showMoreDetails = function (items) {
            console.log(items)
        };

        $scope.backToList = function () {
            $location.path("/product/list");
        }
        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {


            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.product'));
