(function (module) {
    module.service('productService', function (config, httpFactory) {

        this.create = function (product) {
            var url = config.getBaseApi() + "product/create";
            return httpFactory.post(url, product).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getProductById = function (productId) {
            var url = config.getBaseApi() + "product/" + encodeURIComponent(productId);
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.changeDeleteStatus = function (productId) {
            var url = config.getBaseApi() + "product/delete/" + encodeURIComponent(productId);
            return httpFactory.put(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAll = function (searchText, index) {
            var url = config.getBaseApi() + "products/search";
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += "?searchText=" + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.createMaterial = function (material) {
            var url = config.getBaseApi() + "material/create";
            return httpFactory.post(url, material).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.product'));