'use strict';

(function (module) {
    module.controller('inventoryDetailsController', function ($scope, $route, $toaster, $location, $routeParams, sessionFactory, inventoryService, viewbag) {

        var inventoryId = null;
        $scope.showLoading = false;

        $scope.inventoryItem = new Entity.InventoryItem();

        function getinventoryDetails(inventoryId) {
            $scope.showLoading = true;
            inventoryService.getInventoryItemById(inventoryId).then(function (response) {
                $scope.inventoryItem = response[0];;
                $scope.showLoading = false;
            }).catch(function (err) {
                $scope.showLoading = false;
                console.log("Error in controller", err);
            })
        }

        $scope.updateDeleteStatus = function (inventory) {
            inventoryService.changeDeleteStatus(inventory.id).then(function (result) {
                $toaster.pop({
                    message: inventory.code + ' Successfully Deleted',
                    type: 'success'
                });
                $scope.backToList();
            }).catch(function (ex) {
                $toaster.pop({
                    message: 'An error occured, try again',
                    type: 'error'
                });
                console.log(ex);
            });
        };

        $scope.editItem = function (inventoryItem) {
            viewbag.put('inventoryItem', inventoryItem);
            $location.path("/inventory/edit/" + encodeURIComponent(inventoryItem.inventoryId));
        };

        $scope.backToList = function () {
            $location.path("/inventory/list");
        }

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                inventoryId = $routeParams.inventoryId ? decodeURIComponent($routeParams.inventoryId) : null;
                if (inventoryId) {
                    getinventoryDetails(inventoryId);
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };

    });
})(angular.module('project-x.inventory'));