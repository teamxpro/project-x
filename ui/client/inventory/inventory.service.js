'use strict';

(function (module) {
    module.service('inventoryService', function (httpFactory, config) {

        this.getInventoryItemById = function (inventoryId) {
            var url = config.getinventoryItemByIdUrl(inventoryId);
            return httpFactory.get(url, inventoryId).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchInventoryItems = function (searchText, index, queryParams) {
            searchText = searchText ? encodeURIComponent(searchText) : null;
            var pageSize = config.getPageSize();
            var url = config.getBaseApi() + 'inventory/search?searchText=' + searchText + '&itemId=' + queryParams[0] + "&size=" + pageSize + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAvailableStock = function (searchText, index) {
            searchText = searchText ? encodeURIComponent(searchText) : null;
            var url = config.getBaseApi() + 'stock/search?searchText=' + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAvailableInventoryItems = function (searchText) {
            searchText = searchText ? encodeURIComponent(searchText) : null;
            var url = config.getBaseApi() + 'inventory/search/available?searchText=' + searchText;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.updateInventoryItem = function (item) {
            var url = config.getBaseApi() + 'inventory/update/' + item.inventoryId;
            return httpFactory.put(url, item).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

    });
})(angular.module('project-x.inventory'));