'use strict';

(function (module) {
    module.controller('inventoryLogController', function ($scope, $location, $routeParams, $window, inventoryService) {

        var itemId = null;
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onRowClick($event, inventoryItem) {
            $event.preventDefault();
            switch ($event.command) {
                case 'viewGRN':
                    resetQueryStrings();
                    $window.open('/grn/' + inventoryItem.grnId, '_blank');
                    break;
            }
        };

        $scope.init = function () {
            itemId = $routeParams.itemId ? $routeParams.itemId : null;
            $scope.gridAdapter = {
                dataSource: inventoryService.searchInventoryItems,
                onRowCommand: onRowClick,
                queryParams: [itemId]
            };
        };
    });
})(angular.module('project-x.inventory'));