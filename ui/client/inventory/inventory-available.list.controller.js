'use strict';

(function (module) {
    module.controller('inventoryAvailableListController', function ($scope, $location, sessionFactory, inventoryService, viewbag, config, permissionFactory) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onRowClick($event, itemStock) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    $location.path('/inventory/log/' + encodeURIComponent(itemStock.itemId));
                    break;
            }
        }

        $scope.init = function () {
            $scope.gridAdapter = {
                dataSource: inventoryService.searchAvailableStock,
                onRowCommand: onRowClick
            };
        };
    });
})(angular.module('project-x.inventory'));