(function (module) {
    module.controller('dashboardController', function ($scope, sessionFactory, permissionFactory) {
        $scope.visible = false;

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
                return;
            }

            if (!permissionFactory.get().permissions.customerSearch) {
                $location.path('/error/403');
                return;
            }

            $scope.visible = true;
        };
    });
})(angular.module('project-x.dashboard'));