'use strict';

(function (module) {
    module.controller('invoiceDetailsController', function ($scope, $window, $timeout, $route, $templateRequest, $compile, $toaster, $location, $routeParams, viewbag, sessionFactory, invoiceService, permissionFactory) {

        var invoiceId = null;
        $scope.showLoading = false;

        $scope.invoice = new Entity.Invoice();
        $scope.invoiceItem = new Entity.InvoiceItem();

        $scope.invoice.items = [];

        function getInvoiceDetails(invoiceId) {
            if (viewbag.isExist('invoice')) {
                $scope.invoice = viewbag.get('invoice');
                viewbag.remove('invoice');
            } else {
                $scope.showLoading = true;
                invoiceService.getInvoiceById(invoiceId).then(function (response) {
                    $scope.invoice = response.invoiceData ? response.invoiceData[0] : null;
                    $scope.invoice.items = response.invoiceItems;
                    $scope.showLoading = false;
                }).catch(function (err) {
                    $scope.showLoading = false;
                    console.log("Error in controller", err);
                });
            }
        }

        $scope.updateDeleteStatus = function (invoice) {
            invoiceService.changeDeleteStatus(invoice.id).then(function (result) {
                $toaster.pop({
                    message: invoice.code + ' Successfully Deleted',
                    type: 'success'
                });
                $scope.backToList();
            }).catch(function (ex) {
                $toaster.pop({
                    message: 'An error occured, try again',
                    type: 'error'
                });
                console.log(ex);
            });
        };

        $scope.print = function () {
            angular.element('#print').html('');
            $templateRequest("invoice/invoice.print.html").then(function (html) {
                angular.element('#print').append($compile(html)($scope));

                $timeout(function () {
                    $window.print();
                }, 500);
            });
        };

        $scope.backToList = function () {
            $location.path("/invoice/list");
        }

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                if (!permissionFactory.get().permissions.viewInvoice) {
                    $location.path('/error/403');
                    return;
                }

                invoiceId = $routeParams.invoiceId ? decodeURIComponent($routeParams.invoiceId) : null;
                if (invoiceId) {
                    getInvoiceDetails(invoiceId);
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };

    });
})(angular.module('project-x.invoice'));