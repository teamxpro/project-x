(function (module) {
    module.service('invoiceService', function (config, httpFactory) {

        this.create = function (invoice) {
            var url = config.getBaseApi() + "invoice/create";
            return httpFactory.post(url, invoice).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getInvoiceById = function (invoiceId) {
            var url = config.getBaseApi() + "invoice/" + encodeURIComponent(invoiceId);
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.changeDeleteStatus = function (invoiceId) {
            var url = config.getBaseApi() + "invoice/delete/" + encodeURIComponent(invoiceId);
            return httpFactory.put(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchDueInvoices = function (searchText, index) {
            var url = config.getBaseApi() + "invoices/due/search";
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += "?searchText=" + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAll = function (searchText, index) {
            var url = config.getBaseApi() + "invoices/search";
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += "?searchText=" + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.invoice'));