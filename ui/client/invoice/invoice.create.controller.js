'use strict';

(function (module) {
    module.controller('invoiceCreateController', function ($scope, $route, $toaster, $window, $templateRequest, $timeout, $compile, $location, $filter, lookupService, invoiceService, sessionFactory, permissionFactory) {

        $scope.showLoading = false;
        $scope.errorMessage = null;

        $scope.salesTypes = [];
        $scope.paymentTypes = [];

        $scope.invoice = new Entity.Invoice();
        $scope.invoice.items = [];

        function showSuccessToaster(message) {
            $toaster.pop({
                message: message,
                type: 'success'
            });
        }

        function showErrorToaster(message) {
            $toaster.pop({
                message: message,
                type: 'error'
            });
        }

        function ajustFinalAmounts() {
            $scope.invoice.total = $scope.invoice.items.reduce(function (prev, next) {
                return Number(prev) + Number(next.subTotal);
            }, 0);
            $scope.invoice.netAmount = $scope.invoice.total - $scope.invoice.discountAmount;
            var balanceAmount = $scope.invoice.receivedAmount - $scope.invoice.netAmount;
            if (balanceAmount >= 0) {
                $scope.invoice.balanceAmount = balanceAmount;
            } else {
                $scope.invoice.balanceAmount = 0;
            }
        }

        function fillSalesTypes() {
            $scope.salesTypes = lookupService.getLookup('salesType');
            $scope.selectedSalesType = $scope.salesTypes[0];
            $scope.invoice.salesTypeId = $scope.salesTypes[0].key;
            $scope.invoice.salesTypeName = $scope.salesTypes[0].value;
        }

        function fillPaymentTypes() {
            $scope.paymentTypes = lookupService.getLookup('paymentMethod');
            $scope.selectedPayType = $scope.paymentTypes[0];
            $scope.invoice.paymentMethodId = $scope.paymentTypes[0].key;
            $scope.invoice.paymentMethodName = $scope.paymentTypes[0].value;
        }

        function resetInvoiceAmounts() {
            $scope.invoice.balanceAmount = 0;
            $scope.invoice.receivedAmount = 0;
            $scope.invoice.discountAmount = 0;
        }

        function print() {
            angular.element('#print').html('');
            $templateRequest("invoice/invoice.print.html").then(function (html) {
                angular.element('#print').append($compile(html)($scope));
                $timeout(function () {
                    $route.reload();
                    $window.print();
                }, 500);
            });
        };

        function validateInvoice(invoice) {
            if (invoice.items.length === 0) {
                $toaster.pop({
                    message: 'Select an Item.',
                    type: 'warning'
                });
                return false;
            }

            if (invoice.paymentMethodName === 'Cash' && invoice.receivedAmount < invoice.netAmount) {
                return false;
            }

            if (invoice.discountAmount > invoice.total) {
                return false;
            }
            return true;
        }

        function addItem(inventory) {
            var invoiceItem = new Entity.InvoiceItem();

            var item = $scope.invoice.items.filter(function (obj) {
                return obj.inventoryId === inventory.inventoryId;
            })[0];

            //check whether the item is already in the invoice items
            if (item) {
                return;
            }

            //if selected item has no quantity
            if (inventory.quantity === 0) {
                return;
            }

            invoiceItem.inventoryId = inventory.inventoryId;
            invoiceItem.itemCode = inventory.itemCode;
            invoiceItem.inventoryCode = inventory.inventoryCode;
            invoiceItem.itemName = inventory.itemname;
            invoiceItem.availableQuantity = inventory.quantity;
            invoiceItem.quantity = 1; //default quantity

            //price
            if ($scope.invoice.salesTypeName === 'Retail') {//retail 1
                invoiceItem.price = inventory.retailSellingPrice;
            } else if ($scope.invoice.salesTypeName === 'Whole Sale') { //Whole Sale 3
                invoiceItem.price = inventory.wholeSellingPrice;
            } else {
                invoiceItem.price = inventory.retailSellingPrice;
            }
            invoiceItem.subTotal = invoiceItem.price * invoiceItem.quantity;

            $scope.invoice.items.push(invoiceItem);
            ajustFinalAmounts();
            invoiceItem = new Entity.InvoiceItem();
        }

        $scope.setSelectedSalesType = function () {
            $scope.invoice.salesTypeId = $scope.selectedSalesType.key;
            $scope.invoice.salesTypeName = $scope.selectedSalesType.value;
        };

        $scope.setSelectedPayType = function () {
            $scope.invoice.paymentMethodId = $scope.selectedPayType.key;
            $scope.invoice.paymentMethodName = $scope.selectedPayType.value;

            if ($scope.selectedPayType.value !== 'Cheque') {
                $scope.invoice.balanceAmount = 0;
                $scope.invoice.receivedAmount = 0;
            }
        };

        $scope.calculateSubTotal = function (item) {
            var itemElement = angular.element('#item' + item.inventoryId);
            var quantity = itemElement.val() ? Number(itemElement.val()) : 0;
            if (item.availableQuantity < quantity) {
                item.quantity = 1;
            }
            item.subTotal = item.price * item.quantity;
            ajustFinalAmounts();
        };

        $scope.ajustDiscountValues = function () {
            if ($scope.invoice.discountAmount <= $scope.invoice.total) {
                $scope.invoice.netAmount = $scope.invoice.total - $scope.invoice.discountAmount;
            }
        };

        $scope.ajustBalanceAmount = function () {
            var balanceAmount = $scope.invoice.receivedAmount - $scope.invoice.netAmount;
            //update balance and due amount in invoice
            if (balanceAmount >= 0) {
                $scope.invoice.dueAmount = 0;
                $scope.invoice.balanceAmount = balanceAmount;
            } else {
                $scope.invoice.dueAmount = $scope.invoice.netAmount - $scope.invoice.receivedAmount;
                $scope.invoice.balanceAmount = 0;
            }
        };

        $scope.removeItem = function ($event, index) {
            $event.preventDefault();
            $scope.invoice.items.splice(index, 1);
            resetInvoiceAmounts();
            ajustFinalAmounts();
        };

        $scope.saveChanges = function (invoice, doPrint) {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            if (!$scope.form.$valid || !validateInvoice(invoice)) {
                $scope.form.$setPristine();
                hasValidationErrors = true;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;
                invoice.isActive = 1;

                invoiceService.create(invoice).then(function (response) {
                    if (response.success) {
                        $scope.invoice.code = response.invoiceCode;
                        $scope.invoice.id = response.invoiceId;

                        if (doPrint) {
                            print();
                        } else {
                            $toaster.pop({
                                message: 'Invoice has been successfully saved',
                                type: 'success'
                            });
                            $timeout(function () {
                                $route.reload();
                            });
                        }
                    } else {
                        $scope.errorMessage = response.message;
                    }
                    $scope.showLoading = false;
                }).catch(function (ex) {
                    $toaster.pop({
                        message: 'Invoice has not being saved. Try again',
                        type: 'error'
                    });
                    $scope.showLoading = false;
                });
            }
        };

        $scope.customerSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.name, code: o.customerId, nic: o.nic };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    var phoneNumber = data.mobile1 || data. mobile2 || data.fixedLine;

                    var template = '<div class="dropdown-item-card">' 
                        + '<div class="secondry-text">' + data.code +'</div>'
                        + '<div class="primary-text">' + data.text + '</div>'
                        +  (data.nic ? ('<div>' + data.nic + '</div>') : '')
                        +  (phoneNumber ? ('<div class="hover-text">' + phoneNumber + '</div>') : '')
                        +'</div>';

                    return $(template);
                }

                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.$apply(function () {
                    $scope.invoice.customerName = event.params.data.text;
                });
            }
        };

        $scope.inventorySearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.itemCode, text: o.itemname, inventory: o, disabled: o.quantity === 0 };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                
                if (!data.loading) {
                    var isOutOfStock = data.inventory.quantity === 0;
                    var price = data.inventory.retailSellingPrice;

                    if ($scope.invoice.salesTypeName === 'Whole Sale') { //Whole Sale 3
                        price = data.inventory.wholeSellingPrice;
                    }

                    var template = '<div class="dropdown-item-card' + (isOutOfStock ? ' danger disabled' : '') + '">' 
                        + '<div class="secondry-text">' + data.inventory.inventoryCode +'</div>'
                        + '<div class="primary-text">' + data.inventory.itemname + '</div>'
                        + '<div>1 x ' + $filter('currency')(price,'Rs.') + '</div>'
                        + '<div class="hover-text">'+ data.inventory.quantity + ' available</div>'
                        +'</div>';

                    return $(template);
                }

                return 'Please wait..';
            },
            onSelect: function (event) {
                console.log(event);
                angular.element(event.target).val('').trigger('change');
                angular.element(event.target).select2('close');
                
                $scope.$apply(function () {
                    addItem(event.params.data.inventory);
                    $scope.selectedInventoryItem = null;
                });
            }
        };

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                if (!permissionFactory.get().permissions.addInvoice) {
                    $location.path('/error/403');
                    return;
                }

                fillSalesTypes();
                fillPaymentTypes();
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.invoice'));