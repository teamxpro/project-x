(function (module) {
    module.directive('itemSearchAutocomplete', function ($filter, httpFactory) {
        return {
            restrict: 'A',
            link: function ($scope, $element, $attr) {
                $element.autocomplete({
                    source: function (req, res) {
                        httpFactory.get('/api/inventory/search/available', { params: { searchText: req.term, index: 1, size: 100 } }).then(function (serverResponse) {
                            res(serverResponse.data.records);
                        }).catch(function (ex) {
                            res(null);
                        })
                    },
                    select : function(event, ui){
                        console.log(ui);
                    },
                    minLength: 1
                })
                .autocomplete( "instance" )
                ._renderItem = function(ul, item) {
                    var isOutOfStock = item.quantity === 0;
                    var price = item.retailSellingPrice;

                    /*if ($scope.invoice.salesTypeName === 'Whole Sale') { //Whole Sale 3
                        price = data.inventory.wholeSellingPrice;
                    }*.*/

                    var template = '<li><div class="dropdown-item-card' + (isOutOfStock ? ' danger disabled' : '') + '">' 
                    + '<div class="secondry-text">' + item.inventoryCode +'</div>'
                    + '<div class="primary-text">' + item.itemname + '</div>'
                    + '<div>1 x ' + $filter('currency')(price,'Rs.') + '</div>'
                    + '<div class="hover-text">'+ item.quantity + ' available</div>'
                    +'</div></li>';

                    return $(template).appendTo(ul);
                };
            }
        }
    });
})(angular.module('project-x.invoice'));