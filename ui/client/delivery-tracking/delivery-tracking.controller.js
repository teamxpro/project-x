'use strict';

(function (module) {
    module.controller('deliveryTrackingController', function ($scope, $routeParams, $toaster, sessionFactory, itemIssueService, permissionFactory) {

        var loadingSheetId = null;
        var selectedItemList = [];

        $scope.itemList = [];
        $scope.showNoResultMassage = false;
        $scope.deliveryStatus = null;
        $scope.changedStatus = null;

        function showSuccessToaster(message) {
            $toaster.pop({
                message: message,
                type: 'success'
            });
        }

        function showErrorToaster(message) {
            $toaster.pop({
                message: message,
                type: 'error'
            });
        }

        $scope.getItemListByTackingStatus = function () {
            $scope.showNoResultMassage = false;
            $scope.itemList.length = 0; //empty the list
            $scope.isLoadingItems = true;
            itemIssueService.getItemListByTackingStatus(0, null, loadingSheetId).then(function (response) {
                $scope.itemList = response;
                if ($scope.itemList.length === 0) {
                    $scope.showNoResultMassage = true;
                } else {
                    angular.forEach($scope.itemList, function (item) {
                        item.isSelected = item.isSelected === 1;
                    });
                    $scope.showNoResultMassage = false;
                }
                $scope.isLoadingItems = false;
            }).catch(function (err) {
                $scope.isLoadingItems = false;
                console.log("Error in controller", err);
            });
        };

        $scope.saveChanges = function () {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            selectedItemList.length === 0;
            $scope.itemList.forEach(function (item) {
                if (item.isSelected && item.status === $scope.deliveryStatus) {
                    item.orderStatus = parseInt($scope.changedStatus);
                    selectedItemList.push(item);
                }
            });

            if (selectedItemList.length === 0) {
                return;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;
                itemIssueService.updateItemTrackingStatus(selectedItemList).then(function (response) {
                    if (response.success) {
                        showSuccessToaster('Record(s) have been successfully updated.');
                        $scope.getItemListByTackingStatus();
                    } else {
                        $scope.errorMessage = response.message;
                    }
                    $scope.showLoading = false;
                }).catch(function (ex) {
                    showErrorToaster('Something went wrong. Please try again.');
                    $scope.showLoading = false;
                });
            }
        };

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
            } else {
                loadingSheetId = $routeParams.loadingSheetId;
                $scope.getItemListByTackingStatus();
            }
        };
    });
})(angular.module('project-x.tracking'));
