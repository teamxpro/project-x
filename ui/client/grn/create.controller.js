'use strict';

(function (module) {
    module.controller('grnCreateController', function ($scope, $routeParams, $route, $toaster, $location, itemService, supplierService, grnService, lookupService, sessionFactory, permissionFactory) {
        $scope.isEditing = false;
        $scope.isDetails = false;
        $scope.pageTitle = 'New';
        var availableQuantity = null;
        var selectedIndex = null;
        var grnId = null;

        $scope.showLoading = false;
        $scope.errorMessage = null;
        $scope.showSaveButton = false;
        $scope.toggleSaveModify = true;

        $scope.items = [];
        $scope.suppliers = [];
        $scope.paymentTypes = [];

        $scope.grn = new Entity.GRN();
        $scope.grn.inventoryItems = [];
        var deletedItems = [];
        $scope.inventoryItem = new Entity.InventoryItem();

        var intNumPatttern = /^[0-9]+$/;
        var floatingNumPattern = /^[0-9]*\.?[0-9]+$/;

        function getSuppliers() {
            supplierService.getAllSuppliers().then(function (results) {
                if (results) {
                    $scope.suppliers = results[0];
                }
            }).catch(function (exception) {
                console.log(exception);
            });
        }

        function fillPaymentTypes() {
            $scope.paymentTypes = lookupService.getLookup('paymentMethod');
            $scope.grn.paymentMethodId = $scope.paymentTypes[0].key;
            $scope.grn.paymentMethodName = $scope.paymentTypes[0].value;
        }

        function getItemsList() {
            $scope.items = [];
            itemService.getAllItems().then(function (results) {
                if (results) {
                    $scope.items = results;
                }
            }).catch(function (exception) {
                console.log(exception);
            });
        }

        function validateInventoryItem() {
            if ($scope.inventoryItem.itemId == undefined) {
                $toaster.pop({
                    message: 'Select an Item.',
                    type: 'warning'
                });
                return false;
            }
            if (!intNumPatttern.test($scope.inventoryItem.quantity)) {
                $toaster.pop({
                    message: 'Invalid Quatitity.',
                    type: 'warning'
                });
                return false;
            }
            if (!floatingNumPattern.test($scope.inventoryItem.buyingPrice)) {
                $toaster.pop({
                    message: 'Invalid Buying Price.',
                    type: 'warning'
                });
                return false;
            }
            if (!floatingNumPattern.test($scope.inventoryItem.retailSellingPrice)) {
                $toaster.pop({
                    message: 'Invalid Retail Price.',
                    type: 'warning'
                });
                return false;
            }
            if (!floatingNumPattern.test($scope.inventoryItem.wholeSellingPrice)) {
                $toaster.pop({
                    message: 'Invalid Wholesale Price.',
                    type: 'warning'
                });
                return false;
            }
            if (!floatingNumPattern.test($scope.inventoryItem.easyPaymentSellingPrice)) {
                $toaster.pop({
                    message: 'Invalid Easy Pay Price.',
                    type: 'warning'
                });
                return false;
            }
            if (!floatingNumPattern.test($scope.inventoryItem.customPrice)) {
                $toaster.pop({
                    message: 'Invalid Custom Price.',
                    type: 'warning'
                });
                return false;
            }

            return true;
        }

        function validateGRN() {
            if (!$scope.grn.grnCode) {
                $toaster.pop({
                    message: 'Invalid GRN Code.',
                    type: 'warning'
                });
                return false;
            }
            if ($scope.grn.supplier == undefined) {
                $toaster.pop({
                    message: 'Select Supplier.',
                    type: 'warning'
                });
                return false;
            }
            if (!$scope.grn.receivedDate) {
                $toaster.pop({
                    message: 'Invalid Date.',
                    type: 'warning'
                });
                return false;
            }
            if ($scope.grn.paymentMethodName == undefined) {
                $toaster.pop({
                    message: 'Select Payment Method.',
                    type: 'warning'
                });
                return false;
            }
            else if ($scope.grn.paymentMethodName == 'Cheque') {
                if (!$scope.grn.chequeNo) {
                    $toaster.pop({
                        message: 'Invalid cheque no.',
                        type: 'warning'
                    });
                    return false;
                }
                if (!$scope.grn.bank) {
                    $toaster.pop({
                        message: 'Invalid bank details.',
                        type: 'warning'
                    });
                    return false;
                }
                if (!$scope.grn.branch) {
                    $toaster.pop({
                        message: 'Invalid branch details.',
                        type: 'warning'
                    });
                    return false;
                }
            }

            if ($scope.grn.inventoryItems.length == 0) {
                $toaster.pop({
                    message: 'No Inventory items added.',
                    type: 'warning'
                });
                return false;
            }
            if ($scope.grn.discountAmount && !floatingNumPattern.test($scope.grn.discountAmount)) {
                $toaster.pop({
                    message: 'Invalid Discount Amount.',
                    type: 'warning'
                });
                return false;
            }

            return true;
        }

        $scope.createNew = function () {
            $route.reload();
        };

        $scope.setSelectedSupplier = function ($event, supplier) {
            $event.preventDefault();
            $scope.grn.supplier = supplier.id;
            $scope.grn.supplierName = supplier.name;
        };

        $scope.setSelectedPayType = function ($event, type) {
            $event.preventDefault();
            $scope.grn.paymentMethod = type.key;
            $scope.grn.paymentMethodName = type.value;
        };

        $scope.calculateTotal = function () {
            var total = 0;
            $scope.grn.inventoryItems.forEach(function (item) {
                total += item.buyingPrice * item.quantity;
            });
            $scope.grn.total = total;

            if (isNaN(parseFloat($scope.grn.discountAmount))) {
                $scope.grn.netAmount = $scope.grn.total;
            }
            else {
                $scope.grn.netAmount = $scope.grn.total - parseFloat($scope.grn.discountAmount);
            }
        }

        $scope.selectItem = function ($event, item, index) {
            $scope.inventoryItem = angular.copy(item);
            selectedIndex = index;
            $scope.toggleSaveModify = false;
        };

        $scope.removeItem = function ($event, index) {
            $event.preventDefault();

            //if existing inventory item add it to delete list
            if ($scope.grn.inventoryItems[index].inventoryId) {
                deletedItems.push($scope.grn.inventoryItems[index].inventoryId);
            }

            $scope.grn.inventoryItems.splice(index, 1);
            $scope.calculateTotal();
        };

        $scope.addItem = function () {
            if (validateInventoryItem()) {
                $scope.grn.inventoryItems.push($scope.inventoryItem);
                $scope.inventoryItem = new Entity.InventoryItem();
                $scope.calculateTotal();

                angular.element('#itemSearchSelect').val(null).trigger('change');
            }
        };

        $scope.modifyItem = function () {
            if (validateInventoryItem()) {
                $scope.isEditing = true;
                $scope.inventoryItem.isDirty = true;
                $scope.grn.inventoryItems[selectedIndex] = $scope.inventoryItem;
                $scope.toggleSaveModify = true;
                $scope.inventoryItem = new Entity.InventoryItem();
                $scope.calculateTotal();
            }
        };

        $scope.backToList = function ($event) {
            $event.preventDefault();
            $location.url('/grn/list');
        };

        $scope.createNew = function () {
            $route.reload();
        };

        $scope.saveChanges = function (grn) {
            $scope.errorMessage = null;
            $scope.grn.deletedItems = deletedItems;

            if (validateGRN()) {
                $scope.showLoading = true;
                if ($scope.isEditing) {
                    grnService.updateGRN(grn).then(function (response) {
                        if (response && response.success) {
                            $toaster.pop({
                                message: 'Successfully saved',
                                type: 'success'
                            });
                        } else {
                            $scope.errorMessage = "Error saving GRN";
                        }
                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        console.log("Error in controller", ex);
                        $scope.showLoading = false;
                    });
                }
                else {
                    grnService.createGRN(grn).then(function (response) {
                        if (response && response.success) {
                            $toaster.pop({
                                message: 'Successfully saved',
                                type: 'success'
                            });
                        } else {
                            $scope.errorMessage = "Error saving GRN";
                        }
                        $scope.showLoading = false;
                        $location.path('/grn/list');
                    }).catch(function (ex) {
                        console.log("Error in controller", ex);
                        $scope.showLoading = false;
                    });
                }
            }
        };

        function setSlectedItem(item) {
            $scope.inventoryItem.itemId = item.id;
            $scope.inventoryItem.item = item.text;
        };

        $scope.itemSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, code: o.code, text: o.name };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.code + ' - <strong>' + data.text + '</strong>');
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                setSlectedItem(event.params.data);
            }
        };

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                //check permission for create GRN
                if ($location.absUrl().indexOf('create') > 0 && !permissionFactory.get().permissions.addGRN) {
                    $location.path('/error/403');
                    return;
                }

                getItemsList();
                getSuppliers();
                fillPaymentTypes();
                $scope.showSaveButton = true;

                grnId = $routeParams.grnId ? decodeURIComponent($routeParams.grnId) : null;
                if (grnId) {
                    if (!permissionFactory.get().permissions.viewGRN) {
                        $location.path('/error/403');
                        return;
                    }

                    $scope.isDetails = true;
                    $scope.showSaveButton = false;
                    $scope.pageTitle = 'Details';

                    $scope.showLoading = true;
                    grnService.getGRNDetailsById(grnId).then(function (result) {
                        if (result && result !== 'undefined') {
                            $scope.grn = result.grnData[0];
                            $scope.grn.receivedDate = new Date($scope.grn.receivedDate);
                            $scope.grn.inventoryItems = result.inventoryItems;
                            $scope.calculateTotal();
                        } else {
                            $scope.backToList();
                        }
                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        console.log(ex);
                        $scope.showLoading = false;
                    });
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.grn'));