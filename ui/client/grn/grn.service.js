'use strict';

(function (module) {
    module.service('grnService', function (httpFactory, config) {

        this.createGRN = function (grn) {
            var url = config.getBaseApi() + 'grn/create';
            return httpFactory.post(url, grn).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getGRNDetailsById = function (grnId) {
            var url = config.getBaseApi() + 'grn/' + grnId;
            return httpFactory.get(url, grnId).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchGRNItems = function (searchText, index) {
            searchText = searchText ? encodeURIComponent(searchText) : null;
            var url = config.getBaseApi() + 'grn/search';
            url += '?searchText=' + searchText + '&size=' + config.getPageSize() + '&index=' + index;
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.updateGRN = function (grn) {
            var url = config.getBaseApi() + 'grn/update/' + encodeURIComponent(grn.id);
            return httpFactory.put(url, grn).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

    });
})(angular.module('project-x.grn'));