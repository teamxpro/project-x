(function (module) {
    module.service('loginService', function (httpFactory) {
        this.login = function (store, username, password) {
            return httpFactory.post('/authentication', {
                store: store,
                username: username,
                password: password
            }).then(function (response) {
                return {
                    username: response.data.username,
                    token: response.data.token,
                    userId: response.data.userId,
                    success: response.data.success,
                    message: response.data.message
                };
            }).catch(function (exception) {
                console.log(exception);
            });
        }
    });
})(angular.module('project-x.login'));
