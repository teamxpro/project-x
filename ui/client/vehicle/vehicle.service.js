'use strict';

(function (module) {
    module.service('vehicleService', function (httpFactory, httpExceptionFactory, config) {

        this.create = function (vehicle) {
            var url = config.getBaseApi() + "vehicle/create";
            return httpFactory.post(url, vehicle).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.getByVehicleId = function (vehicleId) {
            var url = config.getBaseApi() + "vehicle/" + encodeURIComponent(vehicleId);
            return httpFactory.get(url).then(function (response) {
                return (response.data && response.data.length > 0) ? response.data[0] : null;
            }).catch(handleException);
        };

        this.changeActiveStatus = function (vehicleId, isActive) {
            var url = config.getBaseApi() + "vehicle/status/" + encodeURIComponent(vehicleId);
            return httpFactory.put(url, { isActive: isActive }).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.changeDeleteStatus = function (vehicleId) {
            var url = config.getBaseApi() + "vehicle/delete/" + encodeURIComponent(vehicleId);
            return httpFactory.put(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.update = function (vehicle) {
            var url = config.getBaseApi() + "vehicle/update/" + encodeURIComponent(vehicle.id);
            return httpFactory.put(url, vehicle).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.searchAll = function (searchText, index) {
            var url = config.getBaseApi() + "vehicles/search";
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += "?searchText=" + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.recentlyCreated = function (size) {
            var url = config.getBaseApi() + "vehicles/recently-created?size=" + size;
            
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        function handleException(ex) {
            switch (ex && ex.status) {
                case 401:
                    httpExceptionFactory.unauthorized(ex);
                    break;

                case 403:
                    httpExceptionFactory.forbidden(ex);
                    break;

                case 500:
                    httpExceptionFactory.internalServerError(ex);
                    break;

                default:
                    httpExceptionFactory.unknownError(ex);
                    break;
            }
        }
    });
})(angular.module('project-x.vehicle'));