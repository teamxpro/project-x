'use strict';

(function (module) {
    module.controller('issueListController', function ($scope, $location, sessionFactory, itemIssueService, viewbag, config) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    //deleteInventoryIssues(records);
                    break;
            }
        }

        function onRowClick($event, issue) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    $location.path("/item-issue/details/" + issue.id);
                    break;
                case 'delete':
                    //deleteInventoryIssues([issue]);
                    break;
            }
        }

        $scope.init = function () {
            $scope.gridAdapter = {
                dataSource: itemIssueService.searchAll,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/item-issue/create"
            };
        };

    });
})(angular.module('project-x.item-issue'));