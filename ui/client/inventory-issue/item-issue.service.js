(function (module) {
    module.service('itemIssueService', function (config, httpFactory) {

        this.create = function (issue) {
            var url = config.getBaseApi() + 'inventory-issue/create';
            return httpFactory.post(url, issue).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getInventoryIssueById = function (issueId) {
            var url = config.getBaseApi() + 'inventory-issue/' + encodeURIComponent(issueId);
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.saveLoadingSheet = function (loadingSheet) {
            var url = config.getBaseApi() + 'inventory-issue/loading-sheet';
            return httpFactory.post(url, loadingSheet).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.updateItemTrackingStatus = function (items) {
            var url = config.getBaseApi() + 'inventory-issue/status';
            return httpFactory.put(url, items).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getItemListByTackingStatus = function (status, lineId, loadingSheetId) {
            var url = config.getBaseApi() + 'inventory-issue/list/' + status + '?lineId=' + lineId + '&loadingSheetId=' + loadingSheetId;
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAll = function (searchText, index) {
            var url = config.getBaseApi() + 'inventory-issues/search';
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += '?searchText=' + searchText + '&size=' + config.getPageSize() + '&index=' + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getAggregateItemListByTackingStatus = function (status, lineId) {
            var url = config.getBaseApi() + 'inventory-issue/items/list/' + status + '?lineId=' + lineId;
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getLoadingSheet = function (loadingDate, lineId) {
            var url = config.getBaseApi() + 'inventory-issue/loading-sheet-history/?loadingDate=' + loadingDate + '&lineId=' + lineId;
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.item-issue'));
