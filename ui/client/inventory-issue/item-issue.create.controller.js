'use strict';

(function (module) {
    module.controller('issueCreateController', function ($scope, $route, $toaster, $timeout, itemIssueService, sessionFactory) {

        $scope.issueItem = new Entity.IssuedItem();

        $scope.showLoading = false;
        $scope.errorMessage = null;

        $scope.issue = new Entity.InventoryIssue();
        $scope.issue.items = [];

        function showSuccessToaster(message) {
            $toaster.pop({
                message: message,
                type: 'success'
            });
        }

        function showErrorToaster(message) {
            $toaster.pop({
                message: message,
                type: 'error'
            });
        }

        function validateIssue(issue) {
            if (issue.items.length === 0) {
                return false;
            }
            return true;
        }

        $scope.addItem = function () {
            if ($scope.issueItem.inventoryId) {
                $scope.issue.items.push($scope.issueItem);
                $scope.issueItem = new Entity.IssuedItem();
                $scope.selectedInventoryItem = null;
            }
        }

        $scope.removeItem = function ($event, index) {
            $event.preventDefault();
            $scope.issue.items.splice(index, 1);
        };

        $scope.saveChanges = function (issue) {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            if (!$scope.form.$valid || !validateIssue(issue)) {
                $scope.form.$setPristine();
                hasValidationErrors = true;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;
                issue.isActive = 1;

                itemIssueService.create(issue).then(function (response) {
                    if (response.success) {
                        $scope.issue.code = response.issueCode;
                        $scope.issue.id = response.issueId;
                        showSuccessToaster('Item Issue has been successfully saved.');
                    } else {
                        $scope.errorMessage = response.message;
                    }
                    $scope.showLoading = false;
                }).catch(function (ex) {
                    showErrorToaster('Item Issue has not being saved. Try again.');
                    $scope.showLoading = false;
                });
            }
        };

        $scope.empoyeeSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.name, code: o.employeeId };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.code + ' - <strong>' + data.text + '</strong></span> - \t');
                }
                return 'Please wait..';
            }
        };

        $scope.inventorySearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.itemCode, text: o.itemname, inventory: o };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.id + ' - <strong>' + data.text + '</strong> - <i> ' + (data.inventory.quantity === 0 ? 'Out of stock' : data.inventory.quantity) + '</i></span > - \t');
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.$apply(function () {
                    $scope.issueItem = new Entity.IssuedItem();
                    var inventory = event.params.data.inventory;

                    var item = $scope.issue.items.filter(function (obj) {
                        return obj.inventoryId === inventory.inventoryId;
                    })[0];

                    //check whether the item is already in the issue items
                    if (item) {
                        return;
                    }

                    //if selected item has no quantity
                    if (inventory.quantity === 0) {
                        return;
                    }

                    $scope.issueItem.inventoryId = inventory.inventoryId;
                    $scope.issueItem.itemCode = inventory.itemCode;
                    $scope.issueItem.inventoryCode = inventory.inventoryCode;
                    $scope.issueItem.itemName = inventory.itemname;
                    $scope.issueItem.availableQuantity = inventory.quantity;
                    $scope.issueItem.quantity = 1; //default quantity
                });
            }
        };

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.item-issue'));