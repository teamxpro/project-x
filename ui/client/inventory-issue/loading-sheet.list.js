'use strict';

(function (module) {
    module.controller('showLoadingSheetController', function ($scope, $location, sessionFactory, itemIssueService, viewbag, config) {
        $scope.gridAdapter = null;
        $scope.items = null;
        $scope.showLoading = false;

        $scope.showLoadingSheet = function () {
            $scope.showLoading = true;
            var loadingDate = null;
            if ($scope.loadingDate) {
                var dateString = new Date($scope.loadingDate);
                var loadingDate = dateString.getFullYear() + '-' + ("0" + (dateString.getMonth() + 1)).slice(-2) + '-' + ("0" + dateString.getDate()).slice(-2);
                
            }
            var lineId = $scope.lineId;
            itemIssueService.getLoadingSheet(loadingDate, lineId).then(function (result) {
                $scope.items = result.records;
                $scope.showLoading = false;
            }).catch(function (ex) {
                console.error('Ex :' + ex);
                $scope.showLoading = false;
            });

        }

        $scope.redirectToDeliveryTracking = function($event, id){
            $event.preventDefault()
            $location.path('/delivery/tracking/' + id);
        }

        $scope.resetSearchResults = function(){
            $scope.gridAdapter = null;
            $scope.items = null;
            $scope.showLoading = false;
            $scope.loadingDate = null;
            $scope.lineId = null;
        }

        $scope.linesSearchConfig = {
            processResults: function (data, params, displaySize) {
                var items = data.records || [];
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return {
                            id: o.id,
                            text: o.route,
                            line: o
                        };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.line.code + ' - ' + data.text + '</span> - \t');
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.$apply(function () {
                    $scope.lineId = event.params.data.id;
                    $scope.lineRoute = event.params.data.text;
                    $scope.lineDate = event.params.data.line.lineDate;
                });
            }
        };

        $scope.init = function () {
            //$scope.loadingDate = new Date();
            $scope.showLoadingSheet();
        };
    })
})(angular.module('project-x.item-issue'));