'use strict';

(function (module) {
    module.controller('itemLoadingController', function ($scope, $window, $timeout, $route, $templateRequest, $compile, $toaster, $location, $routeParams, viewbag, sessionFactory, itemIssueService, permissionFactory) {

        $scope.loadingSheet = new Entity.LoadingSheet();
        $scope.loadingDate = new Date();
        $scope.loadingList = [];
        $scope.selectedLoadingList = [];
        $scope.aggregatedItems = [];
        $scope.loadingItem = new Entity.LoadingItem();

        $scope.isLoadingSheetGenerated = false;

        $scope.itemOrderAsStatuses = [
            'Default', 'Free', 'Sample', 'Change'
        ];

        function showSuccessToaster(message) {
            $toaster.pop({
                message: message,
                type: 'success'
            });
        }

        function showErrorToaster(message) {
            $toaster.pop({
                message: message,
                type: 'error'
            });
        }

        function validateloadingSheet(loadingItems) {
            if (loadingItems.length === 0) {
                return false;
            }
            return true;
        }

        function print() {
            angular.element('#print').html('');
            $templateRequest("inventory-issue/loading-sheet.print.html").then(function (html) {
                angular.element('#print').append($compile(html)($scope));
                $timeout(function () {
                    $route.reload();
                    $window.print();
                }, 500);
            });
        }

        $scope.changeItemOrderAs = function ($event, value) {
            $event.preventDefault();
            $scope.loadingItem.itemOrderedAs = value;
        };

        $scope.addItem = function () {
            if ($scope.loadingItem.customerId && $scope.loadingItem.itemId) {
                $scope.loadingItem.orderStatus = 2;
                $scope.loadingList.push($scope.loadingItem);
                $scope.loadingItem = new Entity.LoadingItem();

                angular.element('#customerSearchSelect').val(null).trigger('change');
                angular.element('#inventorySearchSelect').val(null).trigger('change');
            }
        };

        $scope.removeItem = function ($event, index) {
            $event.preventDefault();
            $scope.loadingList.splice(index, 1);
        };

        $scope.getItemListByTackingStatus = function () {
            $scope.isLoadingItems = true;
            var orderTrackingStatus = 1; //ordered
            itemIssueService.getItemListByTackingStatus(orderTrackingStatus, $scope.loadingSheet.lineId, null).then(function (response) {
                $scope.loadingList = response;
                $scope.loadingList.forEach(function (item) {
                    item.isSelected = item.isSelected === 1;
                });
                $scope.isLoadingItems = false;
            }).catch(function (err) {
                $scope.isLoadingItems = false;
                console.log("Error in controller", err);
            });
        };

        $scope.saveChanges = function () {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            $scope.selectedLoadingList.length = 0;
            $scope.loadingList.forEach(function (item) {
                if (item.isSelected) {
                    item.orderStatus = 2; //set order status
                    $scope.selectedLoadingList.push(item);
                }
            });

            if (!validateloadingSheet($scope.selectedLoadingList)) {
                $scope.form.$setPristine();
                hasValidationErrors = true;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;
                $scope.loadingSheet.items = $scope.selectedLoadingList;

                //get data to display in the loading sheet
                var orderTrackingStatus = 1; //ordered
                itemIssueService.getAggregateItemListByTackingStatus(orderTrackingStatus, $scope.loadingSheet.lineId).then(function (httpResponse) {
                    $scope.aggregatedItems = httpResponse;
                    itemIssueService.saveLoadingSheet($scope.loadingSheet).then(function (response) {
                        if (response.success) {
                            $scope.loadingSheet.loadingSheetCode = response.loadingSheetCode;
                            $scope.isLoadingSheetGenerated = true;
                            showSuccessToaster('Loading sheet has been saved.');
                            print();
                            $scope.showLoading = false;
                        } else {
                            $scope.errorMessage = response.message;
                        }
                    }).catch(function (ex) {
                        showErrorToaster('Loading sheet has not been saved. Please try again.');
                        $scope.showLoading = false;
                    });
                }).catch(function (ex) {
                    showErrorToaster('Error generating loading sheet. Please try again.');
                    $scope.showLoading = false;
                });

            }
        };

        $scope.empoyeeSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.name, code: o.employeeId };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.code + ' - <strong>' + data.text + '</strong></span> - \t');
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.loadingSheet.employeeId = event.params.data.id;
                $scope.loadingSheet.employeeCode = event.params.data.code;
                $scope.loadingSheet.employeeName = event.params.data.text;
            }
        };

        $scope.linesSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.route, line: o };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.line.code + ' - ' + data.text + '</span> - \t');
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.$apply(function () {
                    $scope.loadingSheet.lineId = event.params.data.id;
                    $scope.loadingSheet.lineRoute = event.params.data.text;
                    $scope.loadingList = [];
                });
                $scope.getItemListByTackingStatus();
            }
        };

        $scope.inventorySearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.itemCode, text: o.itemname, inventory: o, disabled: o.quantity === 0 };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    var isOutOfStock = data.inventory.quantity === 0;

                    var template = (isOutOfStock ? '<div class="not-available">' : '<div>')
                        + '<small class="helper-block">' + data.id + '</small>'
                        + '<p>'
                        + (isOutOfStock ? '<span class="oos-symbol-container">out of stock <i class="fa fa-level-down" aria-hidden="true"></i></span>' : '')
                        + '<strong>' + data.text + '</strong> ' + (isOutOfStock ? '' : '- <span class="available">' + data.inventory.quantity) + '</span>'
                        + '</p>'
                        + '</div>';

                    return $(template);
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.$apply(function () {
                    var selectedItem = event.params.data.inventory;

                    //if selected item has no quantity
                    if (selectedItem.quantity === 0) {
                        $scope.selectedInventoryItem = null;
                        return;
                    }

                    $scope.loadingItem.itemId = selectedItem.itemId;
                    $scope.loadingItem.itemCode = selectedItem.itemCode;
                    $scope.loadingItem.itemName = selectedItem.itemname;
                    $scope.loadingItem.customProductPrice = selectedItem.customPrice;
                    $scope.loadingItem.collectiveAmount = selectedItem.customPrice;
                    $scope.loadingItem.quantity = 1;
                });
            }
        };

        $scope.customerSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.name, code: o.customerId, nic: o.nic };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    var template = '<div>'
                        + '<small class="helper-block">' + data.code + '</small>'
                        + '<p>'
                        + '<strong>' + data.text + '</strong><br/>'
                        + (data.nic ? ('<small>(' + data.nic + ')</small>') : '')
                        + '</p>'
                        + '</div>';

                    return $(template);
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.$apply(function () {
                    $scope.loadingItem.customerId = event.params.data.id;
                    $scope.loadingItem.customerName = event.params.data.text;
                });
            }
        };

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
                return;
            }
        };
    });
})(angular.module('project-x.item-issue'));