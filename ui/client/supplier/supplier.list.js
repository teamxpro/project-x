'use strict';

(function (module) {
    module.controller('supplierListController', function ($scope, $location, sessionFactory, supplierService, viewbag, config) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    deleteSuppliers(records);
                    break;
            }
        }

        function onRowClick($event, supplier) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    viewbag.put('supplier', supplier);
                    $location.path("/supplier/details/" + encodeURIComponent(supplier.id));
                    break;
                case 'edit':
                    resetQueryStrings();
                    viewbag.put('supplier', supplier);
                    $location.path("/supplier/edit/" + encodeURIComponent(supplier.id));
                    break;
                case 'delete':
                    deleteSuppliers([supplier]);
                    break;
            }
        }

        function editSupplier(supplier) {
            $event.preventDefault();
            resetQueryStrings();
            viewbag.put('supplier', supplier);
            $location.path("/supplier/edit/" + encodeURIComponent(supplier.id));
        }

        function deleteSuppliers(suppliers) {
            if (confirm('Are you sure you want delete this ?')) {
                var selected = suppliers.map(function (supplier) {
                    return supplier.supplierId;
                });

                supplierService.changeDeleteStatus(selected).then(function () {
                    $scope.gridAdapter.reload();
                }).catch(function (ex) {
                    console.error('Ex :' + ex);
                });
            }
        }

        $scope.init = function () {
            $scope.gridAdapter = {
                dataSource: supplierService.searchAll,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/supplier/create"
            };
        };

    });
})(angular.module('project-x.supplier'));