'use strict';

(function (module) {
    module.service('supplierService', function (httpFactory, config) {

        this.create = function (supplier) {
            var url = config.getBaseApi() + 'supplier/create';
            return httpFactory.post(url, supplier).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getBySupplierById = function (id) {
            var url = config.getBaseApi() + 'supplier/' + encodeURIComponent(id);
            return httpFactory.get(url).then(function (response) {
                return (response.data && response.data.length > 0) ? response.data[0] : null;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.changeActiveStatus = function (supplierId, isActive) {
            var url = config.getBaseApi() + 'supplier/status/' + encodeURIComponent(id);
            return httpFactory.put(url, { isActive: isActive }).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.changeDeleteStatus = function (supplierId) {
            var url = config.getBaseApi() + 'supplier/delete/' + encodeURIComponent(id);
            return httpFactory.put(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.update = function (supplier) {
            var url = config.getBaseApi() + 'supplier/update/' + encodeURIComponent(supplier.id);
            return httpFactory.put(url, supplier).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getAllSuppliers = function () {
            var url = config.getBaseApi() + 'suppliers/active';
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAll = function (searchText, index) {
            var url = config.getBaseApi() + 'suppliers/search';
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += '?searchText=' + searchText + '&size=' + config.getPageSize() + '&index=' + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.supplier'));