'use strict';

(function (module) {
    module.controller('employeeListController', function ($scope, $location, sessionFactory, permissionFactory, employeeService, viewbag, config) {

        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    deleteEmployee(records);
                    break;
            }
        }

        function onRowClick($event, employee) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    viewbag.put('employee', employee);
                    $location.path("/employee/edit/" + encodeURIComponent(employee.id));
                    break;
                case 'edit':
                    resetQueryStrings();
                    viewbag.put('employee', employee);
                    $location.path("/employee/edit/" + encodeURIComponent(employee.id));
                    break;
                case 'delete':
                    deleteEmployee([employee]);
                    break;
            }
        }

        function deleteEmployee(employees) {
            if (confirm('Are you sure you want delete this ?')) {
                var selected = employees.map(function (employee) {
                    return employee.id;
                });

                employeeService.changeDeleteStatus(selected).then(function () {
                    $scope.gridAdapter.reload();
                }).catch(function (ex) {
                    console.error('Ex :' + ex);
                });
            }
        }

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
                return;
            }

            /*if (!permissionFactory.get().permissions.customerSearch) {
                $location.path('/error/403');
                return;
            }*/

            $scope.gridAdapter = {
                dataSource: employeeService.searchAll,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/employee/create"
            };
        };
    });
})(angular.module('project-x.employee'));