'use strict';

(function (module) {
    module.controller('employeeCreateController', function ($scope, $routeParams, $route, $location, $filter, $toaster, sessionFactory, employeeService, viewbag) {

        var employeeId = null;
        $scope.nicValidateFormat = /^[0-9]{9}[vVxX]$/;
        $scope.showLoading = false;
        $scope.isEditing = false;
        $scope.errorMessage = null;
        $scope.showSaveButton = false;
        $scope.employee = new Entity.Employee();

        function getEmployeeDetails(employeeId) {
            $scope.showLoading = true;
            $scope.employee = null;
            employeeService.getEmployeeByEmployeeId(employeeId).then(function (result) {
                if (result && result !== 'undefined') {
                    $scope.showSaveButton = true;
                    $scope.showLoading = false;
                    $scope.employee = result;
                } else {
                    $scope.backToList();
                }
            }).catch(function (ex) {
                console.log(ex);
                $scope.showLoading = false;
            });
        }

        $scope.createNew = function () {
            if ($scope.isEditing) {
                $location.path('/employee/create');
            } else {
                $route.reload();
            }
        };

        $scope.backToList = function () {
            $location.path("/employee/list");
        }

        $scope.save = function (employee) {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            if (!$scope.form.$valid) {
                hasValidationErrors = true;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;
                if (employeeId && $scope.employee) {
                    employeeService.update(employee).then(function (response) {
                        if (response.success) {
                            viewbag.put('employee', employee);
                            $toaster.pop({
                                message: 'Successfully saved',
                                type: 'success'
                            });
                        } else {
                            $scope.errorMessage = response.message;
                        }
                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        console.log("Error in controller", ex);
                        $scope.showLoading = false;
                    });
                } else {
                    employeeService.create(employee).then(function (response) {
                        if (response.success) {
                            employee.id = response.id;
                            $toaster.pop({
                                message: 'Record has been successfully saved',
                                type: 'success'
                            });
                            $scope.form.$setPristine();
                            $scope.employee = new Entity.Employee();
                        } else {
                            $scope.errorMessage = response.message;
                        }
                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        console.log("Error in controller", ex);
                        $scope.showLoading = false;
                    });
                }
            }
        };

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                employeeId = $routeParams.employeeId ? decodeURIComponent($routeParams.employeeId) : null;
                if (employeeId) {
                    $scope.isEditing = true;
                    if (viewbag.isExist('employee')) {
                        $scope.showSaveButton = true;
                        $scope.employee = viewbag.get('employee');
                        viewbag.remove('employee');
                    } else {
                        getEmployeeDetails(employeeId);
                    }
                } else {
                    $scope.showSaveButton = true;
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.employee'));