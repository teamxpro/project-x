'use strict';

(function (module) {
    module.controller('itemReturnController', function ($scope, $route, $toaster, sessionFactory, itemService, lookupService) {

        $scope.searchFields = {
            itemId: null,
            salesTypeId: null,
            customerId: null
        };

        $scope.returnedItem = {
            amount: null,
            salesPerson: null,
            itemId: null,
            code: null,
            itemName: null,
            qty: null,
            salesTypeId: null,
            salesType: null,
            customerId: null,
            customerName: null
        };

        $scope.showLoading = false;
        $scope.isEditing = false;
        $scope.errorMessage = null;

        $scope.resetSearchResults = function () {
            $scope.searchFields = null;
            $scope.returnedItem = null;
        }
        $scope.save = function (returnedItem) {
            $scope.showLoading = true;
            itemService.addReturnedItem(returnedItem).then(function (response) {
                if (response.success) {
                    showSuccessToaster('Returned item was saved successfully');
                } else {
                    $scope.errorMessage = response.message;
                }
                $scope.showLoading = false;
                $scope.errorMessage = null;
                $scope.form.$setPristine();
            }).catch(function (ex) {
                showErrorToaster('An error occured, retry');
                $scope.showLoading = false;
            });
        };

        $scope.search = function (searchField) {
            var salesType = searchField.salesTypeId.value;
            var itemId = searchField.itemId;
            var customerId = searchField.customerId;

            itemService.searchAllSoldItems(salesType, itemId, customerId).then(function (response) {
                if (response) {
                    $scope.returnedItem = response;
                } else {
                    showErrorToaster('No records found');
                }

            }).catch(function (ex) {
                showErrorToaster('An error occured, retry');
            });
        }
        function fillSalesTypes() {
            $scope.salesTypes = lookupService.getLookup('salesType');
            $scope.selectedSalesType = $scope.salesTypes[0];
        }

        $scope.inventorySearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.itemId, text: o.itemname, inventory: o };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.id + ' - <strong>' + data.text + '</strong> - <i> ' + (data.inventory.quantity === 0 ? 'Out of stock' : data.inventory.quantity) + '</i></span > - \t');
                }
                return 'Please wait..';
            }
        };

        $scope.customerSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.name, code: o.customerId };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.code + ' - ' + data.text + '</span> - \t');
                }
                return 'Please wait..';
            }
        };

        function showSuccessToaster(message) {
            $toaster.pop({
                message: message,
                type: 'success'
            });
        }

        function showErrorToaster(message) {
            $toaster.pop({
                message: message,
                type: 'error'
            });
        }
        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                fillSalesTypes();
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };

    });

})(angular.module('project-x.item'));