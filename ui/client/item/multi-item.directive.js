'use strict';

(function (module) {
    module.directive('multiItem', function () {
        return{
            restrict : 'E',
            scope:true,
            controller : function($scope){

            },
            link : function($scope, $element, $attribute){
                
            }
        };
    });
})(angular.module('project-x.item'));