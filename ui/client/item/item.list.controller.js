'use strict';

(function (module) {
    module.controller('itemListController', function ($scope, $location, sessionFactory, itemService, viewbag, config) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    deleteItems(records);
                    break;
            }
        }

        function onRowClick($event, item) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    viewbag.put('item', item);
                    $location.path("/item/details/" + encodeURIComponent(item.id));
                    break;
                case 'edit':
                    resetQueryStrings();
                    viewbag.put('item', item);
                    $location.path("/item/edit/" + encodeURIComponent(item.id));
                    break;
                case 'delete':
                    deleteItems([item]);
                    break;
            }
        }

        function editSupplier(item) {
            $event.preventDefault();
            resetQueryStrings();
            viewbag.put('item', item);
            $location.path("/item/edit/" + encodeURIComponent(item.id));
        }

        function deleteItems(items) {
            if (confirm('Are you sure you want delete this ?')) {
                var selected = items.map(function (item) {
                    return item.id;
                });

                itemService.changeDeleteStatus(selected).then(function () {
                    $scope.gridAdapter.reload();
                }).catch(function (ex) {
                    console.error('Ex :' + ex);
                });
            }
        }

        $scope.init = function () {
            $scope.gridAdapter = {
                dataSource: itemService.searchAll,
                recentDataSource: itemService.recentlyCreated,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/item/create"
            };
        };

    });
})(angular.module('project-x.item'));