'use strict';

(function (module) {
    module.service('itemService', function (httpFactory, config, $http) {

        this.create = function (item) {
            var url = config.getBaseApi() + 'item/create';
            return httpFactory.post(url, item).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getItemById = function (itemId) {
            var url = config.getBaseApi() + 'item/' + encodeURIComponent(itemId);
            return httpFactory.get(url).then(function (response) {
                return (response.data && response.data.length > 0) ? response.data[0] : null;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.changeActiveStatus = function (itemId, isActive) {
            var url = config.getBaseApi() + 'item/status/' + encodeURIComponent(itemId);
            return httpFactory.put(url, { isActive: isActive }).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.changeDeleteStatus = function (itemId) {
            var url = config.getBaseApi() + 'item/delete/' + encodeURIComponent(itemId);
            return httpFactory.put(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.update = function (item) {
            var url = config.getBaseApi() + 'item/update/' + encodeURIComponent(item.id);
            return httpFactory.put(url, item).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getAllItems = function () {
            var url = config.getBaseApi() + 'items/active';
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAll = function (searchText, index) {
            var url = config.getBaseApi() + 'items/search';
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += '?searchText=' + searchText + '&size=' + config.getPageSize() + '&index=' + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.recentlyCreated = function (size) {
            var url = config.getBaseApi() + 'items/recently-created?size=' + size;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.addReturnedItem = function (ReturnedItem) {
            var url = config.getBaseApi() + 'item/return';

            return httpFactory.post(url, ReturnedItem).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAllSoldItems = function (salesType, itemId, customerId) {
            var url = config.getBaseApi() + 'transaction/item/' + itemId + '/type/' + salesType + '?customerId=' + customerId;
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.item'));