'use strict';

(function (module) {
    module.controller('lineCreateController', function ($scope, $routeParams, $location, $filter, sessionFactory, permissionFactory, lineService, viewbag) {

        var lineId = null;
        $scope.showLoading = false;
        $scope.isEditing = false;
        $scope.errorMessage = null;
        $scope.line = new Entity.Line();

        function getLineDetails(lineId) {
            $scope.showLoading = true;
            $scope.line = null;
            lineService.getByLineId(lineId).then(function (result) {
                if (result && result !== 'undefined') {
                    $scope.line = result;
                    setLinerDetails();
                }
                $scope.showLoading = false;
            }).catch(function (ex) {
                console.log(ex);
                $scope.showLoading = false;
            });
        }

        function setLinerDetails() {
            $scope.empoyeeSearchConfig.initialData = { id: $scope.line.linerId, text: $scope.line.linerName };
        }

        $scope.cancelLineView = function () {
            $location.path("/line/list");
        }

        $scope.save = function (line) {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            if (!$scope.form.$valid) {
                hasValidationErrors = true;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;
                if (lineId && $scope.line) {
                    lineService.update(line).then(function (response) {
                        if (response.success) {
                            viewbag.put('line', line);
                            $location.path("/line/details/" + encodeURIComponent(lineId));
                        } else {
                            $scope.errorMessage = response.message;
                        }
                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        console.log("Error in controller", ex);
                        $scope.showLoading = false;
                    });
                } else {
                    lineService.create(line).then(function (response) {
                        if (response.success) {
                            line.lineId = response.lineId;
                            viewbag.put('line', line);
                            $location.path("/line/details/" + encodeURIComponent(line.lineId));
                        } else {
                            $scope.errorMessage = response.message;
                        }
                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        console.log("Error in controller", ex);
                        $scope.showLoading = false;
                    });
                }
            }
        };

        $scope.empoyeeSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.name, code: o.employeeId };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.code + ' - <strong>' + data.text + '</strong></span> - \t');
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.line.linerName = event.params.data.text;
                $scope.line.linerId = event.params.data.id;
            }
        };

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                if (!permissionFactory.get().permissions.createLine) {
                    $location.path('/error/403');
                    return;
                }
                lineId = $routeParams.lineId ? decodeURIComponent($routeParams.lineId) : null;
                if (lineId) {
                    $scope.isEditing = true;
                    if (viewbag.isExist('line')) {
                        $scope.line = viewbag.get('line');
                        viewbag.remove('line');
                        setLinerDetails();

                    } else {
                        getLineDetails(lineId);
                    }
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.line'));