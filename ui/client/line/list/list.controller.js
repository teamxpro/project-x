'use strict';

(function (module) {
    module.controller('lineListController', function ($scope, $location, sessionFactory,permissionFactory, lineService, viewbag, config) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    deleteLines(records);
                    break;
            }
        }

        function onRowClick($event, line) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    viewbag.put('line', line);
                    $location.path("/line/details/" + encodeURIComponent(line.id));
                    break;
                case 'edit':
                    resetQueryStrings();
                    $location.path("/line/edit/" + encodeURIComponent(line.id));
                    break;
                case 'delete':
                    deleteLines([line]);
                    break;
            }
        };

        function editLine(line) {
            $event.preventDefault();
            resetQueryStrings();
            viewbag.put('line', line);
            $location.path("/line/edit/" + encodeURIComponent(line.id));
        }

        function deleteLines(lines) {
            if (confirm('Are you sure you want delete this ?')) {
                var selected = lines.map(function (line) {
                    return line.id;
                });

                lineService.delete(selected).then(function () {
                    $scope.gridAdapter.reload();
                }).catch(function (ex) {
                    console.error('Ex :' + ex);
                });
            }
        }

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
                return;
            }

            if (!permissionFactory.get().permissions.viewLine) {
                $location.path('/error/403');
                return;
            }
            $scope.gridAdapter = {
                dataSource: lineService.searchAll,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/line/create"
            };

        }

    });
})(angular.module('project-x.line'));