'use strict';

Date.prototype.displayFormat = function () {
    //yyyy-mm-dd
    return this.getFullYear() + '-' + (this.getMonth() + 1) + '-' + this.getDate()
};

$(function () {

    $(function () {
        /*https://select2.github.io/options.html*/
        //$.fn.select2.defaults.set("theme", "classic");

        $('[data-toggle="tooltip"]').tooltip();

        $('.dropdown-toggle').dropdown();

        $('[ui-scroll="app"]').on('click', function (e) {
            e.preventDefault();

            $('html,body').scrollTop(0);
        });
    });
});
