(function (module) {
    module.service('lookupsService', function (httpFactory, config) {

        this.getLookupCategories = function () {
            var url = config.getLookupCategoriesUrl();

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.createLookupCategory = function (category) {
            var url = config.createLookupCategoryUrl();

            return httpFactory.post(url, category).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getLookups = function (categoryId) {
            var url = config.getLookupsUrl(categoryId);

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.createLookup = function (lookup) {
            var url = config.getCreateLookupUrl();

            return httpFactory.post(url, lookup).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

    });
})(angular.module('project-x.cms'));