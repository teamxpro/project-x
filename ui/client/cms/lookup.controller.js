(function (module) {
    module.controller('lookupController', function ($scope, lookupsService) {
        $scope.categories = [];
        $scope.lookups = [];
        $scope.category = {name : null};
        $scope.lookup = {name : null};
        $scope.selectedCategory = null;
        $scope.loadingCategory = false;
        $scope.loadingLookups = false;

        $scope.init = function () {
            $scope.loadingCategory = true;
            lookupsService.getLookupCategories().then(function (categories) {
                $scope.loadingCategory = false;
                $scope.categories = categories;
            }).catch(function (ex) {
                $scope.loadingCategory = false;
                console.log(ex);
            });

            $('#addItemModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget)
                var parentId = button.data('parentId')
                
                $scope.lookup =  $scope.lookup || {name : null};
                $scope.lookup.categoryId = $scope.selectedCategory.id;
                if(parentId !== -1){
                    $scope.lookup.parentId = parentId;
                }
            });
        };

        $scope.loadLookups = function(category) {
            $scope.selectedCategory = category;
            $scope.loadingLookups = true;
            lookupsService.getLookups(category.id).then(function (lookups) {
                $scope.loadingLookups = false;
                $scope.lookups = lookups;
            }).catch(function (ex) {
                $scope.loadingLookups = false;
                console.log(ex);
            });
        };

        $scope.addCategory = function(category){
            if ($scope.categoryForm.$valid) {
                lookupsService.createLookupCategory(category).then(function(newCategory){
                    $scope.category = {name : null};
                    $scope.categories.push(newCategory);

                    $scope.categoryForm.$setPristine();

                }).catch(function(ex){
                    console.log(ex);
                });
            }
        }

        $scope.addLookup = function(lookup){
            if ($scope.lookupForm.$valid) {
                lookup.categoryId = $scope.selectedCategory.id;

                lookupsService.createLookup(lookup).then(function(newLookup){
                    $scope.lookup = {name : null};
                    $scope.lookupForm.$setPristine();

                    $scope.loadLookups($scope.selectedCategory);
                }).catch(function(ex){
                    console.log(ex);
                });

                $('#addItemModal').modal('hide');
            }
        };
    });
})(angular.module('project-x.cms'));