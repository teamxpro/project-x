'use strict';

(function (module) {
    module.controller('customerProductListController', function ($scope, $location, sessionFactory, customerProductService, viewbag, config) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    deleteProducts(records);
                    break;
            }
        }

        function onRowClick($event, customerProduct) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    viewbag.put('customerProduct', customerProduct);
                    $location.path("/custom-product/details/" + encodeURIComponent(customerProduct.id));
                    break;
                case 'delete':
                    deleteProducts([customerProduct]);
                    break;
            }
        }

        function deleteProducts(customerProducts) {
            if (confirm('Are you sure you want delete this ?')) {
                var selected = customerProducts.map(function (customerProduct) {
                    return customerProduct.id;
                });

                customerProductService.changeDeleteStatus(selected).then(function () {
                    $scope.gridAdapter.reload();
                }).catch(function (ex) {
                    console.error('Ex :' + ex);
                });
            }
        }

        $scope.init = function () {
            $scope.gridAdapter = {
                dataSource: customerProductService.searchAll,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/custom-product/create"
            };
        };

    });
})(angular.module('project-x.customer-product'));