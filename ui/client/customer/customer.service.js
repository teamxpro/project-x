'use strict';

(function (module) {
    module.service('customerService', function (httpFactory, httpExceptionFactory, config) {

        this.create = function (customer) {
            var url = config.getBaseApi() + 'customers';
            return httpFactory.post(url, customer).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.getByCustomerId = function (customerId) {
            var url = config.getBaseApi() + 'customers/' + encodeURIComponent(customerId);
            return httpFactory.get(url).then(function (response) {
                return (response.data && response.data.length > 0) ? response.data[0] : null;
            }).catch(handleException);
        };

        this.delete = function (customerId) {
            var url = config.getBaseApi() + 'customers/' + encodeURIComponent(customerId);
            return httpFactory.delete(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.update = function (customer) {
            var url = config.getBaseApi() + 'customers/' + encodeURIComponent(customer.id);
            return httpFactory.put(url, customer).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.searchAll = function (searchText, index) {
            var url = config.getBaseApi() + 'customers';
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += "?searchText=" + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.recentlyCreated = function (size) {
            var url = config.getBaseApi() + "customers/recently-created?size=" + size;
            
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        function handleException(ex) {
            switch (ex && ex.status) {
                case 401:
                    httpExceptionFactory.unauthorized(ex);
                    break;

                case 403:
                    httpExceptionFactory.forbidden(ex);
                    break;

                case 500:
                    httpExceptionFactory.internalServerError(ex);
                    break;

                default:
                    httpExceptionFactory.unknownError(ex);
                    break;
            }
        }
    });
})(angular.module('project-x.customer'));