'use strict';

(function (module) {
    module.controller('customerListController', function ($scope, $location, sessionFactory, permissionFactory, customerService, viewbag, config) {

        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    deleteCustomers(records);
                    break;
            }
        }

        function onRowClick($event, customer) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    viewbag.put('customer', customer);
                    $location.path("/customer/details/" + encodeURIComponent(customer.id));
                    break;
                case 'edit':
                    resetQueryStrings();
                    viewbag.put('customer', customer);
                    $location.path("/customer/edit/" + encodeURIComponent(customer.id));
                    break;
                case 'delete':
                    deleteCustomers([customer]);
                    break;
            }
        }

        function editCustomer(customer) {
            $event.preventDefault();
            resetQueryStrings();
            viewbag.put('customer', customer);
            $location.path("/customer/edit/" + encodeURIComponent(customer.id));
        }

        function deleteCustomers(customers) {
            if (confirm('Are you sure you want delete this ?')) {
                var selected = customers.map(function (customer) {
                    return customer.id;
                });

                customerService.delete(selected).then(function () {
                    $scope.gridAdapter.reload();
                }).catch(function (ex) {
                    console.error('Ex :' + ex);
                });
            }
        }

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
                return;
            }

            if (!permissionFactory.get().permissions.customerSearch) {
                $location.path('/error/403');
                return;
            }

            $scope.gridAdapter = {
                dataSource: customerService.searchAll,
                recentDataSource: customerService.recentlyCreated,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/customer/create"
            };
        };
    });
})(angular.module('project-x.customer'));