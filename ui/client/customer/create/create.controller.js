'use strict';

(function (module) {
    module.controller('customerCreateController', function ($scope, $routeParams, $route, $location, $filter, $toaster, lookupService, sessionFactory, customerService, lineService, viewbag) {

        var customerId = null;
        $scope.nicValidateFormat = /^[0-9]{9}[vVxX]$/;
        $scope.showLoading = false;
        $scope.isEditing = false;
        $scope.errorMessage = null;
        $scope.showSaveButton = false;
        $scope.selectedRoute = null;
        $scope.customer = new Entity.Customer();
        $scope.showMap = false;

        function getCustomerDetails(customerId) {
            $scope.showLoading = true;
            $scope.customer = null;
            customerService.getByCustomerId(customerId).then(function (result) {
                if (result && result !== 'undefined') {
                    $scope.selectedLanguage.key = result.language;
                    $scope.selectedLanguage.value = result.languageName;
                    $scope.showSaveButton = true;
                    $scope.showLoading = false;
                    $scope.showMap = true;
                    $scope.customer = result;
                } else {
                    $scope.backToList();
                }
            }).catch(function (ex) {
                console.log(ex);
                $scope.showLoading = false;
            });
        }

        function fillLanguages() {
            $scope.languages = lookupService.getLookup('language');
            $scope.selectedLanguage = $scope.languages[0];
            $scope.customer.language = $scope.languages[0].key;
            $scope.customer.languageName = $scope.languages[0].value;
        }

        $scope.setSelectedLanguage = function () {
            $scope.customer.language = $scope.selectedLanguage.key;
            $scope.customer.languageName = $scope.selectedLanguage.value;
        };

        $scope.createNew = function () {
            if ($scope.isEditing) {
                $location.path('/customer/create');
            } else {
                $route.reload();
            }
        };

        $scope.backToList = function () {
            $location.path("/customer/list");
        }

        $scope.save = function (customer) {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            if (!$scope.form.$valid) {
                hasValidationErrors = true;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;
                customer.isActive = 1;
                if (customerId && $scope.customer) {
                    customerService.update(customer).then(function (response) {
                        if (response.success) {
                            viewbag.put('customer', customer);
                            $toaster.success('Successfully saved');
                        } else {
                            $scope.errorMessage = response.message;
                        }
                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        $toaster.error('Unable to save changes. Please try again');
                        $scope.showLoading = false;
                    });
                } else {
                    customerService.create(customer).then(function (response) {
                        if (response.success) {
                            customer.customerId = response.customerId;
                            
                            $toaster.success('Record has been successfully saved');
                            
                            $scope.form.$setPristine();
                            $scope.customer = new Entity.Customer();
                        } else {
                            $scope.errorMessage = response.message;
                        }
                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        $toaster.error('Unable to save changes. Please try again');
                        $scope.showLoading = false;
                    });
                }
            }
        };

        $scope.linesSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.route, line: o };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.line.code + ' - ' + data.text + '</span> - \t');
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.$apply(function () {
                    $scope.customer.lineId = event.params.data.id;
                    $scope.customer.lineRoute = event.params.data.text;
                    $scope.customer.lineDate = event.params.data.line.lineDate;
                });
            }
        };

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                fillLanguages();
                customerId = $routeParams.customerId ? decodeURIComponent($routeParams.customerId) : null;
                if (customerId) {
                    $scope.isEditing = true;
                    if (viewbag.isExist('customer')) {
                        $scope.showSaveButton = true;
                        $scope.customer = viewbag.get('customer');
                        $scope.showMap = true;
                        viewbag.remove('customer');
                    } else {
                        getCustomerDetails(customerId);
                    }
                } else {
                    $scope.showMap = true;
                    $scope.showSaveButton = true;
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.customer'));