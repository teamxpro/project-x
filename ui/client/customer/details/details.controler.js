'use strict';

(function (module) {
    module.controller('customerDetailsController', function ($scope, $route, $location, $routeParams, $timeout, $toaster, sessionFactory, customerService, viewbag) {

        $scope.customer = null;
        $scope.showLoading = false;
        $scope.showMap = true;

        function initializeGoogleMap() {
            var selectedLatlng = null;
            if ($scope.customer.latitude && $scope.customer.longitude) {

                selectedLatlng = new google.maps.LatLng($scope.customer.latitude, $scope.customer.longitude);
                var mapOptions = {
                    zoom: 10,
                    center: selectedLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

                var marker = new google.maps.Marker({
                    draggable: false,
                    position: selectedLatlng,
                    map: map
                });

                google.maps.event.addListener(marker, 'dragend', function (event) { });
            } else {
                $scope.showMap = false;
            }
        }

        function getCustomerDetails(customerId) {
            $scope.showLoading = true;
            customerService.getByCustomerId(customerId).then(function (result) {
                if (result) {
                    $scope.customer = result;
                    initializeGoogleMap();
                    $scope.showLoading = false;
                } else {
                    $location.path("/customer/list");
                }
            }).catch(function (ex) {
                console.log(ex);
                $toaster.pop(
                    {
                        message: 'An error occured, try again',
                        type: 'error'
                    });
                $scope.showLoading = false;
            });
        };

        $scope.updateDeleteStatus = function (customer) {
            customerService.delete(customer.id).then(function (result) {
                $scope.backToList();
            }).catch(function (ex) {
                $toaster.pop(
                    {
                        message: 'An error occured, try again',
                        type: 'error'
                    });
                console.log(ex);
            });
        };

        $scope.editCustomer = function (customer) {
            viewbag.put('customer', customer);
            $location.path("/customer/edit/" + encodeURIComponent(customer.id));
        };

        $scope.backToList = function () {
            $location.path("/customer/list");
        }

        $scope.init = function () {
            var customerId = decodeURIComponent($routeParams.customerId);
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
                return;
            }
            if (viewbag.isExist('customer')) {
                $scope.customer = viewbag.get('customer');
                if ($scope.customer.latitude && $scope.customer.longitude) {
                    $timeout(function () {
                        initializeGoogleMap();
                    }, 300);
                } else {
                    $scope.showMap = false;
                }
                viewbag.remove('customer');
            } else {
                getCustomerDetails(customerId);
            }
        };
    });
})(angular.module('project-x.customer'));