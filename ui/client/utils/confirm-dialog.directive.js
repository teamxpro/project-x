(function (module) {
    module.directive('confirmDialog', function ($templateCache) {

        var confirmDialogController = function ($scope) {

            $scope.closeWindow = function ($event) {
                $event.preventDefault();
                $('#confirmDialog').modal('hide');
            };

            $scope.onOkayClick = function () {
                $('#confirmDialog').modal('hide');
                $scope.onOkay();
            };
        };
        return {
            restrict: 'EA',
            scope: {
                confirmMessage: '=',
                onOkay: '&onOkay'
            },
            controller: confirmDialogController,
            template: $templateCache.get('utils/confirm-dialog.html')
        };

    });

})(angular.module('project-x.utils'));

