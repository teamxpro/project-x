(function (module) {
    module.directive('mapLocation', function ($templateCache) {
        return {
            restrict: 'EA',
            scope: {
                label: '=',
                latitude: '=',
                longitude: '='
            },
            controller: function ($scope) {
                function initializeGoogleMap() {
                    var selectedLatlng = null;

                    if ($scope.latitude && $scope.longitude) {
                        selectedLatlng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    } else {
                        selectedLatlng = new google.maps.LatLng(6.927079, 79.861244);
                    }

                    var mapOptions = {
                        zoom: 12,
                        center: selectedLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

                    var marker = new google.maps.Marker({
                        draggable: true,
                        position: selectedLatlng,
                        map: map
                    });

                    google.maps.event.addListener(marker, 'dragend', function (event) {
                        $scope.latitude = event.latLng.lat();
                        $scope.longitude = event.latLng.lng();
                    });
                }

                initializeGoogleMap();
            },
            template: $templateCache.get('utils/map-location.html')
        };

    });

})(angular.module('project-x.utils'));

