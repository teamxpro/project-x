(function (module) {
    module.factory('httpFactory', function ($http) {
        return $http;
    });

    module.factory('httpExceptionFactory', function ($http, $rootScope, $toaster, $location) {

        return {
            /**
             * ex: exception
             * config : { log:true, notify:true, logout:true }
             */
            badRequest : function(ex, config){
                $toaster.pop({message: 'Bad request',type: 'error'});
            },
            unauthorized : function(ex, config){
                $toaster.pop({message: 'Session has been expired, login again to continue',type: 'error'});
                $rootScope.$broadcast('application:force-logout');
            },
            forbidden : function(ex, config){
                $toaster.pop({message: 'Your don`t have permission to view this page.',type: 'error'});
                $location.path('/errors/403');
            },
            internalServerError : function(ex, config){
                $toaster.pop({message: 'Exception, try again',type: 'error'});
            },
            unknownError : function(ex, config){
                $toaster.pop({message: 'Unknown error, contact support',type: 'error'});
            }
        };
    });

    /*http://www.webdeveasy.com/interceptors-in-angularjs-and-useful-examples/*/
    module.factory('sessionInjector', function (sessionFactory) {
        var sessionInjector = {
            request: function (config) {
                if (sessionFactory.isAuthenticated()) {
                    config.headers['x-access-token'] = sessionFactory.getSession().token;
                }

                return config;
            }
        };

        return sessionInjector;
    });

    module.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('sessionInjector');
    }]);

})(angular.module('project-x.utils'));