(function(module) {
    module.directive('navigationBar', function($templateCache){
        return {
            restrict : 'E',
            replace: true,
            controller : function($scope, $timeout,$location, sessionFactory, permissionFactory){
                $scope.visible = sessionFactory.isAuthenticated();
                $scope.user = null;

                $scope.logout = function($event) {
                    if($event){
                        $event.preventDefault();
                    }
                    
                    sessionFactory.deleteSession();
                    permissionFactory.expireCache();

                    /*  wait few miliseconds untill session clear. 
                        then redirect to login page
                    */

                    $timeout(function(){
                        $scope.visible = false;
                        $location.url('/login');
                    },300);
                }

                $scope.preventLink = function($event){
                    //TODO: need to do actions latter;

                    $event.preventDefault();
                    return false;
                }

                /* events */
                $scope.$on('application:user:login:success', function(){
                    $scope.visible = true;
                    $scope.user = getUsername();
                });

                $scope.$on('application:force-logout', function(){
                    $scope.logout();
                });

                $scope.init = function(){
                    $scope.user = getUsername();
                }

                function getUsername(){
                    if($scope.visible && sessionFactory.getSession()){
                        return sessionFactory.getSession().username;
                    }

                    return null;
                }
            },
            template : $templateCache.get('utils/navigation-bar.html')
        };
    });

})(angular.module('project-x.utils'));