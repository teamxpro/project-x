'use strict';

(function (module) {
    module.directive('draggable', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function ($scope, $element, $attribute) {
                $element.draggable({ 
                    revert: 'invalid',
                    snap: '.ui-widget-snap',
                    scroll: true, 
                    scrollSensitivity: 100,
                    cursor: 'move',
                    helper: 'clone'
                });
            }
        }
    });

    module.directive('droppable', function () {
        return {
            restrict: 'A',
            scope: {
                droppable : '&'
            },
            link: function ($scope, $element, $attribute) {
                $element.droppable({
                    accept: '.draggable-item',
                    drop : function(event, ui){
                        var $this = $(this);
                        ui.droppable = $this;
                        $scope.droppable({event : event, ui: ui });
                        $scope.$apply();
                    }
                });
            }
        }
    });
})(angular.module('project-x.utils'));