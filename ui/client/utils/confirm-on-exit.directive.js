(function (module) {
    module.directive('confirmOnExit', function () {
        return {
            link: function ($scope, elem, attrs) {
                window.onbeforeunload = function () {
                    if ($scope.form.$dirty && !$scope.form.$submitted) {
                        return "If you leave before saving, your changes will be lost.";
                    }
                }
                $scope.$on('$locationChangeStart', function (event, next, current) {
                    if ($scope.form.$dirty && !$scope.form.$submitted) {
                        if (!confirm("If you leave before saving, your changes will be lost.")) {
                            event.preventDefault();
                        }
                    }
                });
            }
        };
    });

})(angular.module('project-x.utils'));

