(function (module) {
    module.service('config', function (cookieService, $window, $location) {

        var config = function () {
            return serviceVars;
        };

        var baseApi = function () {
            return config().application.api + '/';
        };

        this.fileUploadUrl = function () {
            return config().application.uploadUrl;
        };

        this.getBaseApi = function () {
            return baseApi();
        };

        this.getPageSize = function () {
            return 20; // TODO: read from config
        };

        //*******************Dashboard************************/

        this.getOutOfStockItemsUrl = function (index, pageSize) {
            return baseApi() + 'dashboard/outofstock-items?index=' + index + '&size=' + pageSize;
        };

        //*******************Customer************************/

        this.getCustomerCreateUrl = function () {
            return baseApi() + 'customers';
        };

        this.getCustomerByIdUrl = function (customerId) {
            return baseApi() + 'customers/' + encodeURIComponent(customerId);
        };

        this.getCustomerUpdateUrl = function (customerId) {
            return baseApi() + 'customers/' + encodeURIComponent(customerId);
        };

        this.getCustomerDeactivateUrl = function (customerId) {
            return baseApi() + 'customer/deactivate/' + encodeURIComponent(customerId);
        };

        this.getCustomerDeleteUrl = function (customerId) {
            return baseApi() + 'customers/' + encodeURIComponent(customerId);
        };

        this.getCustomerSearchUrl = function (searchBy) {
            return baseApi() + 'customers';
        };

        //*******************Line************************/

        this.getLineSearchUrl = function () {
            return baseApi() + 'lines/search';
        }

        this.lineCreateUrl = function () {
            return baseApi() + 'line/create';
        }

        this.getActiveLines = function () {
            return baseApi() + 'lines/active';
        }

        this.getLineByIdUrl = function (lineId) {
            return baseApi() + 'line/' + encodeURIComponent(lineId);
        }

        this.getLineUpdateUrl = function (lineId) {
            return baseApi() + 'line/update/' + encodeURIComponent(lineId);
        };

        this.getLineDeleteUrl = function () {
            return baseApi() + 'line/delete';
        }
        //*******************Inventory************************/

        this.getItemByIdUrl = function (itemId) {
            return baseApi() + 'item/' + encodeURIComponent(itemId);
        };

        this.getSearchItemUrl = function (keyword) {
            return baseApi() + 'item/items' + encodeURIComponent(keyword);
        };

        this.inventoryItemCreateUrl = function () {
            return baseApi() + 'inventory/create';
        };

        this.getinventoryItemByIdUrl = function (inventoryId) {
            return baseApi() + 'inventory/' + encodeURIComponent(inventoryId);
        };

        this.getUpdateInventoryItemUrl = function (inventoryId) {
            return baseApi() + '/inventory/update/' + encodeURIComponent(inventoryId);
        };

        //********************End of Inventory****************/

        //********************  Lookups   ****************/

        this.getLookupCategoriesUrl = function () {
            return baseApi() + 'lookups/category';
        };

        this.createLookupCategoryUrl = function () {
            return baseApi() + 'lookups/category';
        };

        this.getLookupsUrl = function (categoryId) {
            return baseApi() + 'lookups/category/' + categoryId;
        };

        this.getCreateLookupUrl = function () {
            return baseApi() + 'lookups';
        };

        //************ Customer Products ************ */

        this.getCustomreProductForCustomerUrl = function (customerId) {
            return baseApi() + 'customer-product/customer/' + customerId;
        };

        this.getCustomProductPaymentHistoryUrl = function(customProductId){
            return baseApi() + '/customer-product/' + customProductId + '/payments/history';
        }
        this.getProductItemUrl = function (productId) {
            return baseApi() + 'product/' + productId;
        };

        this.getCustomerProductItemsDeliveryStatusUrl = function(customerProductId){
            return baseApi() + 'customer-product/'+ customerProductId +'/delivery-status';
        };

        this.getCustomProductItemsSaveForTermsUrl = function (productId) {
            return baseApi() + 'customer-product/' + productId + '/terms/items';
        };

        this.getAllCompositeItems = function(keyword){
            if(!keyword){
                keyword = '';
            }
            
            return baseApi() + 'customer-product/items/search?searchText=' + keyword;
        };

        this.getCustomerProductItemsForProductUrl = function (productId) {
            return baseApi() + 'customer-product/items/' + productId;
        };

        this.getSaveCollectiveAmountUrl = function (termId) {
            return baseApi() + 'term/' + termId;
        };
    });

})(angular.module('project-x.utils'));
