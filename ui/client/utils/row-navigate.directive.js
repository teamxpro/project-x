(function (module) {
    module.directive('rowNavigate', function () {
        return {
            $scope:true,
            link: function ($scope, $element, $attr) {
                $element.find('td:not([override-inherited-navigation])').on('click', function(event){
                    $scope.$apply(function(){
                        $scope.$eval($attr.rowNavigate, { $event: event, data : $scope });
                    });                    
                });
            }
        }
    });

})(angular.module('project-x.utils'));