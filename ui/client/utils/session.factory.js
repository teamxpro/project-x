(function (module) {
    module.factory('sessionFactory', function (cookieService, $rootScope, $window, $location) {
        function createSession(session) {
            cookieService.set('session', JSON.stringify(session), 5184000000)
        }

        function deleteSession() {
            cookieService.clear('session');
        }

        function getSession() {
            return JSON.parse(cookieService.get('session'));
        }

        function isAuthenticated() {
            var session = getSession();
            if (session && session !== 'empty') {
                if (session.store === getStoreName()) {
                    return true;
                } else {
                    deleteSession();
                }
            }

            return false;
        }

        function redirectToLogin() {
            $rootScope.$broadcast('application:force-logout');
            $window.sessionStorage.clear();
            //$location.url('/login');
        }

        function getStoreName() {
            return angular.element('#store').val();
        }

        return {
            isAuthenticated: isAuthenticated,
            getSession: getSession,
            createSession: createSession,
            deleteSession: deleteSession,
            redirectToLoginPage: redirectToLogin,
            getStoreName: getStoreName
        };

    });

})(angular.module('project-x.utils'));