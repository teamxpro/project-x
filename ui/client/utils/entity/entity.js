'user strict';

var Entity = (function () {

    function Customer() {
        this.id;
        this.storeId;
        this.customerId;
        this.name;
        this.nic;
        this.email;
        this.language;
        this.languageName;
        this.location;
        this.mobile1;
        this.mobile2;
        this.fixedLine;
        this.emergencyContact;
        this.contactName;
        this.address;
        this.lineId;
        this.lineRoute;
        this.lineDate;
        this.otherInfo;
        this.latitude;
        this.longitude;
        this.imageUrl;
        this.isActive;
        this.createdDate;
        this.updatedDate;
    }

    function Supplier() {
        this.id;
        this.storeId;
        this.supplierId;
        this.name;
        this.email;
        this.language;
        this.languageName;
        this.mobile;
        this.fixedLine;
        this.address;
        this.agentName;
        this.agentContact;
        this.categoryId;
        this.category;
        this.otherInfo;
        this.imageUrl;
        this.isActive;
        this.createdDate;
        this.updatedDate;
    }

    function Line() {
        this.id;
        this.route;
        this.code;
        this.date;
    }

    function Item() {
        this.itemId;
        this.code;
        this.name;
        this.description;
        this.manufacturer;
        this.model;
        this.reorderLevel;
        this.unit;
        this.categoryId;
        this.categoryName;
        this.subCategoryId;
        this.subCategoryName;
        this.imageUrl;
        this.isActive;
        this.createdDate;
        this.updatedDate;
    }

    function InventoryItem() {
        this.inventoryId;
        this.itemId;
        this.storeId;
        this.itemName;
        this.itemImageUrl;
        this.quantity;
        this.remainingQuantity;
        this.store;
        this.buyingPrice;
        this.retailSellingPrice;
        this.wholeSellingPrice;
        this.easyPaymentSellingPrice;
        this.customPrice;
        this.createdDate;
        this.updatedDate;
    }

    function Invoice() {
        this.id;
        this.storeId;
        this.code;
        this.customerId;
        this.customerName;
        this.salesTypeId;
        this.salesTypeName;
        this.paymentMethodId;
        this.paymentMethodName;
        this.dueDate;
        this.invoicedDate = new Date();
        this.chequeNo;
        this.bank;
        this.branch;
        this.otherInfo;
        this.total = 0;
        this.discountAmount = 0;
        this.receivedAmount = 0;
        this.balanceAmount = 0;
        this.netAmount = 0;
        this.dueAmount = 0;
        this.isActive;
        this.createdDate;
        this.updatedDate;
    }

    function InvoiceItem() {
        this.id;
        this.invoiceId;
        this.inventoryId;
        this.inventoryCode;
        this.storeId;
        this.itemCode;
        this.itemName;
        this.price;
        this.quantity;
        this.availableQuantity;
        this.subTotal;
        this.createdDate;
        this.updatedDate;
    }

    function GRN() {
        this.grnCode;
        this.description;
        this.receivedDate = new Date();
        this.total = 0;
        this.discountAmount = 0;
        this.netAmount = 0;
        this.paymentMethod;
        this.supplierId;
    }

    function Product() {
        this.id;
        this.storeId;
        this.code;
        this.materialName;
        this.name;
        this.description;
        this.amount = 0;
        this.validFrom = new Date();
        this.validTo = new Date();
        this.isNeverExpire;
        this.total;
        this.isActive;
        this.isDeleted;
        this.createdDate;
        this.updatedDate;
        this.createdBy;
        this.modifiedBy;
    }

    function ProductItem() {
        this.id;
        this.storeId;
        this.productId;
        this.itemId;
        this.itemCode;
        this.itemName;
        this.price = 0;
        this.isFree = false;
        this.quantity;
        this.subTotal;
        this.createdDate;
        this.updatedDate;
    }

    //seettu
    function CustomerProduct() {
        this.id;
        this.storeId;
        this.code;
        this.productType = 'Default';
        this.amount = 0;
        this.customerId;
        this.customerName;
        this.productId;
        this.productName;
        this.employeeId;
        this.employeeName;
        this.lineId;
        this.lineRoute;
        this.periodFrom = new Date();
        this.periodTo;
        this.otherInfo;
        this.isActive;
        this.isDeleted;
        this.createdDate;
        this.updatedDate;
        this.createdBy;
        this.modifiedBy;
    }

    function InventoryIssue() {
        this.id;
        this.storeId;
        this.code;
        this.employeeId;
        this.employeeName;
        this.issuedDate = new Date();
        this.otherInfo;
        this.createdDate;
        this.updatedDate;
        this.createdBy;
        this.modifiedBy;
    }

    function IssuedItem() {
        this.id;
        this.inventoryIssueId;
        this.inventoryId;
        this.inventoryCode;
        this.storeId;
        this.itemCode;
        this.itemName;
        this.quantity;
        this.availableQuantity;
        this.createdDate;
        this.updatedDate;
    }

    function Payment() {
        this.id;
        this.storeId;
        this.code;
        this.customerId;
        this.customerName;
        this.paymentMethod;
        this.paymentMethodName;
        this.paymentDate = new Date();
        this.amount = 0;
        this.otherInfo;
        this.isDeleted;
        this.createdDate;
        this.updatedDate;
        this.createdBy;
        this.modifiedBy;
    }

    function Cheque() {
        this.id;
        this.storeId;
        this.paymentId;
        this.chequeNo;
        this.bank;
        this.branch;
        this.chequeDate = new Date();
        this.amount = 0;
    }

    function Employee() {
        this.id;
        this.storeId;
        this.employeeId;
        this.name;
        this.nic;
        this.email;
        this.mobile1;
        this.mobile2;
        this.fixedLine;
        this.emergencyContact;
        this.contactName;
        this.address;
        this.otherInfo;
        this.isActive;
        this.createdDate;
        this.updatedDate;
    }

    function LoadingItem() {
        this.id;
        this.isSelected = true;
        this.customerId;
        this.customerName;
        this.itemId;
        this.itemCode;
        this.itemName;
        this.quantity;
        this.customProductPrice;
        this.collectiveAmount;
        this.orderStatus;
        this.itemOrderedAs = 'Default';
        this.itemOrderedAsEdit = true;
        this.remarks;
    }

    function LoadingSheet() {
        this.id;
        this.code;
        this.loadingDate = new Date();
        this.lineId;
        this.lineRoute;
        this.lineDate;
        this.employeeId;
        this.employeeCode;
        this.employeeName;
        this.vehicleNo;
        this.otherInfo;
        this.items;
    }

    return {
        Customer: Customer,
        Supplier: Supplier,
        Line: Line,
        Item: Item,
        InventoryItem: InventoryItem,
        Invoice: Invoice,
        InvoiceItem: InvoiceItem,
        GRN: GRN,
        Product: Product,
        ProductItem: ProductItem,
        CustomerProduct: CustomerProduct,
        InventoryIssue: InventoryIssue,
        IssuedItem: IssuedItem,
        Payment: Payment,
        Cheque: Cheque,
        Employee: Employee,
        LoadingSheet: LoadingSheet,
        LoadingItem: LoadingItem
    };
})();
