(function (module) {
    module.directive('paginate', function ($templateCache, $routeParams, $location, config) {
        return {
            restrict: 'EA',
            scope: {
                recordCount: '='
            },
            controller: function ($scope) {
                $scope.pageIndex = 0;
                $scope.numberOfPages = 0;

                $scope.paginate = function (index, $event) {
                    $event.preventDefault();
                    if (index >= 0 && $scope.numberOfPages > index) {
                        $location.search('page', index + 1);
                    }
                };

                function init() {
                    var itemLength = $scope.recordCount;
                    var itemsPerPage = config.getPageSize();
                    if (itemLength < itemsPerPage) {
                        $scope.numberOfPages = 1;
                    } else {
                        $scope.numberOfPages = Math.ceil(itemLength / itemsPerPage);
                    }

                    if ($routeParams.page) {
                        $scope.pageIndex = $routeParams.page - 1;
                    }
                }

                init();

            },
            template: $templateCache.get('utils/paginate.html')
        };
    });
})(angular.module('project-x.utils'));
