(function (module) {
    module.controller('userSettingsController', function ($scope, sessionFactory, permissionFactory, settingsService) {
        $scope.visible = false;

        $scope.userSettings = {
            userId: sessionFactory.getSession().userId,
            currentPassword: null,
            newPassword: null,
            confirmPassword: null
        }

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
                return;
            }
            $scope.visible = true;
        };
    });
})(angular.module('project-x.settings'));
