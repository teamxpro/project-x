(function (module) {
    module.controller('changePasswordController', function ($scope, $toaster, sessionFactory, permissionFactory, settingsService) {
        $scope.visible = false;

        $scope.userSettings = {
            userId: sessionFactory.getSession().userId,
            currentPassword: null,
            newPassword: null,
            confirmPassword: null
        }

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
                return;
            }
            $scope.visible = true;
        };

        $scope.changePassword = function () {
            settingsService.changePassword($scope.userSettings).then(function (response) {
                if (response.success) {
                    $toaster.success('Password has been updated successfully.');
                } else {
                    $toaster.error('Password has been updated successfully.');
                }
            }).catch(function (error) {
                console.log(error);
            });
        };
    });
})(angular.module('project-x.settings'));
