(function (module) {
    module.service('settingsService', function (config, httpFactory) {

        this.changePassword = function (userSettings) {
            var url = config.getBaseApi() + 'users/change-password';
            return httpFactory.put(url, userSettings).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

    });
})(angular.module('project-x.settings'));
