'use strict';

(function (module) {
    module.service('orderTakingService', function (httpFactory, httpExceptionFactory, config) {
        this.getCustomProductItems = function (productId) {
            var url = config.getProductItemUrl(productId);

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        }

        this.getCustomerProductItemsDeliveryStatus = function(customerProductId){
            var url = config.getCustomerProductItemsDeliveryStatusUrl(customerProductId);

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.getAllCompositeItems = function(keyword){
            var url = config.getAllCompositeItems(keyword);

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.getPaymentHistory = function(customProductId){
            var url = config.getCustomProductPaymentHistoryUrl(customProductId);

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.addProductItemToTerm = function (customerId, customerProductId, productId, termId, compositeItemId, isFreeItem) {
            var url = config.getCustomProductItemsSaveForTermsUrl(productId);
            return httpFactory.post(url, {customerId : customerId, customerProductId: customerProductId, termId: termId, compositeItemId: compositeItemId, command : 'insert', isFree : isFreeItem }).then(function (response) {
                return response.data;
            }).catch(handleException);
        }

        this.removeProductItemFromTerm = function (productId, trackingId) {
            var url = config.getCustomProductItemsSaveForTermsUrl(productId);
            return httpFactory.post(url, { trackingId : trackingId, command : 'remove' }).then(function (response) {
                return response.data;
            }).catch(handleException);
        }

        this.saveCollectiveAmount = function (customerProductId, termId, collectiveAmount) {
            var url = config.getSaveCollectiveAmountUrl(termId);
            return httpFactory.post(url, {term : termId, customProductId : customerProductId, collectiveAmount : collectiveAmount }).then(function (response) {
                return response.data;
            }).catch(handleException);
        }

        function handleException(ex) {
            switch (ex && ex.status) {
                case 401:
                    httpExceptionFactory.unauthorized(ex);
                    break;

                case 403:
                    httpExceptionFactory.forbidden(ex);
                    break;

                case 500:
                    httpExceptionFactory.internalServerError(ex);
                    break;

                default:
                    httpExceptionFactory.unknownError(ex);
                    break;
            }
        }
    });
})(angular.module('project-x.order-taking'));