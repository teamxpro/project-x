'use strict';

(function (module) {
    module.controller('orderTakingProductSelectController', function ($scope, $toaster, sessionFactory, customerProductService, orderTakingService) {
        $scope.loading = false;
        $scope.keyword = null;
        $scope.selectedTerm = null;
        $scope.compositeItems = null;
        $scope.selecting = false;

        function loadCompositeItems() {
            if($scope.keyword) {
                $scope.loading = true;
                orderTakingService.getAllCompositeItems($scope.keyword).then(function(compositeItems){
                    $scope.loading = false;
                    $scope.compositeItems = compositeItems;
                }).catch(function(ex){
                    $scope.loading = false;
                    console.log(ex);
                });
            }else{
                $scope.compositeItems = null;
            }
        }

        $scope.onKeywordChange = function($event, keyword){
            if(!keyword){
                $scope.compositeItems = null;
            }
        };

        $scope.search = function($event){
            $event.preventDefault();
            loadCompositeItems();
        };

        $scope.select = function($event, compositeItem) {
            $event.preventDefault();

            if($scope.selecting) {
                //prevent doubleClick events
                return;
            }

            $scope.selecting = true;

            compositeItem.term = $scope.selectedTerm;
            $scope.$emit('mixItemSelected', compositeItem);
        };

        $scope.$on('reload-products', function(event, term){
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
            }

            $scope.loading = false;
            $scope.keyword = null;
            $scope.selectedTerm = term;
            $scope.compositeItems = null;
            $scope.selecting = false;
        });
    });
})(angular.module('project-x.order-taking'));