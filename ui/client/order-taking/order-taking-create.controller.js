'use strict';

(function (module) {
    module.controller('orderTakingCreateController', function ($scope, $toaster, $routeParams, $q, sessionFactory, customerService, customerProductService, orderTakingService) {
        $scope.customer = null;
        $scope.products = null;
        $scope.compositeItems = null;
        $scope.productItems = null;
        $scope.step = "step1";
        $scope.selectedProduct = null;
        $scope.numberOfTerms = 12;

        $scope.terms = [];
        $scope.isLoading = false;
        $scope.waiting = false;

        var customerId = $routeParams.customerId;

        var trackingStatus = {
            ordered: 1,
            loaded: 2,
            delivered: 3,
            returned: 4
        };

        function createTerms() {
            $scope.terms = [];

            for (var i = 0; i < $scope.numberOfTerms; i++) {
                $scope.terms.push({
                    id: (i + 1),
                    name: 'Term ' + (i + 1),
                    items: [],
                    hasFreeItem: false,
                    collectiveAmount : 0,
                    canEditCollectiveAmount : true
                });
            }
        }

        function loadCustomerAndProducts() {
            var promises = [];
            var customerPromise = customerService.getByCustomerId(customerId);
            var productPromise = customerProductService.getCustomerProductForCustomer(customerId);

            $scope.isLoading = true;

            promises = $q.all([customerPromise, productPromise]);

            promises.then(function (response) {
                if (angular.isArray(response)) {
                    $scope.customer = response[0];
                    $scope.products = response[1];
                }

                $scope.isLoading = false;
            }).catch(function (ex) {
                showErrorToaster('Unable to load data. Please try again');
                $scope.isLoading = false;
            });
        }

        function loadCustomProductItems(customerProductId, productId, successFn) {
            successFn = successFn || angular.noop;

            $scope.waiting = true;

            var promises = [];
            var productItemPromise = orderTakingService.getCustomProductItems(productId);
            var compositeItemDeliveryPromise = orderTakingService.getCustomerProductItemsDeliveryStatus(customerProductId);

            promises = $q.all([productItemPromise, compositeItemDeliveryPromise]);

            promises.then(function (response) {
                if (angular.isArray(response)) {
                    var compositeItems = response[0] ? response[0].compositeItems : [];
                    var compositeItemDeliveryStatus = response[1] ? response[1] : [];

                    $scope.compositeItems = compositeItems;
                    
                    angular.forEach(compositeItemDeliveryStatus, function(compositeItem) {
                        var term = $scope.terms[compositeItem.term - 1];
                        
                        term.canEditCollectiveAmount = !(compositeItem.status === trackingStatus.delivered || compositeItem.status === trackingStatus.loaded);

                        term.collectiveAmount = compositeItem.collectiveAmount ? compositeItem.collectiveAmount : $scope.selectedProduct.amount;

                        term.items.push(compositeItem);
                    });
                    
                    successFn();
                    $scope.waiting = false;
                }

                $scope.isLoading = false;
            }).catch(function (ex) {
                showErrorToaster('Unable to load data. Please try again');
                $scope.isLoading = false;
            });
        }

        function saveItemInTerm(termId, compositeItem) {
            $scope.waiting = true;

            orderTakingService.addProductItemToTerm(customerId, $scope.selectedProduct.id, $scope.selectedProduct.productId, termId, compositeItem.id, compositeItem.isFree)
            .then(function () {
                $scope.waiting = false;
            }).catch(function () {
                $scope.waiting = false;
                showErrorToaster('Unable to save item. Please try again');
            });
        };

        function removeItemFromTerm(trackingId, callback) {
            orderTakingService.removeProductItemFromTerm($scope.selectedProduct.productId, trackingId).then(function () {
                callback('success');
            }).catch(function () {
                callback('error');
                showErrorToaster('Unable to remove item. Please try again');
            });
        };

        function showSuccessToaster(message) {
            $toaster.pop({ message: message, type: 'success' });
        }

        function showErrorToaster(message) {
            $toaster.pop({ message: message, type: 'error' });
        }

        function showWarningToaster(message) {
            $toaster.pop({ message: message, type: 'warning' });
        }

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {

                createTerms();
                loadCustomerAndProducts();

            } else {
                sessionFactory.redirectToLoginPage();
            }
        };

        $scope.selectProduct = function (product) {
            if (!product.selected) {
                $scope.selectedProduct = product;

                angular.forEach($scope.products, function (p) {
                    p.selected = (p.productId === product.productId);
                });

                loadCustomProductItems(product.id, product.productId);

                $scope.step = "step2";
            }
        };

        $scope.selectAnotherProduct = function () {
            angular.forEach($scope.products, function (p) {
                p.selected = false;
            });

            $scope.selectedProduct = null;
            $scope.compositeItems = null;
            createTerms();
            $scope.step = "step1";
        };

        $scope.onDropProduct = function (event, ui) {
            var element = ui.draggable;
            var compositeItemId = parseInt(element.attr('composite-item-id'));
            var index = ui.droppable.index();
            var term = $scope.terms[index];

            var compositeItem = $scope.compositeItems.find(function (i) {
                return i.id === compositeItemId;
            });

            var newCompositeItem = angular.copy(compositeItem);

            newCompositeItem.status = trackingStatus.ordered;
            newCompositeItem.term = index + 1;
            term.items.push(newCompositeItem);

            if(term.items.length === 1) {
                //assign default amount
                term.collectiveAmount = $scope.selectedProduct.amount;
            }

            saveItemInTerm(term.id, newCompositeItem);
        };

        $scope.removeItemFromTerm = function (term, compositeItem, index) {
            if (compositeItem.status === trackingStatus.loaded || compositeItem.status === trackingStatus.delivered) {
                showWarningToaster('Unable to remove this item');
                return;
            }

            term.items.splice(index, 1);

            $scope.waiting = true;
            removeItemFromTerm(compositeItem.trackingId, function (status) {
                if (status === 'error') {
                    //reload to current state
                    $scope.selectProduct($scope.selectedProduct);
                }else{
                    if(term.items.length === 0){
                        term.collectiveAmount = 0;
                        orderTakingService.saveCollectiveAmount($scope.selectedProduct.id, term.id, 0);
                    }
                }

                $scope.waiting = false;
            });
        };

        $scope.showAddNewProductItemPopup = function($event, term){
            $event.preventDefault();
            
            $scope.$broadcast('reload-products', term);
            var addProductItemPopup = angular.element('#addProductItemPopup');
            addProductItemPopup.modal('show');
        };

        $scope.saveAmmountToBeCollected = function($event, termId, collectiveAmount) {
            var term = $scope.terms[termId - 1];
            if(term.items.length === 0){
                term.collectiveAmount = 0;
                showWarningToaster('No items added to this term. Please add items before adjust the amount');
                return;
            }

            if(!angular.isNumber(collectiveAmount)){
                if($event) {
                    $event.preventDefault();
                }

                term.collectiveAmount = $scope.selectedProduct.amount;
            }

            $scope.waiting = true;

            orderTakingService.saveCollectiveAmount($scope.selectedProduct.id, term.id, term.collectiveAmount).then(function(e){
                $scope.waiting = false;
                showSuccessToaster('New amount has been saved');
            }).catch(function(ex){
                $scope.waiting = false;
                showErrorToaster('Error saving amount.');
                term.collectiveAmount = $scope.selectedProduct.amount;
            });
        };

        $scope.$on('mixItemSelected', function(event, compositeItem) {
            var term = $scope.terms[compositeItem.term.id - 1];
            compositeItem.status = trackingStatus.ordered;

            if(term.items.length === 0){
                //assign default amount
                term.collectiveAmount = $scope.selectedProduct.amount;
            }

            term.items.push(compositeItem);
            
            saveItemInTerm(term.id, compositeItem);

            var addProductItemPopup = angular.element('#addProductItemPopup');
            addProductItemPopup.modal('hide');
        });
    });
})(angular.module('project-x.order-taking'));