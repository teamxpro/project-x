'use strict';

(function (module) {
    module.directive('customProductSummary', function ($templateCache, orderTakingService) {
        return {
            restrict: 'E',
            scope: {
                customProductId: '='
            },
            controller: function ($scope) {
                $scope.loading = true;

                $scope.payments = [];
                $scope.totalAmount = 0;
                $scope.collectedTotal = 0;
                $scope.gridAdapter = null;

                orderTakingService.getPaymentHistory($scope.customProductId)
                .then(function(payments){
                    angular.forEach(payments, function(payment){
                        $scope.payments.push({
                            term : payment.term,
                            collectiveAmount : payment.collectiveAmount,
                            collectedAmount : payment.collectedAmount,
                            date : payment.collectedDate ? new Date(payment.collectedDate).displayFormat() : ''
                        });

                        $scope.totalAmount += payment.collectiveAmount;
                        
                        if(angular.isNumber(payment.collectedAmount)){
                            $scope.collectedTotal += payment.collectedAmount;
                        }
                    });

                    $scope.loading = false;

                }).catch(function(ex){
                    console.log('Error', ex);
                    $scope.loading = false;
                });
            },
            template: $templateCache.get('order-taking/custom-product-summary.html')
        }
    });
})(angular.module('project-x.order-taking'));