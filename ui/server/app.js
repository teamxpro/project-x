var express = require('express'),
    path = require('path'),
    http = require('http'),
    url = require('url'),
    app = express(),
    configuration = require('../../core/config.js'),
    StoreCache = require('../../core/stores.js'),
    stores = new StoreCache();

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'public/views'));

app.get('/:store/', stores.isValidStore.bind(stores), (req, res) => {
    res.render('index',
        {
            store: req.params.store,
            googleMapApiKey: configuration.read().googleMapApiKey
        });
});

app.get('/service-vars/config.js', (req, res) => {
    var config = {
        store: req.params.store,
        application: {
            api: `http://${req.headers.host}/api`,
            uploadUrl: configuration.read().application.uploadUrl
        }
    };
    
    res.header("Content-Type", "application/javascript");
    var serviceVars = 'var serviceVars = ' + JSON.stringify(config);
    res.send(serviceVars);
});

module.exports = app;
