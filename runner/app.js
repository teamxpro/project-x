/* Jenkins Changes*/
var express = require('express'),
    compression = require('compression'),
    http = require('http'),
    bodyParser = require('body-parser'),
    path = require('path'),
    configuration = require('../core/config.js'),
    logger = require('../core/logger.js'),
    Authentication = require('../core/auth/authentication.js'),
    StoreCache = require('../core/stores.js'),
    stores = new StoreCache();

var app = express();
var auth = new Authentication(app);

app.use(compression());

app.use(bodyParser.urlencoded({
    extended: false
}));
// for parsing application/json
app.use(bodyParser.json());

// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.static('public'));

//set application view engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'public/views'));

app.set('superSecret', configuration.read().auth.secret);

app.get('/:store/status', stores.isValidStore.bind(stores), (req, res) => {
    res.send('OK');
});

function loadModules() {
    var packages = configuration.read().packages;

    packages.forEach((package) => {
        var module = require(`../${package.startUp}`);
        if (package.prefixRequired) {
            app.use(`/${package.name}`, module);
        } else {
            app.use(module);
        }

        logger.log(`${package.name} package attached`);

    }, this);
}

function init() {
    var port = configuration.read().application.port;
    var environment = 'development';

    if (['development', 'production'].indexOf(process.env.NODE_ENV) > -1) {
        environment = process.env.NODE_ENV;
    }

    if (process.env.PORT) {
        port = process.env.PORT;
    }

    app.set('env', environment);

    console.log(`Application is running on ${environment} environment`);

    loadModules();
    auth.init();

    //check database availability
    logger.log('Trying to connecto to database');
    var Database = require('../core/database.js'),
        db = new Database(configuration.read().db);

    db.open().then(() => {
        logger.log('Database connected');
    }).catch((err) => {
        logger.log('Unable to connect to database.\n' + err);
    });

    db.close();

    http.createServer(app).listen(port, () => {
        logger.log(port);
        logger.log('Server started');
    });
}

init();
