var environment = 'development';

if(['development','production'].indexOf(process.env.NODE_ENV) > -1){
    environment = process.env.NODE_ENV;
}

class Configuration {
    constructor(){
        this.configJson = require(`./config/${environment}/config.json`);
    }

    read() {
        return this.configJson;
    }
}

module.exports = new Configuration();