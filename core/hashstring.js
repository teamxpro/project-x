var Cryptography = require('crypto');
class HashString {
    constructor() {
        this.saltLength = 10;
    }

    createHash(password, salt) {
        var hash = this.md5(password + salt);
        return hash;
    }

    validateHash(hash, salt, password) {
        return hash === this.createHash(password, salt);
    }

    generateSalt() {
        var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ',
            setLen = set.length,
            salt = '';

        for (var i = 0; i < this.saltLength; i++) {
            var p = Math.floor(Math.random() * setLen);
            salt += set[p];
        }

        return salt;
    }

    md5(string) {
        return Cryptography.createHash('md5').update(string).digest('hex');
    }

}

module.exports = HashString;