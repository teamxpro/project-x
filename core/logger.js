class Logger {
    constructor(){
    }

    static log(message) {
        if(typeof message === 'object'){
            console.log(message);
            return;
        }

        console.log(JSON.stringify(message));
        
    }
}

module.exports = Logger;