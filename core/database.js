var mysql = require('mysql');
var Promise = require('promise');

class Database {
    constructor(config) {
        this.connection = mysql.createConnection(config);
    }

    open() {
        return new Promise((resolve, reject) => {
            this.connection.connect((err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve('Connection established');
                }
            });
        });
    }

    beginTransaction() {
        return new Promise((resolve, reject) => {
            this.connection.beginTransaction((err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve('Begin Transaction');
                }
            });
        });
    }

    rollback() {
        return new Promise((resolve, reject) => {
            this.connection.rollback((err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve('Transaction Rolledback');
                }
            });
        });
    }

    commit() {
        return new Promise((resolve, reject) => {
            this.connection.commit((err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve('Transaction Commited');
                }
            });
        });
    }

    query(statment, params) {
        return new Promise((resolve, reject) => {
            //escaping query values
            if (params) {
                params.forEach((param) => {
                    param = this.connection.escape(param);
                });
            }

            this.connection.query(statment, params, (err, recordSet) => {
                if (err) {
                    reject(err);
                } else {
                    var dst = new Dataset();
                    Object.assign(dst, recordSet);
                    resolve(dst);
                }
            });
        });
    }

    execute(statment, params) {
        return new Promise((resolve, reject) => {
            //escaping query values
            if (params) {
                params.forEach((param) => {
                    param = this.connection.escape(param);
                });
            }

            this.connection.query(statment, params, (err, recordSet) => {
                if (err) {
                    reject(err);
                } else {
                    var records = recordSet.length > 0 ? recordSet[0] : recordSet;

                    var dst = new Dataset();
                    Object.assign(dst, records);
                    resolve(dst);
                }
            });
        });
    }

    close() {
        return new Promise((resolve, reject) => {
            this.connection.end((err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve('Connection has been colsed');
                }
            });
        });
    }
}

class Dataset extends Array {
    constructor() {
        super()
    }

    hasRecords() {
        return this && this.length > 0;
    }

    first() {
        if (this.hasRecords()) {
            return this[0];
        }

        return null;
    }

    any(expression) {
        return this.map(expression);
    }
}

module.exports = Database;