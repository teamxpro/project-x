CREATE TABLE `lookupCategory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `storeId` INT NULL,
  `name` NVARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  INDEX `lookupCategory_store_idx` (`storeId` ASC),
  CONSTRAINT `lookupCategory_store`
    FOREIGN KEY (`storeId`)
    REFERENCES `store` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);
	
	
CREATE TABLE `lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storeId` int(11) DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lookup_store_idx` (`storeId`),
  KEY `lookup_category_idx` (`categoryId`),
  KEY `lookup_parent_idx` (`parentId`),
  CONSTRAINT `lookup_category` FOREIGN KEY (`categoryId`) REFERENCES `lookupCategory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lookup_parent` FOREIGN KEY (`parentId`) REFERENCES `lookup` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lookup_store` FOREIGN KEY (`storeId`) REFERENCES `store` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
