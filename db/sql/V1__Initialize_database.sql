CREATE TABLE `store` 
  ( 
     `id`       INT(11) NOT NULL auto_increment, 
     `name`     VARCHAR(50) DEFAULT NULL, 
     `isactive` TINYINT(1) DEFAULT '1', 
     PRIMARY KEY (`id`), 
     UNIQUE KEY `name_unique` (`name`) 
  ) 
engine=innodb 
auto_increment=1 
DEFAULT charset=utf8; 

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storeId` int(11) NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`storeId`,`name`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  CONSTRAINT `role_storeId` FOREIGN KEY (`storeId`) REFERENCES `store` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) 
ENGINE=InnoDB AUTO_INCREMENT=1 
DEFAULT CHARSET=latin1;

CREATE TABLE `user` 
  ( 
     `storeid`   INT(11) NOT NULL, 
     `username`  VARCHAR(50) NOT NULL, 
     `password`  VARCHAR(50) DEFAULT NULL, 
     `roleid`    INT(11) DEFAULT NULL, 
     `isactive`  TINYINT(1) DEFAULT NULL, 
     `lastlogin` DATETIME DEFAULT NULL, 
     PRIMARY KEY (`storeid`, `username`), 
     KEY `store_user_idx` (`storeid`), 
     KEY `user_role_idx` (`roleid`), 
     CONSTRAINT `store_user` FOREIGN KEY (`storeid`) REFERENCES `store` (`id`) 
     ON DELETE CASCADE ON UPDATE no action, 
     CONSTRAINT `user_role` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`) ON 
     DELETE no action ON UPDATE no action 
  ) 
engine=innodb 
DEFAULT charset=utf8; 





CREATE TABLE `permission` 
  ( 
     `id`     INT(11) NOT NULL auto_increment, 
     `roleid` INT(11) DEFAULT NULL, 
     `code`   VARCHAR(150) CHARACTER SET utf8 DEFAULT NULL, 
     PRIMARY KEY (`id`), 
     KEY `permission_role_idx` (`roleid`), 
     CONSTRAINT `permission_role` FOREIGN KEY (`roleid`) REFERENCES `role` (`id` 
     ) ON DELETE no action ON UPDATE no action 
  ) 
engine=innodb 
auto_increment=1 
DEFAULT charset=latin1; 


CREATE TABLE `customer` 
  ( 
     `id`               BIGINT(11) NOT NULL auto_increment, 
     `storeid`          INT(11) NOT NULL, 
     `lineid`           INT(11) DEFAULT NULL, 
     `customerid`       VARCHAR(15) NOT NULL, 
     `name`             VARCHAR(500) DEFAULT NULL, 
     `nic`              VARCHAR(13) DEFAULT NULL, 
     `email`            VARCHAR(45) DEFAULT NULL, 
     `location`         LONGTEXT, 
     `address`          VARCHAR(500) DEFAULT NULL, 
     `mobile1`          INT(10) DEFAULT NULL, 
     `mobile2`          INT(10) DEFAULT NULL, 
     `fixedline`        INT(10) DEFAULT NULL, 
     `emergencycontact` INT(10) DEFAULT NULL, 
     `contactname`      VARCHAR(45) DEFAULT NULL, 
     `language`         VARCHAR(45) DEFAULT NULL, 
     `otherinfo`        VARCHAR(500) DEFAULT NULL, 
     `latitude`         FLOAT DEFAULT NULL, 
     `longitude`        FLOAT DEFAULT NULL, 
     `imageurl`         LONGTEXT, 
     `isactive`         TINYINT(1) DEFAULT '1', 
     `isdeleted`        TINYINT(1) DEFAULT '0', 
     `createddate`      DATETIME DEFAULT NULL, 
     `updateddate`      DATETIME DEFAULT NULL, 
     PRIMARY KEY (`id`), 
     UNIQUE KEY `id_unique` (`id`), 
     UNIQUE KEY `customerid_unique` (`customerid`), 
     KEY `store_customer_idx` (`storeid`), 
     CONSTRAINT `store_customer` FOREIGN KEY (`storeid`) REFERENCES `store` ( 
     `id`) ON DELETE no action ON UPDATE no action 
  ) 
engine=innodb 
auto_increment=1 
DEFAULT charset=utf8; 

CREATE TABLE `line` 
  ( 
     `id`           INT(11) NOT NULL auto_increment, 
     `storeid`      INT(11) DEFAULT NULL, 
     `code`         VARCHAR(50) DEFAULT NULL, 
     `route`        VARCHAR(250) DEFAULT NULL, 
     `linedate`     VARCHAR(100) DEFAULT NULL, 
     `isactive`     TINYINT(1) DEFAULT '1', 
     `isdeleted`    TINYINT(1) DEFAULT '0', 
     `createddate`  DATETIME DEFAULT NULL, 
     `modifieddate` DATETIME DEFAULT NULL, 
     `linecol`      VARCHAR(45) DEFAULT NULL, 
     PRIMARY KEY (`id`), 
     UNIQUE KEY `id_unique` (`id`), 
     KEY `fk_line_store_idx` (`storeid`), 
     CONSTRAINT `fk_line_store` FOREIGN KEY (`storeid`) REFERENCES `store` (`id` 
     ) ON DELETE no action ON UPDATE no action 
  ) 
engine=innodb 
auto_increment=1 
DEFAULT charset=latin1; 

CREATE TABLE `supplier` 
  ( 
     `id`           BIGINT(11) NOT NULL auto_increment, 
     `supplierid`   VARCHAR(45) NOT NULL, 
     `storeid`      INT(11) DEFAULT NULL, 
     `categoryid`   INT(11) DEFAULT NULL, 
     `name`         VARCHAR(100) DEFAULT NULL, 
     `email`        VARCHAR(45) DEFAULT NULL, 
     `language`     VARCHAR(45) DEFAULT NULL, 
     `mobile`       INT(10) DEFAULT NULL, 
     `fixedline`    INT(10) DEFAULT NULL, 
     `address`      VARCHAR(500) DEFAULT NULL, 
     `agentname`    VARCHAR(45) DEFAULT NULL, 
     `agentcontact` INT(10) DEFAULT NULL, 
     `otherinfo`    VARCHAR(500) DEFAULT NULL, 
     `imageurl`     LONGTEXT, 
     `isactive`     TINYINT(1) DEFAULT '1', 
     `isdeleted`    TINYINT(1) DEFAULT '0', 
     `createddate`  DATETIME DEFAULT NULL, 
     `updateddate`  DATETIME DEFAULT NULL, 
     PRIMARY KEY (`id`), 
     UNIQUE KEY `id_unique` (`id`), 
     UNIQUE KEY `supplierid_unique` (`supplierid`) 
  ) 
engine=innodb 
auto_increment=1
DEFAULT charset=latin1; 


CREATE TABLE `sellingMarginUnit` (
  `sellingMarginUnitId` int(11) NOT NULL auto_increment,
  `marginUnitType` varchar(45) DEFAULT NULL,
  `marginUnitTypeDescription` varchar(100) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`sellingMarginUnitId`)
) 
engine=innodb 
auto_increment=1
DEFAULT charset=latin1; 


CREATE TABLE `itemCategory` 
  ( 
     `catid`       INT(11) NOT NULL auto_increment, 
     `storeid`     INT(11) NOT NULL, 
     `name`        VARCHAR(45) DEFAULT NULL, 
     `description` VARCHAR(250) DEFAULT NULL, 
     `isactive`    TINYINT(1) DEFAULT NULL, 
     `createddate` DATETIME DEFAULT NULL, 
     `updateddate` DATETIME DEFAULT NULL, 
     PRIMARY KEY (`catid`, `storeid`), 
     KEY `fk_itemcategory_store_idx` (`storeid`), 
     CONSTRAINT `fk_itemcategory_store` FOREIGN KEY (`storeid`) REFERENCES 
     `store` (`id`) ON DELETE no action ON UPDATE no action 
  ) 
engine=innodb 
auto_increment=1 
DEFAULT charset=latin1; 


CREATE TABLE `itemSubCategory` (
  `subCatId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`subCatId`),
  KEY `FK_CategoryId_idx` (`categoryId`),
  CONSTRAINT `FK_CategoryId` FOREIGN KEY (`categoryId`) REFERENCES `itemCategory` (`catId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) 
engine=innodb 
auto_increment=1 
DEFAULT charset=latin1; 


CREATE TABLE `item` (
    `storeId` INT(11) NOT NULL,
    `itemId` INT(128) NOT NULL,
    `name` VARCHAR(45) NOT NULL,
    `description` VARCHAR(250) DEFAULT NULL,
    `manufacturer` VARCHAR(100) DEFAULT NULL,
    `model` VARCHAR(45) DEFAULT NULL,
    `reorderLevel` INT(32) DEFAULT NULL,
    `unit` VARCHAR(45) DEFAULT NULL,
    `categoryId` INT(11) DEFAULT NULL,
    `subCategoryId` INT(11) DEFAULT NULL,
    `image` VARCHAR(100) DEFAULT NULL,
    `isActive` TINYINT(1) DEFAULT NULL,
    `isDeleted` TINYINT(1) DEFAULT NULL,
    `createdDate` DATETIME DEFAULT NULL,
    `updatedDate` DATETIME DEFAULT NULL,
    PRIMARY KEY (`itemId` , `storeId`),
    KEY `FK_CategoryId_idx` (`categoryId`),
    KEY `FK_SubCategoryId_idx` (`subCategoryId`),
    KEY `FK_CategoryId_idx_items` (`categoryId`),
    KEY `FK_SubCategoryId_Items_idx_items` (`subCategoryId`),
    KEY `FK_Item_Srore_idx` (`storeId`),
    CONSTRAINT `FK_CategoryId_Items_to_Categories` FOREIGN KEY (`categoryId`)
        REFERENCES `itemCategory` (`catId`)
        ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_Item_Srore` FOREIGN KEY (`storeId`)
        REFERENCES `store` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_SubCategoryId_Items_to_SubCategories` FOREIGN KEY (`subCategoryId`)
        REFERENCES `itemSubCategory` (`subCatId`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1; 

CREATE TABLE `inventory` (
  `inventoryId` int(128) NOT NULL AUTO_INCREMENT,
  `itemId` int(64) DEFAULT NULL,
  `storeId` int(11) DEFAULT NULL,
  `itemName` varchar(45) DEFAULT NULL,
  `itemImageUrl` varchar(100) DEFAULT NULL,
  `quantity` int(32) DEFAULT NULL,
  `buyingPrice` double DEFAULT NULL,
  `defaultMargin` float DEFAULT NULL,
  `marginUnit` int(11) DEFAULT NULL,
  `retailSellingPrice` double DEFAULT NULL,
  `wholeSellingPrice` double DEFAULT NULL,
  `easyPaymentSellingPrice` double DEFAULT NULL,
  `remainingQuantity` int(32) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `updatedBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`inventoryId`),
  KEY `FK_Inventory_to_margin_marginId_idx` (`marginUnit`),
  KEY `FK_Inventory_to_Items_idx` (`itemId`),
  KEY `FK_Inventory_to_store_idx` (`storeId`),
  CONSTRAINT `FK_Inventory_to_Items` FOREIGN KEY (`itemId`) REFERENCES `item` (`itemId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Inventory_to_margin_marginId` FOREIGN KEY (`marginUnit`) REFERENCES `master_sellingMarginUnit` (`sellingMarginUnitId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Inventory_to_store` FOREIGN KEY (`storeId`) REFERENCES `store` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--insert default data
INSERT INTO `store` (`name`,`isActive`) VALUES ('demo',1);
INSERT INTO `store` (`name`,`isActive`) VALUES ('Golden',1);

INSERT INTO `role` (`storeId`,`name`) VALUES (1,'administrator');
INSERT INTO `role` (`storeId`,`name`) VALUES (1,'basicuser');
INSERT INTO `role` (`storeId`,`name`) VALUES (1,'manager');
INSERT INTO `role` (`storeId`,`name`) VALUES (2,'administrator');
INSERT INTO `role` (`storeId`,`name`) VALUES (2,'basicuser');
INSERT INTO `role` (`storeId`,`name`) VALUES (2,'manager');

INSERT INTO `permission` (`roleId`,`code`) VALUES (1,'CUSTOMERS_ADD');
INSERT INTO `permission` (`roleId`,`code`) VALUES (1,'CUSTOMERS_SEARCH');
INSERT INTO `permission` (`roleId`,`code`) VALUES (1,'CUSTOMERS_EDIT');
INSERT INTO `permission` (`roleId`,`code`) VALUES (1,'CUSTOMERS_VIEW_DETAILS');
INSERT INTO `permission` (`roleId`,`code`) VALUES (1,'CUSTOMERS_DELETE');
INSERT INTO `permission` (`roleId`,`code`) VALUES (1,'CUSTOMERS_CHANGE_STATUS');

INSERT INTO `user` (`storeId`,`username`,`password`,`roleId`,`isActive`,`lastLogin`) VALUES (1,'administrator','123',1,1,'2017-03-02 16:26:23');
INSERT INTO `user` (`storeId`,`username`,`password`,`roleId`,`isActive`,`lastLogin`) VALUES (2,'administrator','123',4,1,'2017-02-24 09:01:50');

--stored procedures

DELIMITER $$ 
CREATE definer=`root`@`%` 
PROCEDURE `authenticate`(IN $store    VARCHAR(50), 
                         IN $username nvarchar(50), 
                         IN $password nvarchar(50)) 
begin 
  DECLARE $storeid INT DEFAULT 0;
  
  SELECT id 
  INTO   $storeid 
  FROM   store 
  WHERE  name = $store;

  SELECT * 
  FROM   user 
  WHERE  storeid = $storeid 
  AND    username = $username 
  AND    password = $password;
  
end$$ 
delimiter ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `createCustomer`(

IN $storeId INT(11),
IN $lineId INT(11),
IN $name VARCHAR(500),
IN $nic VARCHAR(13),
IN $email varchar(45),
IN $address VARCHAR(500),
IN $mobile1 INT(10),
IN $mobile2 INT(10),
IN $fixedLine INT(10),
IN $emergencyContact INT(10),
IN $contactName VARCHAR(45),
IN $language VARCHAR(45),
IN $latitude FLOAT,
IN $longitude FLOAT,
IN $otherInfo VARCHAR(500),
IN $imageUrl LONGTEXT
)
BEGIN
	
    DECLARE $customerId VARCHAR(15);
    DECLARE $newID int;
    
    SET $newID = (SELECT coalesce(MAX(id),0) + 1 FROM `customer`);
    
    SET $customerId = CONCAT('CS-', YEAR(NOW()), '-', CONVERT($newId, CHAR));

	INSERT INTO `customer`
		(`customerId`, `storeId`, `lineId`, `name`, `nic`, `email`, `address`, `mobile1`, `mobile2`, 
		`fixedLine`,`emergencyContact`,`contactName`,`language`, `otherInfo`, `imageUrl`, `latitude`, `longitude`, `isActive`, `createdDate`, `updatedDate`)
	VALUES
		($customerId, $storeId, $lineId, $name, $nic, $email, $address, $mobile1, $mobile2,
		$fixedLine, $emergencyContact, $contactName, $language, $otherInfo, $imageUrl, $latitude, $longitude, 1, NOW(), NOW() );

	SELECT $customerId;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `createInventoryItem`(
    IN $username VARCHAR(55),
    IN $storeId INT(11),
	IN $itemId INT(64),
	IN $itemName VARCHAR(45),
	IN $itemImageUrl VARCHAR(100),
	IN $quantity INT(32),
	IN $buyingPrice DOUBLE,
	IN $defaultMargin FLOAT,
	IN $marginUnit INT(11),
	IN $retailSellingPrice DOUBLE,
	IN $wholeSellingPrice DOUBLE,
	IN $easyPaymentSellingPrice DOUBLE
)
BEGIN
DECLARE $inventoryID int;
INSERT INTO `inventory`
(
	`itemId`,
    `storeId`,
	`itemName`,
	`itemImageUrl`,
	`quantity`,
	`buyingPrice`,
	`defaultMargin`,
	`marginUnit`,
	`retailSellingPrice`,
	`wholeSellingPrice`,
	`easyPaymentSellingPrice`,
    `remainingQuantity`,
	`createdDate`,
	`updatedDate`,
    `createdBy`,
    `updatedBy`
)
VALUES
(
	$itemId,
    $storeId,
	$itemName,
	$itemImageUrl,
	$quantity,
	$buyingPrice,
	$defaultMargin,
	$marginUnit,
	$retailSellingPrice,
	$wholeSellingPrice,
	$easyPaymentSellingPrice,
    $quantity,
	NOW(),
	NOW(),
    $username,
    $username
);

SET $inventoryID = LAST_INSERT_ID();
SELECT $inventoryID;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `createItem`(
IN $storeId int,
IN $name varchar(45), 
IN $description varchar(250), 
IN $manufacturer varchar(100), 
IN $model varchar(45), 
IN $reorderLevel int, 
IN $unit varchar(45), 
IN $category int,
IN $subCategory int, 
IN $image varchar(100)
)
BEGIN

DECLARE $itemId int;
SET $itemId = (SELECT coalesce(MAX(itemId),0) + 1 FROM item);

INSERT INTO `item`
(`itemId`,
`storeId`,
`name`,
`description`,
`manufacturer`,
`model`,
`reorderLevel`,
`unit`,
`categoryId`,
`subCategoryId`,
`image`,
`isActive`,
`isDeleted`,
`createdDate`,
`updatedDate`
)
VALUES
($itemId,
$storeId,
$name,
$description,
$manufacturer,
$model,
$reorderLevel,
$unit,
$category,
$subCategory,
$image,
1,0,
NOW(),
NOW()
);

SELECT $itemId;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `createLine`(
	IN $storeId INT(11),
    IN $route VARCHAR(250),
	IN $code VARCHAR(100),
	IN $lineDate VARCHAR(100)
)
BEGIN
DECLARE $lineID int;

INSERT INTO line
(
    `storeId`,
	`route`,
	`code`,
	`lineDate`,
    `createdDate`,
    `modifiedDate`
)
VALUES
(
    $storeId,
	$route,
	$code,
	$lineDate,
    NOW(),
    NOW()
);

SET $lineID = LAST_INSERT_ID();
SELECT $lineID;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `createNewItem`(
IN $itemId int, 
IN $itemName varchar(45), 
IN $itemDescription varchar(250), 
IN $itemManufacturer varchar(100), 
IN $itemModel varchar(45), 
IN $reoderLevel int, 
IN $itemUnit varchar(45), 
IN $itemCategory int, 
IN $itemSubCategory int, 
IN $itemImage varchar(100), 
IN $ItemSupplier int,
IN $storeId INT(11)
)
BEGIN
INSERT INTO `item`
(`itemId`,
`itemName`,
`itemDescription`,
`itemManufacturer`,
`itemModel`,
`reorderLevel`,
`itemUnit`,
`itemCategory`,
`itemSubCategory`,
`itemImage`,
`itemSupplier`,
`createdDate`,
`updatedDate`, 
`storeId`, 
`createdDate`, 
`updatedDate`
)
VALUES
($itemId,
$itemName,
$itemDescription,
$itemManufacturer,
$itemModel,
$reorderLevel,
$itemUnit,
$itemCategory,
$itemSubCategory,
$itemImage,
$itemSupplier,
$createdDate,
$updatedDate,
$storeId,
NOW(),
NOW()
);

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `createSupplier`(

IN $storeId INT(11),
IN $name VARCHAR(500),
IN $email varchar(45),
IN $language VARCHAR(45),
IN $mobile INT(10),
IN $fixedLine INT(10),
IN $address VARCHAR(500),
IN $agentName VARCHAR(45),
IN $agentContact INT(10),
IN $categoryId INT(11),
IN $otherInfo VARCHAR(500),
IN $imageUrl LONGTEXT
)
BEGIN
	
    DECLARE $supplierId VARCHAR(15);
    DECLARE $newID int;
    
    SET $newID = (SELECT coalesce(MAX(id),0) + 1 FROM supplier);
    
    SET $supplierId = CONCAT('SP-', YEAR(NOW()), '-', CONVERT($newId, CHAR));

	INSERT INTO supplier
		(`supplierId`, `storeId`, `name`, `email`, `language`, `mobile`, `fixedLine`,`address`,`agentName`,
        `agentContact`, `categoryId`, `otherInfo`, `imageUrl`, `isActive`, `createdDate`, `updatedDate`)
	VALUES
		($supplierId, $storeId, $name, $email, $language, $mobile, $fixedLine, $address, $agentName,
		$agentContact, $categoryId, $otherInfo, $imageUrl, 1, NOW(), NOW() );

	SELECT $supplierId;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `deleteInventoryItem`(
	IN $inventoryId INT(128)
)
BEGIN

DELETE FROM `inventory`
WHERE $inventoryId;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `deleteItem`(IN $itemId int)
BEGIN
UPDATE `item`
SET
`isDeleted` = 1
WHERE `itemId` = $itemId;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `deleteLines`(IN $ids varchar(1000))
BEGIN
	UPDATE line SET isDeleted = 1
    WHERE  find_in_set(id, $ids);
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getActiveItemCategories`(IN $storeId INT(11))
BEGIN
	SELECT catId, name, description
    FROM itemCategory
    WHERE storeId = $storeId AND isActive = 1;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getActiveItemSubCategories`(IN $storeId INT(11))
BEGIN
	SELECT subCatId, name, description, isActive, categoryId
    FROM subCategory
    WHERE storeId = $storeId;
    
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getAllActiveLines`(IN $storeId INT(11))
BEGIN

	SELECT	*
    FROM	line
    WHERE	isActive = 1 AND storeId = $storeId;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getAllItemCategories`(IN $storeId INT(11))
BEGIN
	SELECT catId, name, description, isActive
    FROM itemCategory
    WHERE storeId = $storeId;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getAllItems`(IN $storeId int)
BEGIN
   SELECT 
    `item`.`storeId`,
    `item`.`id`,
    `item`.`name`,
    `item`.`description`,
    `item`.`manufacturer`,
    `item`.`model`,
    `item`.`reorderLevel`,
    `item`.`unit`,
    `item`.`categoryId`,
    `item`.`subCategoryId`,
    `item`.`image`,
    `item`.`isActive`,
    `item`.`isDeleted`,
    `item`.`createdDate`,
    `item`.`updatedDate`
	FROM `item`
    WHERE isActive = 1 AND storeId = $storeId;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getAllItemSubCategories`(IN $storeId INT(11))
BEGIN
	SELECT subCatId, subCategoryName, subCategoryDescription, isActive, categoryId 
    FROM itemSubCategories
    WHERE storeId = $storeId;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getCustomerByCustomerId`( IN $customerId varchar(15),IN $storeId INT(11))
BEGIN
	
    SELECT 	c.*, ln.id AS 'lineId', ln.route AS 'lineRoute', ln.lineDate AS 'lineDate'
    FROM 	customer c LEFT JOIN Line ln ON c.lineId = ln.id
    WHERE 	c.customerId = $customerId AND c.isDeleted = 0 AND storeId = $storeId;
    
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getInventoryItemById`(IN $storeId INT(11), IN $inventoryId INT(128))
BEGIN

SELECT `inventory`.`inventoryId`,
    `inventory`.`itemId`,
    `inventory`.`itemName`,
    `inventory`.`itemImageUrl`,
    `inventory`.`quantity`,
    `inventory`.`buyingPrice`,
    `inventory`.`defaultMargin`,
    `inventory`.`marginUnit`,
    `inventory`.`retailSellingPrice`,
    `inventory`.`wholeSellingPrice`,
    `inventory`.`easyPaymentSellingPrice`,
    `inventory`.`storeId`,
    `inventory`.`createdDate`,
    `inventory`.`updatedDate`,
    `item`.`name` as item
FROM `inventory`
LEFT OUTER JOIN `item` ON inventory.itemId = item.itemId
WHERE inventory.storeId = $storeId AND inventory.inventoryId = $inventoryId;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getItemById`(IN $itemId int, IN $storeId INT(11))
BEGIN
	
    SELECT  	i.*, ic.name AS 'categoryName', isc.name AS 'subCategoryName'
	FROM		item i
				LEFT JOIN itemCategory ic ON i.categoryId = ic.catId
				LEFT JOIN itemSubCategory isc ON ic.catId = isc.categoryId
    WHERE 		i.itemId = $itemId AND i.isDeleted = 0 AND i.storeId = $storeId;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getItemSubCategoriesByCategoryId`( IN $categoryId Int, IN $storeId INT(11))
BEGIN
	SELECT subCatId, name, description, isActive, categoryId 
    FROM itemSubCategory
    WHERE categoryId= $categoryId AND isActive = 1;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getLineById`(IN $id INT, IN $storeId INT(11))
BEGIN

SElECT * FROM line where id = $id AND storeId = $storeId;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getSupplierBySupplierId`( IN $supplierId varchar(15), IN $storeId INT(11) )
BEGIN
	
    SELECT 	s.*
    FROM 	supplier s
    WHERE 	s.supplierId = $supplierId AND s.isDeleted = 0 AND storeId = $storeId;
    
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `getUser`(IN $store varchar(50), IN $username nvarchar(50), IN $updateLastLogin boolean)
BEGIN
   DECLARE $storeId INT DEFAULT 0;
   
	SELECT 
    id
INTO $storeId FROM
    store
WHERE
    name = $store;
	
SELECT 
    u.storeId,
    u.username,
    u.password,
    r.name AS 'role',
    GROUP_CONCAT(p.code) AS 'permissions',
    u.isActive,
    u.lastLogin
FROM
    user u
        LEFT JOIN
    role r ON u.roleId = r.id
        LEFT JOIN
    permission p ON p.roleId = u.roleId
WHERE
    u.storeId = $storeId
        AND u.username = $username;

    IF ($updateLastLogin = true) THEN
		UPDATE user u SET u.lastLogin = now()
        WHERE u.storeId = $storeId AND u.username = $username;
	END IF;
   
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `searchAllCustomers`(IN $storeId int, IN $keyword nvarchar(50), IN $pageIndex int, IN $pageSize int, OUT $total int)
BEGIN

	DECLARE $pagingStart INT DEFAULT 1;
    
    -- Set default parameters
    IF ($pageIndex < 1) THEN
		SET $pageIndex = 1;
	END IF;
    
    IF ($pageSize < 1) THEN
		SET $pageSize = 1;
	END IF;
    
    SET $pagingStart = ($pageIndex -1)  * $pageSize;
    
	-- Get matching record counts
    SELECT COUNT(id) INTO $total FROM customer
	WHERE	isDeleted = 0
			AND storeId = $storeId
            AND (((customerId LIKE CONCAT($keyword, '%')) OR $keyword is null)
            OR ((name LIKE CONCAT($keyword, '%')) OR $keyword is null)
            OR ((nic LIKE CONCAT($keyword, '%')) OR $keyword is null));
    
    -- Search customers
    SELECT c.*, ln.id AS 'lineId', ln.route AS 'lineRoute', ln.lineDate AS 'lineDate'
    FROM customer c LEFT JOIN line ln ON c.lineId = ln.id
	WHERE	c.isDeleted = 0
			AND c.storeId = $storeId
            AND (((c.customerId LIKE CONCAT($keyword, '%')) OR $keyword is null)
            OR ((c.name LIKE CONCAT($keyword, '%')) OR $keyword is null)
            OR ((c.nic LIKE CONCAT($keyword, '%')) OR $keyword is null))
            
	ORDER BY c.id
	LIMIT $pagingStart , $pageSize;
    
   END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `searchAllItems`(IN $storeId int, IN $keyword nvarchar(50), IN $pageIndex int, IN $pageSize int, OUT $total int)
BEGIN
    DECLARE $pagingStart INT DEFAULT 1;
    
    -- Set default parameters
    IF ($pageIndex < 1) THEN
		SET $pageIndex = 1;
	END IF;
    
    IF ($pageSize < 1) THEN
		SET $pageSize = 1;
	END IF;
    
    SET $pagingStart = ($pageIndex -1)  * $pageSize;
    
-- Get matching record counts
SELECT	COUNT(i.itemId) INTO $total
FROM		item i 
			LEFT JOIN itemCategory ic ON i.categoryId = ic.catId
			LEFT JOIN itemSubCategory isc ON ic.catId = isc.categoryId
WHERE		i.storeId = $storeId AND i.isDeleted = 0 AND
			(((i.name LIKE CONCAT($keyword, '%')) OR $keyword IS NULL)
			OR ((i.manufacturer LIKE CONCAT($keyword, '%')) OR $keyword IS NULL));


SELECT  	i.*, ic.name AS 'categoryName', isc.name AS 'subCategoryName'
FROM		item i 
			LEFT JOIN itemCategory ic ON i.categoryId = ic.catId
			LEFT JOIN itemSubCategory isc ON ic.catId = isc.categoryId
WHERE		i.storeId = $storeId AND i.isDeleted = 0 AND
			(((i.name LIKE CONCAT($keyword, '%')) OR $keyword IS NULL)
			OR ((i.manufacturer LIKE CONCAT($keyword, '%')) OR $keyword IS NULL))
ORDER BY 	i.itemId
LIMIT 		$PAGINGSTART , $PAGESIZE;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `searchAllSuppliers`(IN $storeId int, IN $keyword nvarchar(50), IN $pageIndex int, IN $pageSize int, OUT $total int)
BEGIN

	DECLARE $pagingStart INT DEFAULT 1;
    
    -- Set default parameters
    IF ($pageIndex < 1) THEN
		SET $pageIndex = 1;
	END IF;
    
    IF ($pageSize < 1) THEN
		SET $pageSize = 1;
	END IF;
    
    SET $pagingStart = ($pageIndex -1)  * $pageSize;
    
	-- Get matching record counts
SELECT 
    COUNT(id)
INTO $total FROM
    supplier
WHERE
    isDeleted = 0 AND storeId = $storeId
        AND (((supplierId LIKE CONCAT($keyword, '%'))
        OR $keyword IS NULL)
        OR ((name LIKE CONCAT('%', $keyword, '%'))
        OR $keyword IS NULL));
    
    -- Search customers
SELECT 
    s.*
FROM
    supplier s
WHERE
    s.isDeleted = 0 AND s.storeId = $storeId
        AND (((s.supplierId LIKE CONCAT($keyword, '%'))
        OR $keyword IS NULL)
        OR ((s.name LIKE CONCAT('%', $keyword, '%'))
        OR $keyword IS NULL))
ORDER BY s.id
LIMIT $PAGINGSTART , $PAGESIZE;
    
   END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `searchLines`(IN $storeId INT(11), IN $keyword nvarchar(50), IN $pageIndex int, IN $pageSize int, OUT $total int)
BEGIN
    DECLARE $pagingStart INT DEFAULT 1;
    
    -- Set default parameters
    IF ($pageIndex < 1) THEN
		SET $pageIndex = 1;
	END IF;
    
    IF ($pageSize < 1) THEN
		SET $pageSize = 1;
	END IF;
    
    SET $pagingStart = ($pageIndex -1)  * $pageSize;
    
	-- Get matching record counts
SELECT 
    COUNT(id)
INTO $total FROM
    line
WHERE
    (((route LIKE CONCAT($keyword, '%'))
        OR $keyword IS NULL)
        OR ((code LIKE CONCAT($keyword, '%'))
        OR $keyword IS NULL))
        AND isDeleted = 0
        AND storeId = $storeId;

   -- Search lines
SELECT 
    `line`.`id`,
    `line`.`storeId`,
    `line`.`code`,
    `line`.`route`,
    `line`.`lineDate`,
    `line`.`isActive`,
    `line`.`isDeleted`,
    `line`.`createdDate`,
    `line`.`modifiedDate`,
    `line`.`linecol`
FROM
    line
WHERE
    (((route LIKE CONCAT($keyword, '%'))
        OR $keyword IS NULL)
        OR ((code LIKE CONCAT($keyword, '%'))
        OR $keyword IS NULL))
        AND isDeleted = 0
        AND storeId = $storeId
ORDER BY route
LIMIT $PAGINGSTART , $PAGESIZE;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `updateCustomer`(
IN $customerId varchar(15),
IN $storeId INT(11),
IN $lineId INT(11),
IN $name VARCHAR(500),
IN $nic VARCHAR(13),
IN $email varchar(45),
IN $address VARCHAR(500),
IN $mobile1 INT(10),
IN $mobile2 INT(10),
IN $fixedLine INT(10),
IN $emergencyContact INT(10),
IN $contactName VARCHAR(45),
IN $language VARCHAR(45),
IN $latitude FLOAT,
IN $longitude FLOAT,
IN $otherInfo VARCHAR(500),
IN $imageUrl LONGTEXT

)
BEGIN

	UPDATE 	`customer`
    SET 	`storeId` = $storeId,
			`lineId` = $lineId,
			`name` = $name,			
			`nic` = $nic, 
			`email` = $email,
			`address` = $address,
            `mobile1` = $mobile1,
            `mobile2` = $mobile2,
            `fixedLine` = $fixedLine,
            `emergencyContact` = $emergencyContact,
            `contactName` = $contactName,
            `language` = $language,
            `latitude` =  $latitude,
            `longitude` = $longitude,
			`otherInfo` = $otherInfo,
            `imageUrl` = $imageUrl,
			`updatedDate` = NOW()
    WHERE	`customerId` = $customerId;
    
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `updateCustomerActiveStatus`(

IN $customerIds varchar(1000),
IN $isActive TINYINT(1)

)
BEGIN
	/*
    DECLARE $customers varchar(500);
    
    SET $customers = (SELECT CONCAT("'", REPLACE($customerIds, ',', "','"), "'"));

    SET @query = CONCAT('UPDATE customer SET isActive = ', $isActive, ', updatedDate = NOW() WHERE customerId IN (', $customers, ')'); 

	PREPARE stmt FROM @query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	*/
    
    UPDATE customer SET isActive = $isActive, updatedDate = NOW()
    WHERE  find_in_set(customerId, $customerIds);
    
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `updateCustomerDeleteStatus`(

IN $customerIds varchar(1000)

)
BEGIN
	/*
    DECLARE $customers varchar(500);
    
    SET $customers = (SELECT CONCAT("'", REPLACE($customerIds, ',', "','"), "'"));

    SET @query = CONCAT('UPDATE customer SET isDeleted = 1, isActive = 0, updatedDate = NOW() WHERE customerId IN (', $customers, ')'); 

	PREPARE stmt FROM @query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
    */
    
    UPDATE customer SET isDeleted = 1, isActive = 0, updatedDate = NOW()
    WHERE  find_in_set(customerId, $customerIds);
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `updateInventoryItem`(
    IN $inventoryId INT(128),
    IN $username VARCHAR(55),
    IN $storeId INT(11),
	IN $itemId INT(64),
	IN $itemName VARCHAR(45),
	IN $itemImageUrl VARCHAR(100),
	IN $quantity INT(32),
	IN $buyingPrice DOUBLE,
	IN $defaultMargin FLOAT,
	IN $marginUnit INT(11),
	IN $retailSellingPrice DOUBLE,
	IN $wholeSellingPrice DOUBLE,
	IN $easyPaymentSellingPrice DOUBLE
)
BEGIN
	UPDATE `inventory`
	SET
	`itemId` = $itemId,
	`itemName` = $itemName,
	`itemImageUrl` = $itemImageUrl,
	`quantity` = $quantity,
	`buyingPrice` = $buyingPrice,
	`defaultMargin` = $defaultMargin,
	`marginUnit` = $marginUnit,
	`retailSellingPrice` = $retailSellingPrice,
	`wholeSellingPrice` = $wholeSellingPrice,
	`easyPaymentSellingPrice` = $easyPaymentSellingPrice,
	`updatedDate` = NOW(),
    `updatedBy` = $username
	WHERE `inventoryId` = $inventoryId AND `storeId` = $storeId;
END$$
DELIMITER ;

DELIMITER $$

CREATE DEFINER=`root`@`%` PROCEDURE `searchInventory`(IN $storeId INT(11), IN $itemId INT(64), IN $keyword nvarchar(50), IN $startDate DATETIME, IN $endDate DATETIME, IN $pageIndex int, IN $pageSize int, OUT $total int)
BEGIN

	DECLARE $pagingStart INT DEFAULT 1;
    
    -- Set default parameters
    IF ($pageIndex IS NULL OR $pageIndex < 1) THEN
		SET $pageIndex = 1;
	END IF;
    
    IF ($pageSize  IS NULL OR $pageSize < 1) THEN
		SET $pageSize = 10;
	END IF;
    
    SET $pagingStart = ($pageIndex -1)  * $pageSize;
    
    SELECT 
	  COUNT(inventory.inventoryId)
	INTO $total
	FROM inventory INNER JOIN item
           ON inventory.itemId = item.itemId
	WHERE 
		(item.itemId = $itemId OR $itemId IS NULL) 
        AND 
		 (
		   $keyword is null OR $keyword = '' OR item.name like CONCAT($keyword, '%') OR inventory.createdBy like CONCAT($keyword, '%')
		   OR inventory.updatedBy like CONCAT($keyword, '%')
		 )
	   AND
        ($startDate IS NULL OR inventory.createdDate > $startDate)
       AND
        ($endDate IS NULL OR inventory.createdDate < $endDate)
       AND
	    item.storeId = $storeId;
       
       SELECT 
		  inventory.inventoryId,
		  inventory.storeId,
		  inventory.quantity,
		  inventory.buyingPrice,
		  inventory.defaultMargin,
		  inventory.marginUnit,
		  inventory.createdBy,
		  inventory.createdDate,
		  inventory.updatedBy,
		  inventory.updatedDate,
		  item.name as itemname,
		  item.itemId,
		  item.description
	FROM inventory INNER JOIN item
           ON inventory.itemId = item.itemId
	WHERE 
		(item.itemId = $itemId OR $itemId IS NULL) 
        AND 
		 (
		   $keyword is null OR $keyword = '' OR item.name like CONCAT($keyword, '%') OR inventory.createdBy like CONCAT($keyword, '%')
		   OR inventory.updatedBy like CONCAT($keyword, '%')
		 )
		AND
         ($startDate IS NULL OR inventory.createdDate > $startDate)
        AND
         ($endDate IS NULL OR inventory.createdDate < $endDate)
	    AND
	     item.storeId = $storeId
	ORDER BY inventory.createdDate DESC
    LIMIT $PAGINGSTART , $PAGESIZE;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `updateItem`(
IN $itemId int,
IN $storeId INT(11),
IN $name varchar(45), 
IN $description varchar(250), 
IN $manufacturer varchar(100), 
IN $model varchar(45), 
IN $reorderLevel int, 
IN $unit varchar(45), 
IN $category int, 
IN $subCategory int, 
IN $image varchar(100)
)
BEGIN
UPDATE `item`
SET
`name` = $name,
`description` = $description,
`manufacturer` = $manufacturer,
`model` = $model,
`reorderLevel` = $reorderLevel,
`unit` = $unit,
`categoryId` = $category,
`subCategoryId` = $subCategory,
`image` = $image,
`updatedDate` = NOW()

WHERE `itemId` = $itemId AND storeId = $storeId;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `updateItemActiveStatus`(

IN $itemIds varchar(1000),
IN $isActive TINYINT(1)

)
BEGIN

    UPDATE item SET isActive = $isActive, updatedDate = NOW()
    WHERE  find_in_set(itemId, $itemIds);
    
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `updateItemDeleteStatus`(

IN $itemIds varchar(1000)

)
BEGIN

    UPDATE item SET isDeleted = 1, isActive = 0, updatedDate = NOW()
    WHERE  find_in_set(itemId, $itemIds);
    
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `updateLine`(
	IN $id INT(11),
	IN $route VARCHAR(250),
	IN $code VARCHAR(100),
	IN $lineDate VARCHAR(100),
    IN $storeId INT(11)
)
BEGIN

UPDATE line
SET
	`route` = $route,
	`code` = $code,
	`lineDate` = $lineDate,
    `modifiedDate` = NOW()
WHERE `id` = $id AND storeId = $storeId;


END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `updateSupplier`(
IN $supplierId varchar(15),
IN $storeId INT(11),
IN $name VARCHAR(500),
IN $email varchar(45),
IN $language VARCHAR(45),
IN $mobile INT(10),
IN $fixedLine INT(10),
IN $address VARCHAR(500),
IN $agentName VARCHAR(45),
IN $agentContact INT(10),
IN $categoryId INT(11),
IN $otherInfo VARCHAR(500),
IN $imageUrl LONGTEXT

)
BEGIN

	UPDATE 	`supplier`
    SET 	`storeId` = $storeId,
			`name` = $name,	
			`email` = $email,
			`language` = $language,
            `mobile` = $mobile,
            `fixedLine` = $fixedLine,
            `address` = $address,
            `agentName` = $agentName,
            `agentContact` = $agentContact,
            `categoryId` =  $categoryId,
            `otherInfo` = $otherInfo,
            `imageUrl` = $imageUrl,
			`updatedDate` = NOW()
    WHERE	`supplierId` = $supplierId AND storeId = $storeId;
    
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `updateSupplierActiveStatus`(

IN $supplierIds varchar(1000),
IN $isActive TINYINT(1)

)
BEGIN
	/*
    DECLARE $suppliers varchar(500);
    
    SET $suppliers = (SELECT CONCAT("'", REPLACE($supplierIds, ',', "','"), "'"));

    SET @query = CONCAT('UPDATE supplier SET isActive = ', $isActive, ', updatedDate = NOW() WHERE supplierId IN (', $suppliers, ')'); 

	PREPARE stmt FROM @query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	*/
    
    UPDATE supplier SET isActive = $isActive, updatedDate = NOW()
    WHERE  find_in_set(supplierId, $supplierIds);
    
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `updateSupplierDeleteStatus`(

IN $supplierIds varchar(1000)

)
BEGIN

    /*
    DECLARE $suppliers varchar(500);
    
    SET $suppliers = (SELECT CONCAT("'", REPLACE($supplierIds, ',', "','"), "'"));

    SET @query = CONCAT('UPDATE supplier SET isDeleted = 1, isActive = 0, updatedDate = NOW() WHERE supplierId IN (', $suppliers, ')'); 

	PREPARE stmt FROM @query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
    */
    
    UPDATE supplier SET isDeleted = 1, isActive = 0, updatedDate = NOW()
    WHERE  find_in_set(supplierId, $supplierIds);
    
END$$
DELIMITER ;
