var gulp = require('gulp'),
    pkg = require('./package.json'),
    print = require('gulp-print'),
    filter = require('gulp-filter'),
    concat = require('gulp-concat'),
    uglifyJs = require('gulp-uglify'),
    uglifyCss = require('gulp-uglifycss'),
    rename = require('gulp-rename'),
    beautify = require('gulp-beautify'),
    ngAnnotate = require('gulp-ng-annotate'),
    templates = require('gulp-angular-templatecache'),
    sass = require('gulp-sass'),
    bower = require('main-bower-files'),
    gulpCopy = require('gulp-copy');

function deployBower(destPath) {
    var bowerComponents = { base: 'bower_components' };

    gulp.src(bower(bowerComponents))
        .pipe(filter('**/*.js'))
        .pipe(print())
        .pipe(beautifyJs())
        .pipe(gulp.dest(destPath + 'js'))
        .pipe(minifyJs())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(destPath + 'js'));

    gulp.src(bower(bowerComponents)
        .concat(['bower_components/bootstrap/dist/css/*', 'bower_components/font-awesome/css/*']))
        .pipe(filter('**/*.css'))
        .pipe(print())
        .pipe(gulp.dest(destPath + 'css'))
        .pipe(minifyCss())
        .pipe(gulp.dest(destPath + 'css'));

    gulp.src(bower(bowerComponents).concat(['bower_components/bootstrap/fonts/*', 'bower_components/font-awesome/fonts/*']))
        .pipe(filter('**/*.{eot,svg,ttf,woff,woff2}'))
        .pipe(gulp.dest(destPath + 'fonts'));
}

function minifyJs() {
    return uglifyJs();
}

function minifyCss() {
    return uglifyCss();
}

function beautifyJs() {
    return beautify({
        jslint_happy: true,
        end_with_newline: true,
        keep_array_indentation: true,
        keep_function_indentation: true,
        indentSize: 2,
        indent_with_tabs: true
    });
}

function deployAngularApp(srcPath, destPath, package) {
    gulp.src(srcPath)
        .pipe(concat(package + '.js'))
        .pipe(ngAnnotate({ single_quotes: true }))
        .pipe(beautifyJs())
        .pipe(gulp.dest(destPath + 'js'))
        .pipe(minifyJs())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(destPath + 'js'));
}

function deployAngularAppTemplates(srcPath, destPath, package) {
    gulp.src(srcPath)
        .pipe(templates({
            module: package + '.templates',
            standalone: true,
            transformUrl: function (url) {
                return url.replace(/\.tpl\.html$/, '.html');
            }
        }))
        .pipe(beautifyJs())
        .pipe(gulp.dest(destPath + 'js'))
        .pipe(uglifyJs())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(destPath + 'js'));
}

function deploySass(srcPath, destPath) {
    var scss = gulp.src(srcPath)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('layout.css'))
        .pipe(gulp.dest(destPath + 'css'))
        .pipe(minifyCss())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(destPath + 'css'));
}

function copyFiles(sourcePath, destinationPath) {
    return gulp.src(sourcePath)
        .pipe(print())
        .pipe(gulp.dest(destinationPath));
}

gulp.task('deploy-bower', () => {
    deployBower('../ui/server/public/libs/');
});

gulp.task('compile', () => {
    deployAngularApp(['../ui/client/**/*.js'], '../ui/server/public/', 'project-x');
    deployAngularAppTemplates(['../ui/client/**/*.html'], '../ui/server/public/', 'project-x');
    deploySass('../ui/client/assets/styles/**/*.scss', '../ui/server/public/');
});

gulp.task('default', () => {
    var imageFolderPath = '../ui/client/assets/images';
    //build ui
    deployAngularApp(['../ui/client/**/*.js'], '../ui/server/public/', 'project-x');
    deployAngularAppTemplates(['../ui/client/**/*.html'], '../ui/server/public/', 'project-x');
    deploySass('../ui/client/assets/styles/**/layout.scss', '../ui/server/public/');
    copyFiles([imageFolderPath + '/*.jpg', imageFolderPath + '/*.gif', imageFolderPath + '/*.svg', imageFolderPath + '/*.png', imageFolderPath + '/*.ico'], '../ui/server/public/images/');
    copyFiles([imageFolderPath + '/favicons/*.*'], '../ui/server/public/favicons/')
});