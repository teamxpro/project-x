/* https://en.wikipedia.org/wiki/List_of_HTTP_status_codes */
var Logger = require('../../core/logger.js');

class HttpException {
    constructor () {
    }

    static throw(response, exception) {
        Logger.log(exception);
        return response.status(exception.code).send({message : exception.message });
    }
};

module.exports.HttpException = HttpException;

class HttpBaseException {
    constructor (statusCode, message, stack) {
        this.code = statusCode;
        this.message = message;
        this.stack = stack;
    }
};

class BadRequest extends HttpBaseException {
    constructor(message, stack){
        super(400, message, stack);
    }
};

module.exports.BadRequest = BadRequest;

class Unauthorized extends HttpBaseException {
    constructor(message, stack){
        super(401, message, stack);
    }
};

module.exports.Unauthorized = Unauthorized;

class Forbidden extends HttpBaseException {
    constructor(message, stack){
        super(403, message, stack);
    }
};

module.exports.Forbidden = Forbidden;

class InternalServerError extends HttpBaseException {
    constructor(message, stack){
        super(500, message, stack);
    }
};

module.exports.InternalServerError =  InternalServerError;
