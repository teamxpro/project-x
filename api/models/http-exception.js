/* https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
https://www.joyent.com/node-js/production/design/errors */
var Logger = require('../../core/logger.js');

function reply(response, code, exception, message, logException){
    if(logException){
        Logger.log(exception);
    }

    if(!exception){
        exception = {message : message}
    };
    
    return response.status(code).send({message : exception.message });
}

class HttpException {
    constructor () {
    }

    static badRequest(response, exception) {
        return reply(response, 400, exception, 'Bad Request', true);
    }

    static unauthorized(response, exception) {
        return reply(response, 401, exception, 'Unauthorized', true);
    }

    static forbidden(response, exception) {
        return reply(response, 403, exception, 'Forbidden', true);
    }

    static internalServerError(response, exception) {
        return reply(response, 500, exception, 'Internal Server Error', true);
    }
};

module.exports = HttpException;
