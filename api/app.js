const express = require('express'),
    app = express(),
    RouteHandler = require('./routes/route-handler'),
    Logger = require('../core/logger');

//initialize the routes
RouteHandler.init(app);

/* Error handling */
app.use((err, req, res, next) => {
    if (res.headersSent) {
        return next(err);
    }

    Logger.log(err);

    if (err instanceof HttpException) {
        return res.status(err.code).send(err.message);
    }

    return res.status(500).send('Something broke!');
});

module.exports = app;


