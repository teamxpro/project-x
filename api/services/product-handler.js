const async = require('async'),
    configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class ProductHandler {
    constructor() {

    }

    createProduct(req, res) {
        // TO DO : Add the permission
        const config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        let data = req.body;

        let product = {
            materialId: data.materialId,
            name: data.label,
            description: data.description ? data.description : null,
            amount: data.amount,
            type: data.type,
            compositeItems: []
        };

        data.compositeItems.forEach((ci) => {
            let compositeItem = {
                label: ci.label,
                isFree: ci.isFree,
                items: []
            };

            ci.items.forEach((pi) => {
                compositeItem.items.push({
                    id: pi.id,
                    quantity: pi.quantity
                });
            }, this);

            product.compositeItems.push(compositeItem);

        }, this);

        let productId, productCode, compositeItemId = null;

        let compositeInsertQuery = `INSERT INTO compositeProductItem
                (storeId, productId, label, isFree, isActive, createdBy, createdDate)
                VALUES (?,?,?,?,?,?,?); SELECT LAST_INSERT_ID() AS 'compositeItemId'`;

        let compositeItemInsertQuery = `INSERT INTO productItem
                (storeId, compositeProductItemId, itemId, quantity, isActive, createdBy, createdDate)
                VALUES (?,?,?,?,?,?,?)`;

        db.beginTransaction().then(() => {

            let productCreateParams = [req.session.store.id, product.materialId, product.name, product.description, product.amount, product.type, req.session.username];

            async.series([
                (callback) => { //create product
                    db.execute('CALL productCreate(?,?,?,?,?,?,?)', productCreateParams).then((result) => {
                        productId = result.first().productId;
                        productCode = result.first().productCode;
                        callback();
                    }).catch((err) => {
                        return callback(err);
                    });
                },
                (callback) => {
                    async.forEachSeries(product.compositeItems, (cpItem, cpItemEachCallback) => {
                        let cpItemCreatePrams = [req.session.store.id, productId, cpItem.label, cpItem.isFree, 1, req.session.username, new Date()];

                        //create compositeProductItems
                        db.query(compositeInsertQuery, cpItemCreatePrams).then((result) => {
                            compositeItemId = result[1][0].compositeItemId;

                            //create items
                            async.forEachSeries(cpItem.items, (item, itemEachCallback) => {
                                let itemCreateParams = [req.session.store.id, compositeItemId, item.id, item.quantity, 1, req.session.username, new Date()];
                                db.query(compositeItemInsertQuery, itemCreateParams).then((result) => {
                                    itemEachCallback();
                                }).catch((err) => {
                                    return itemEachCallback(err);
                                });
                            }, (err) => {
                                if (err) {
                                    return cpItemEachCallback(err);
                                } else {
                                    cpItemEachCallback();
                                }
                            });

                        }).catch((err) => {
                            return cpItemEachCallback(err);
                        });
                    }, (err) => {
                        if (err) {
                            return callback(err);
                        } else {
                            callback();
                        }
                    });
                }
            ], (err) => {
                if (err) {
                    db.rollback().catch((error) => {
                        return HttpException.internalServerError(res, error);
                    });
                    return HttpException.internalServerError(res, err);
                } else {
                    // commmit here when all the insertions have been successful
                    db.commit().then((result) => {
                        res.status(200).json({ success: true, productId: productId, productCode: productCode, message: 'Record has been saved.' });
                    }).catch((error) => {
                        return HttpException.internalServerError(res, err);
                    });
                }
                db.close();
            });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });
    }

    getProductDetails(req, res) {
        // TO DO : Add the permission
        const config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query('CALL productDetail(?,?)', [req.session.store.id, req.params.productId]).then((result) => {
            let product = result ? result[0][0] : {};
            let compositeItems = result ? result[1] : [];
            let items = result ? result[2] : [];

            if (product) {
                product.compositeItems = [];

                for (let ci of compositeItems) {
                    let compositeItem = {
                        id: ci.id,
                        label: ci.label,
                        isFree: ci.isFree,
                        items: []
                    };

                    for (let item of items) {
                        if (item.compositeProductItemId === ci.id) {
                            compositeItem.items.push({
                                id: item.id,
                                code: item.code,
                                name: item.name,
                                manufacturer: item.manufacturer,
                                model: item.model,
                                description: item.description,
                                quantity: item.quantity
                            });
                        }
                    }
                    product.compositeItems.push(compositeItem);
                }
            }
            return res.json(product);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchProducts(req, res) {
        // TO DO : Add the permission
        const config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        const searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        const pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        const index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        let query = null;

        if (searchText) {
            query = `CALL products(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL products(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchActiveProducts(req, res) {
        // TO DO : Change the below permission to the right one. It is not LINE_VIEW
        if (!PermissionManager.canExecute(Permissions.LINE_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }
        let db = new Database(configuration.read().db);
        let searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL searchActiveProducts(?,?)', [req.session.store.id, searchText]).then((result) => {
            res.status(200).json({ records: result });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}

module.exports = new ProductHandler();