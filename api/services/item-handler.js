var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class ItemHandler {
    constructor() {

    }

    createItem(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        var item = req.body;
        var params = [req.session.store.id, item.name, item.description, item.manufacturer, item.model, item.reorderLevel, item.unit, item.categoryId,
        item.subCategoryId, item.imageUrl];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL createItem(?,?,?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, itemId: result.first().$itemId, message: 'Record has been created.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    updateItem(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        var item = req.body;
        var params = [item.id, req.session.store.id, item.name, item.description, item.manufacturer, item.model, item.reorderLevel, item.unit, item.categoryId,
        item.subCategoryId, item.imageUrl];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateItem(?,?,?,?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been updated.' });

        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    deleteItem(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateItemDeleteStatus(?)', [decodeURIComponent(req.params.itemId)]).then((result) => {
            res.status(200).json({ success: true, message: 'Record deleted.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getItemByItemId(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL getItemById(?,?)', [decodeURIComponent(req.params.itemId), req.session.store.id,]).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    changeStatus(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateItemActiveStatus(?,?)', [decodeURIComponent(req.params.itemId), req.body.isActive]).then((result) => {
            res.status(200).json({ success: true, message: 'Record deactivated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchAllItems(req, res) {
        // TO DO : Add the permission
        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchAllItems(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchAllItems(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchActiveItems(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL searchActiveItems(?,?)', [req.session.store.id, searchText]).then((result) => {
            res.status(200).json({ records: result });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getRecentCreatedItemList(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);
        var size = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute(`CALL itemGetRecentList (?,?,?)`, [req.session.store.id, size, configuration.read().grid.pageSize]).then((result) => {
            res.status(200).json({ records: result });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getActiveItems(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        var params = [req.session.store.id];
        db.execute('CALL getAllItems(?)', params).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
};

module.exports = new ItemHandler();