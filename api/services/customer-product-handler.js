const configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class CustomerProductHandler {
    constructor() {

    }

    createCustomerProduct(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOM_PRODUCT_CREATE, req, res)) {
            return HttpException.forbidden(res);
        }
        const db = new Database(configuration.read().db);

        let customerProduct = req.body;

        let params = [req.session.store.id, customerProduct.productType, customerProduct.amount, customerProduct.customerId, customerProduct.productId, customerProduct.employeeId, customerProduct.lineId, customerProduct.periodFrom,
            customerProduct.periodTo, customerProduct.otherInfo, req.session.username
        ];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL createCustomerProduct(?,?,?,?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({
                success: true,
                customProductItemId: result.first().$customProductItemId,
                message: 'Record has been created.'
            });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getCustomerProductDetails(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOM_PRODUCT_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }
        const config = configuration.read().db;
        const db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let query = `SELECT
                cp.id, cp.code, cp.productId, p.name AS 'productName', cp.customerId, c.name AS 'customerName', cp.employeeId, e.name AS 'employeeName', cp.lineId, l.route AS 'lineRoute',
				            cp.otherInfo, DATE_FORMAT(periodFrom,'%Y-%m-%d') AS 'periodFrom', DATE_FORMAT(periodTo,'%Y-%m-%d') AS 'periodTo', cp.isActive
                FROM 		customerProduct cp
                            LEFT JOIN customer c ON c.id = cp.customerId
                            LEFT JOIN product p ON p.id = cp.productId
                            LEFT JOIN employee e ON e.id = cp.employeeId
                            LEFT JOIN line l ON l.id = cp.lineId
                WHERE		cp.id = ? AND cp.storeId = ?;`;

        let params = [req.params.customProductItemId, req.session.store.id];

        db.query(query, params).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getCustomProductForCustomer(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOM_PRODUCT_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }
        const config = configuration.read().db;

        const db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let params = [req.session.store.id, req.params.customerId];

        let query = `SELECT 
                        cp.id, cp.productId, cp.code, p.name,
                        CASE
                            WHEN cp.productType = 'Default' THEN p.amount
                            ELSE cp.amount
                        END AS amount, p.productType
                    FROM
                        customerProduct cp INNER JOIN product p ON cp.productId = p.id
                    WHERE
                        cp.storeId = ? AND cp.customerId = ? AND p.isDeleted = 0`;

        db.query(query, params).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getCompositeItemDeliveryStatus(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOM_PRODUCT_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }

        const config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let params = [req.session.store.id, req.params.customerProductId];

        db.query('CALL compositeItemDeliveryStatus(?, ?)', params).then((result) => {
            let compositeItems = result ? result[0] : [];
            let items = result ? result[1] : [];

            for (let ci of compositeItems) {
                ci.items = [];

                for (let item of items) {
                    if (item.compositeProductItemId === ci.compositeItemId) {
                        ci.items.push({
                            id: item.id,
                            code: item.code,
                            name: item.name,
                            manufacturer: item.manufacturer,
                            model: item.model,
                            description: item.description,
                            quantity: item.quantity
                        });
                    }
                }
            }

            res.json(compositeItems);

        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getProductItemsForProduct(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOM_PRODUCT_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }
        const config = configuration.read().db;

        const db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let params = [req.session.store.id, req.params.productId];

        let query = `SELECT pi.id, pi.productId, pi.itemId, i.code, i.name, pi.price, pi.quantity
                    FROM productItem pi inner join item i on pi.itemId = i.id
                    WHERE pi.storeId = ? AND pi.productId = ?`;

        db.query(query, params).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getCustomProductPaymentHistory(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOM_PRODUCT_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }
        const db = new Database(configuration.read().db);

        let customerProduct = req.body;

        let params = [req.session.store.id, req.params.customerProductId];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL getCustomProductPaymentHistory(?,?)', params).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    saveCustomProductToTerms(req, res) {
        if (!PermissionManager.canExecute(Permissions.ORDER_TAKE, req, res)) {
            return HttpException.forbidden(res);
        }
        const db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let params = null,
            command = req.body.command,
            query = null;

        switch (command) {
            case 'insert':
                let itemOrderedAs = req.body.isFree ? 'Free' : 'Default';

                query = 'CALL addCustomProductInTerm (?,?,?,?,?,?)';
                params = [req.session.store.id, req.body.customerProductId, req.body.compositeItemId, req.body.customerId, req.body.termId, itemOrderedAs];
                break;

            case 'remove':
                query = 'DELETE FROM itemDeliveryTracker WHERE storeId = ? AND id = ?';
                params = [req.session.store.id, req.body.trackingId];
                break;
        }

        db.query(query, params).then((result) => {
            return res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchCustomerProducts(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOM_PRODUCT_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }
        const config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        let searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        let pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        let index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        let query = null;

        if (searchText) {
            query = `CALL searchCustomerProducts(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchCustomerProducts(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({
                records: recordSet.first(),
                recordCount: recordSet[2][0]["@recordCount"]
            });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchCustomProductItems(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOM_PRODUCT_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }
        const config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        let keyword = req.query.searchText;
        if (!keyword) {
            keyword = '';
        }

        let searchText = keyword === 'null' ? '' : decodeURIComponent(keyword);

        let query = `CALL compositeItems(${req.session.store.id},'${searchText}');`;

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((results) => {
            let compositeItems = results ? results[0] : [];
            let items = results ? results[1] : [];

            for (let ci of compositeItems) {
                ci.items = [];

                for (let item of items) {
                    if (item.compositeProductItemId === ci.id) {
                        ci.items.push({
                            id: item.id,
                            code: item.code,
                            name: item.name,
                            manufacturer: item.manufacturer,
                            model: item.model,
                            description: item.description,
                            quantity: item.quantity
                        });
                    }
                }
            }

            res.json(compositeItems);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    insertOrUpdateTerms(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOM_PRODUCT_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }

        const db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let term = req.body;

        db.query('CALL insertOrUpdateTerms(?,?,?,?)', [req.session.store.id, term.customProductId, term.term, term.collectiveAmount]).then((result) => {
            return res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
};

module.exports = new CustomerProductHandler();
