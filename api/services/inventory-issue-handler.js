const async = require('async'),
    configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class InventoryIssueHandler {
    constructor() {

    }

    createInventoryIssue(req, res) {
        if (!PermissionManager.canExecute(Permissions.CREATE_INVENTORY_ISSUE, req, res)) {
            return HttpException.forbidden(res);
        }

        const db = new Database(configuration.read().db);

        let inventoryIssue = req.body;

        let inventoryIssueParams = [req.session.store.id, inventoryIssue.employeeId, inventoryIssue.issuedDate,
            inventoryIssue.otherInfo, req.session.username
        ];

        let inventoryIssueId = null;
        let inventoryIssueCode = null;

        db.beginTransaction().then(() => {

            db.execute('CALL inventoryIssue(?,?,?,?,?)', inventoryIssueParams).then((result) => {
                inventoryIssueId = result.first().inventoryIssueId;
                inventoryIssueCode = result.first().inventoryIssueCode;

                async.eachSeries(inventoryIssue.items, (inventoryIssueItem, callback) => {

                    const issuedItemParams = [inventoryIssueId, inventoryIssueItem.inventoryId, req.session.store.id, inventoryIssueItem.quantity, req.session.username];

                    db.execute('CALL inventoryItemIssue(?,?,?,?,?)', issuedItemParams).then((result) => {
                        callback();
                    }).catch((err) => {
                        return callback(err);
                    });
                }, (err) => {
                    if (err) {
                        //rollback
                        db.rollback().catch((error) => {
                            return HttpException.internalServerError(res, error);
                        });
                        return HttpException.internalServerError(res, err);
                    } else {
                        // commmit here when all the insertions have been successful
                        db.commit().then((result) => {
                            res.status(200).json({
                                success: true,
                                inventoryIssueId: inventoryIssueId,
                                inventoryIssueCode: inventoryIssueCode,
                                message: 'Record has been saved.'
                            });
                        }).catch((error) => {
                            return HttpException.internalServerError(res, err);
                        });
                    }
                });
            }).catch((err) => {
                db.close();
                return HttpException.internalServerError(res, err);
            });
        }).catch((err) => {
            db.close();
            return HttpException.internalServerError(res, err);
        });
    }

    getInventoryIssueDetails(req, res) {
        if (!PermissionManager.canExecute(Permissions.VIEW_INVENTORY_ISSUE_DETAILS, req, res)) {
            return HttpException.forbidden(res);
        }

        let config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        let params = [req.session.store.id, req.params.inventoryIssueId, req.session.store.id, req.params.inventoryIssueId];

        let inventoryIssue = {
            inventoryIssueData: null,
            inventoryIssueItems: []
        }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(`CALL inventroyIssueDetails(?,?); CALL inventoryItemIssueDetails(?,?)`, params).then((result) => {
            inventoryIssue.inventoryIssueData = result[0] ? result[0] : null;
            inventoryIssue.inventoryIssueItems = result[2] ? result[2] : [];
            res.send(inventoryIssue);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    updateItemTrackingStatus(req, res) {
        if (!PermissionManager.canExecute(Permissions.UPDATE_ITEM_TRACKING_STATUS, req, res)) {
            return HttpException.forbidden(res);
        }

        const db = new Database(configuration.read().db);

        let itemDeliveries = req.body;

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let updateQuery = `
        UPDATE	itemDeliveryTracker
        SET		itemOrderedAs = ?, status = ?, updatedDate = ?, updatedBy = ?
        WHERE	storeId = ? AND customerProductId = ? AND term = ?`;

        let insertCollectiveQuery = `
        INSERT INTO collection
        (storeId, deliveryTrackingId ,customerProductId, term, collectedAmount, collectedDate)
        VALUES (?,?,?,?,?,?)`;

        async.forEach(itemDeliveries, (item, callback) => {
            db.query(updateQuery, [item.itemOrderedAs, item.orderStatus, new Date(), req.session.username, req.session.store.id, item.customerProductId, item.term]).then((result) => {
                callback();
            }).catch((err) => {
                return callback(err);
            });

            if (item.collectedAmount && item.collectedAmount > 0) {
                let params = [req.session.store.id, item.id, item.customerProductId, item.term, item.collectedAmount, new Date()];
                if (item.term && item.customerProductId) {
                    params = [req.session.store.id, null, item.customerProductId, item.term, item.collectedAmount, new Date()];
                }
                db.query(insertCollectiveQuery, params).then((result) => {
                    callback();
                }).catch((err) => {
                    return callback(err);
                });
            }
        }, (err) => {
            if (err) return HttpException.internalServerError(req, err);
            res.status(200).json({
                success: true,
                message: 'Records have been updated.'
            });
        });

        db.close();
    }

    saveLoadingSheet(req, res) {
        if (!PermissionManager.canExecute(Permissions.SAVE_LOADING_SHEET, req, res)) {
            return HttpException.forbidden(res);
        }

        const db = new Database(configuration.read().db);

        let loadingSheet = req.body;

        let loadingSheetParams = [req.session.store.id, loadingSheet.employeeId, loadingSheet.lineId, loadingSheet.loadingDate, loadingSheet.vehicleNo, loadingSheet.otherInfo, req.session.username];

        let loadingSheetId = null;
        let loadingSheetCode = null;

        let updateQuery = `
        UPDATE	itemDeliveryTracker
        SET		itemOrderedAs = ?, status = ?, updatedDate = ?, updatedBy = ?, loadingSheetId = ?
        WHERE	storeId = ? AND customerProductId = ? AND term = ?`;

        let insertQuery = `
        INSERT INTO itemDeliveryTracker
        (storeId, status, loadingSheetId, customerId, itemId, quantity, itemOrderedAs, createdDate, createdBy)
        VALUES (?,?,?,?,?,?,?,?,?)`;

        db.beginTransaction().then(() => {
            async.series([
                //create loading sheet entry
                (callback) => {
                    db.execute('CALL createLoadingSheet(?,?,?,?,?,?,?)', loadingSheetParams).then((result) => {
                        loadingSheetId = result.first().loadingSheetId;
                        loadingSheetCode = result.first().loadingSheetCode;
                        callback();
                    }).catch((err) => {
                        return callback(err);
                    });
                },
                //add loading Items
                (callback) => {
                    async.forEach(loadingSheet.items, function (item, eachCallback) {
                        if (item.id) {
                            db.query(updateQuery, [item.itemOrderedAs, item.orderStatus, new Date(), req.session.username, loadingSheetId, req.session.store.id, item.customerProductId, item.term]).then((result) => {
                                eachCallback();
                            }).catch((err) => {
                                return eachCallback(err);
                            });
                        } else {
                            db.query(insertQuery, [req.session.store.id, item.orderStatus, loadingSheetId, item.customerId, item.itemId, item.quantity, item.itemOrderedAs, new Date(), req.session.username]).then((result) => {
                                eachCallback();
                            }).catch((err) => {
                                return eachCallback(err);
                            });
                        }
                    }, (err) => {
                        if (err) {
                            return callback(err);
                        } else {
                            callback();
                        }
                    });
                }
            ], (err) => { //This function gets called after all the callbacks called
                if (err) {
                    //rollback
                    db.rollback().catch((error) => {
                        return HttpException.internalServerError(res, error);
                    });
                    return HttpException.internalServerError(res, err);
                } else {
                    // commmit here when all the insertions have been successful
                    db.commit().then((result) => {
                        res.status(200).json({
                            success: true,
                            loadingSheetId: loadingSheetId,
                            loadingSheetCode: loadingSheetCode,
                            message: 'Record has been saved.'
                        });
                    }).catch((error) => {
                        return HttpException.internalServerError(res, err);
                    });
                }
                db.close();
            });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });
    }

    getItemListByTackingStatus(req, res) {
        if (!PermissionManager.canExecute(Permissions.VIEW_LOADING_SHEET_ITEMS, req, res)) {
            return HttpException.forbidden(res);
        }

        // TO DO : Add the permission
        const db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let lineId = (req.query.lineId === 'null' || req.query.lineId === 'undefined') ? null : req.query.lineId;
        let loadingSheetId = (req.query.loadingSheetId === 'null' || req.query.loadingSheetId === 'undefined') ? null : req.query.loadingSheetId;

        db.execute('CALL itemTrackingListByStatus(?,?,?,?)', [req.session.store.id, req.params.status, lineId, loadingSheetId]).then((result) => {

            let trackingItem = null;
            let trackingItems = [];
            let termItem = null;
            let termItems = [];
            let itemList = [];

            let itemLabel = '';

            result.forEach((element, index) => {
                itemList.push({
                    itemId: element.itemId,
                    itemCode: element.itemCode,
                    itemName: element.itemName,
                    quantity: element.quantity
                });

                if (!result[index + 1] || result[index].id !== result[index + 1].id) {
                    itemLabel = `${itemLabel} ${element.compositeItemLabel}`;
                }

                if (!result[index + 1] || result[index].customerId !== result[index + 1].customerId || result[index].term !== result[index + 1].term) {

                    trackingItem = {
                        id: element.id,
                        customerId: element.customerId,
                        customerName: element.customerName,
                        customerProductId: element.customerProductId,
                        term: element.term,
                        itemOrderedAs: element.itemOrderedAs,
                        customProductPrice: element.customProductPrice,
                        collectiveAmount: element.collectiveAmount,
                        collectedAmount: element.collectedAmount === 0 ? element.collectiveAmount : element.collectedAmount,
                        dueAmount: element.dueAmount,
                        compositeItemLabel: itemLabel,
                        status: element.status,
                        isSelected: element.isSelected,
                        items: itemList
                    };

                    trackingItems.push(trackingItem);
                    trackingItem = null;
                    itemLabel = '';
                    itemList = [];
                }
            });
            res.send(trackingItems);

        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchInventoryIssues(req, res) {
        if (!PermissionManager.canExecute(Permissions.SEARCH_INVENTORY, req, res)) {
            return HttpException.forbidden(res);
        }

        let config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        let searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        let pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        let index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        let query = null;

        if (searchText) {
            query = `CALL inventoryIssueSearch(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL inventoryIssueSearch(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({
                records: recordSet.first(),
                recordCount: recordSet[2][0]["@recordCount"]
            });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getAggregatedItemsForCompItems(req, res) {
        if (!PermissionManager.canExecute(Permissions.GET_AGG_ITEMS_FOR_COMP_ITEMS, req, res)) {
            return HttpException.forbidden(res);
        }

        const db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let lineId = (req.query.lineId === 'null' || req.query.lineId === 'undefined') ? null : decodeURIComponent(req.query.lineId);

        db.execute('CALL getAggregatedItemsForCompositeItems(?,?,?)', [req.session.store.id, req.params.status, lineId]).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getLoadingSheetsForLineorLoadingDate(req, res) {
        if (!PermissionManager.canExecute(Permissions.GET_LOADING_SHEET_FOR_LINE, req, res)) {
            return HttpException.forbidden(res);
        }

        const db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        const lineId = (req.query.lineId === 'null' || req.query.lineId === 'undefined') ? null : decodeURIComponent(req.query.lineId);
        const loadingDate = (req.query.loadingDate === 'null' || req.query.loadingDate === 'undefined') ? null : decodeURIComponent(req.query.loadingDate);

        db.execute('CALL getLoadingSheetsForLineorLoadingDate(?,?,?)', [req.session.store.id, loadingDate, lineId]).then((result) => {
            res.status(200).json({
                records: result
            });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
};

module.exports = new InventoryIssueHandler();
