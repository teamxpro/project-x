var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class SupplierHandler {
    constructor() {

    }

    createSupplier(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        var supplier = req.body;
        var params = [req.session.store.id, supplier.name, supplier.email, supplier.language, supplier.mobile, supplier.fixedLine,
        supplier.address, supplier.agentName, supplier.agentContact, supplier.categoryId, supplier.otherInfo, supplier.imageUrl];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL createSupplier(?,?,?,?,?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, supplierId: result.first().$supplierId, message: 'Record has been created.' });

        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getSupplierBySupplierId(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL getSupplierBySupplierId(?,?)', [decodeURIComponent(req.params.supplierId), req.session.store.id]).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    deactivateSupplierById(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateSupplierActiveStatus(?,?)', [decodeURIComponent(req.params.supplierId), req.body.isActive]).then((result) => {
            res.status(200).json({ success: true, message: 'Record deactivated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    deleteSupplierById(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateSupplierDeleteStatus(?)', [decodeURIComponent(req.params.supplierId)]).then((result) => {
            res.status(200).json({ success: true, message: 'Record deleted.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    updateSupplier(req, res) {
        // TO DO : Add the permission
        var db = new Database(configuration.read().db);

        var supplier = req.body;
        var params = [supplier.supplierId, req.session.store.id, supplier.name, supplier.email, supplier.language, supplier.mobile, supplier.fixedLine,
        supplier.address, supplier.agentName, supplier.agentContact, supplier.categoryId, supplier.otherInfo, supplier.imageUrl];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateSupplier(?,?,?,?,?,?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been updated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchAllSuppliers(req, res) {
        // TO DO : Add the permission
        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchAllSuppliers(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchAllSuppliers(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getAllSuppliers(req, res) {
        // TO DO : Add the permission
        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);
        var query = `CALL getAllSuppliers(${req.session.store.id})`;

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}

module.exports = new SupplierHandler();