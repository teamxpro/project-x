var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class EmployeeHandler {
    constructor() {

    }

    createEmployee(req, res) {
        if (!PermissionManager.canExecute(Permissions.EMPLOYEE_ADD, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        var employee = req.body;
        var params = [req.session.username, employee.employeeId, req.session.store.id, employee.name, employee.nic, employee.email, employee.address, employee.mobile1, employee.mobile2,
        employee.fixedLine, employee.emergencyContact, employee.contactName, employee.otherInfo];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL createEmployee(?,?,?,?,?,?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, id: result.first().$id, message: 'Record has been created.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getEmployeeByEmployeeId(req, res) {
        if (!PermissionManager.canExecute(Permissions.EMPLOYEE_VIEW_DETAILS, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL getEmployeeByEmployeeId(?, ?)', [decodeURIComponent(req.params.employeeId), req.session.store.id]).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    deleteEmployeesByIds(req, res) {
        if (!PermissionManager.canExecute(Permissions.EMPLOYEE_DELETE, req, res)) {
            return HttpException.forbidden(res);
        }
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateEmployeeDeleteStatus(?, ?, ?)', [decodeURIComponent(req.params.employeeIds), req.session.username, req.session.store.id]).then((result) => {
            res.status(200).json({ success: true, message: 'Record deleted.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    updateEmployee(req, res) {
        if (!PermissionManager.canExecute(Permissions.EMPLOYEE_EDIT, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        var employee = req.body;
        var params = [employee.id, req.session.username, employee.employeeId, req.session.store.id, employee.name, employee.nic, employee.email, employee.address, employee.mobile1, employee.mobile2,
        employee.fixedLine, employee.emergencyContact, employee.contactName, employee.otherInfo];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateEmployee(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been updated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchAllEmployees(req, res) {
        if (!PermissionManager.canExecute(Permissions.EMPLOYEE_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchAllEmployees(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchAllEmployees(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getRecentCreatedList(req, res) {
        if (!PermissionManager.canExecute(Permissions.EMPLOYEE_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);
        var size = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute(`CALL customerGetRecentList (?,?,?)`, [req.session.store.id, size, configuration.read().grid.pageSize]).then((result) => {
            res.status(200).json({ records: result });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchSalesReps(req, res) {
        if (!PermissionManager.canExecute(Permissions.EMPLOYEE_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }
        var config = configuration.read().db;

        var db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);

        let query = `   SELECT 		id, employeeId, name
                        FROM  		employee
                        WHERE		isActive = 1 AND storeId = ${req.session.store.id} 
                                    AND (${req.query.searchText} IS NULL OR ${req.query.searchText} = '' OR name LIKE CONCAT(${req.query.searchText}, '%'))
                        ORDER BY 	name ASC;`;

        db.query(query).then((result) => {
            res.status(200).json({ records: result });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    lookup(req, res) {
        if (!PermissionManager.canExecute(Permissions.EMPLOYEE_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchAllCustomers(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchAllCustomers(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            var records = recordSet.first().map(function (c) {
                return {
                    id: c.id,
                    text: c.name
                };
            });

            res.status(200).json({ records: records, recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}

module.exports = new EmployeeHandler();