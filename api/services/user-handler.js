let configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    HashString = require('../../core/hashstring.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class UserHandler {
    constructor() {

    }

    create(req,res){
        // TO DO : Add the permission
        const config = configuration.read().db;

        let db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let query = `CALL createUser(?,?,?,?,?)`;
        
        var hash = new HashString(),
            salt = hash.generateSalt(),
            hashedPassword = hash.createHash(req.body.password, salt);

        db.execute(query, [req.body.store, req.body.username, hashedPassword, salt, req.body.roleId]).then((recordSet) => {
            res.status(200).json(recordSet);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    changePassword(req, res) {
        const config = configuration.read().db;

        const db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let userDetails = req.body;

        db.query('SELECT * FROM user WHERE id = ?', [userDetails.userId]).then((recordSet) => {
            let user = recordSet.first();
            let hash = new HashString();

            if (hash.validateHash(user.password, user.salt, userDetails.currentPassword)) {
                const hashedPassword = hash.createHash(userDetails.newPassword, user.salt);
                db.query('UPDATE user SET password = ? WHERE id = ?', [hashedPassword, userDetails.userId]).then((user) => {
                    res.json({
                        success: true,
                        message: 'Password has been updated.'
                    });
                }).catch((err) => {
                    return HttpException.internalServerError(res, err);
                });
                db.close();
            } else {
                db.close();
                res.json({
                    success: false,
                    message: 'Please enter correct password.'
                });
            }
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });
    }

    getPermissions(req, res) {
        // TO DO : Add the permission
        const config = configuration.read().db;

        let db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let query = `SELECT r.name AS 'role', GROUP_CONCAT(p.code) AS 'permissions' 
            FROM user u LEFT JOIN role r 
            ON u.roleId = r.id LEFT JOIN permission p ON p.roleId = u.roleId 
            WHERE u.storeId = ? AND u.username = ?`;

        db.query(query, [req.session.store.id, req.session.username]).then((recordSet) => {
            let result = recordSet.first();

            let userPermissions = [];

            if (result.role === 'administrator') {
                //override permissions
                for (let permission of Object.keys(Permissions)) {
                    userPermissions.push(Permissions[permission]);
                }
            } else {
                userPermissions = result.permissions ? result.permissions.split(',') : []
            }

            let userPermission = {
                role: result.role,
                permissions: userPermissions
            };

            res.status(200).json(userPermission);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}

module.exports = new UserHandler();
