var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class LookupHandler {
    constructor() {

    }

    createLookupCategory(req, res) {
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        if (typeof req.body.name === 'undefined' || req.body.name === '') {
            return HttpException.badRequest(res, { message: 'Name is required' });
        }

        var params = [req.session.store.id, req.body.name, req.body.name];

        db.query('INSERT INTO lookupCategory (storeId, code, name) VALUES (?, ?, ?);', params).then((result) => {
            res.status(200).json({ id: result.insertId, storeId: req.session.store.id, code: req.body.name, name: req.body.name });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    createLookup(req, res) {
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        if (typeof req.body.name === 'undefined' || req.body.name === '') {
            return HttpException.badRequest(res, { message: 'Name is required' });
        }

        var params = [req.session.store.id, req.body.name, req.body.parentId, req.body.categoryId];

        db.query('INSERT INTO lookup (storeId, name, parentId, categoryId) VALUES (?, ?, ?, ?);', params).then((result) => {
            res.status(200).json({ id: result.insertId, storeId: req.session.store.id, categoryId: req.body.categoryId, parentId: req.body.parentId });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getLookupCategory(req, res) {
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        var params = [req.session.store.id];

        db.query('SELECT id,storeId,code,name FROM lookupCategory WHERE storeId = ?', params).then((result) => {
            res.status(200).json(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getLookups(req, res) {
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        var query = `SELECT lo.id,
                lo.storeId,
                lo.name,
                lo.parentId,
                loc.id AS categoryId
            FROM lookup lo JOIN lookupCategory loc ON loc.id = lo.categoryId
            WHERE lo.storeId = ? AND loc.id= ? AND lo.isActive = 1;`;

        var params = [req.session.store.id, req.params.categoryId];

        db.query(query, params).then((result) => {
            res.status(200).json(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getLookupsJson(req, res) {
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL getLookups(?)', [req.params.store]).then((result) => {
            res.header("Content-Type", "application/json");
            var lookups = 'var lookups = ' + JSON.stringify(result);
            res.send(lookups);
        }).catch((err) => {
            res.header("Content-Type", "application/json");
            var lookupsError = 'var lookups = undefined';
            res.send(lookupsError);
        });

        db.close();
    }

    getInventoryItems(req, res) {
        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;
        
        if(searchText){
            query = `CALL getInventoryItemsLookup(${req.session.store.id}, '${searchText}', ${index}, ${pageSize},@recordCount);SELECT @recordCount`;
        }else{
            query = `CALL getInventoryItemsLookup(${req.session.store.id}, null,${index}, ${pageSize}, @recordCount);SELECT @recordCount`;
        }

        db.query(query).then((recordSet) => {
            res.send({ 
                records: recordSet.first(), 
                recordCount: recordSet[2][0]["@recordCount"] 
            });

        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}

module.exports = new LookupHandler();