const async = require('async'),
    configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class CashCollectionHandler {
    constructor() {

    }

    getCashCollectionList(req, res) {
        let config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        let params = [req.session.store.id, req.query.startDate, req.query.endDate];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(`CALL getCashCollectionList(?,?,?);`, params).then((result) => {
            res.send(result[0]);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
};

module.exports = new CashCollectionHandler();