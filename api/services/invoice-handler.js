const async = require('async'),
    configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class InvoiceHandler {
    constructor() {

    }

    createInvoice(req, res) {
        if (!PermissionManager.canExecute(Permissions.INVOICE_ADD, req, res)) {
            return HttpException.forbidden(res);
        }

        const db = new Database(configuration.read().db);

        let invoice = req.body;

        const invoiceParams = [req.session.store.id, invoice.customerId, invoice.otherInfo, invoice.salesTypeId, invoice.paymentMethodId, invoice.invoicedDate, invoice.dueDate,
        invoice.chequeNo, invoice.bank, invoice.branch, invoice.total, invoice.discountAmount, invoice.netAmount, invoice.receivedAmount, invoice.balanceAmount, invoice.dueAmount, req.session.username];

        let invoiceId = null;
        let invoiceCode = null;

        db.beginTransaction().then(function () {

            db.execute('CALL invoiceCreate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', invoiceParams).then((result) => {
                invoiceId = result.first().invoiceId;
                invoiceCode = result.first().invoiceCode;

                async.eachSeries(invoice.items, function iterate(invoiceItem, callback) {

                    const invoiceItemParams = [invoiceId, invoiceItem.inventoryId, req.session.store.id, invoiceItem.price,
                        invoiceItem.quantity, invoiceItem.subTotal, req.session.username];

                    db.execute('CALL invoiceItemCreate(?,?,?,?,?,?,?)', invoiceItemParams).then((result) => {
                        callback();
                    }).catch((err) => {
                        return callback(err);
                    });
                }, function done(err) {
                    if (err) {
                        //rollback
                        db.rollback().catch((error) => {
                            return HttpException.internalServerError(res, error);
                        });
                        return HttpException.internalServerError(res, err);
                    } else {
                        // commmit here when all the insertions have been successful
                        db.commit().then((result) => {
                            res.status(200).json({ success: true, invoiceId: invoiceId, invoiceCode: invoiceCode, message: 'Record has been saved.' });
                        }).catch((error) => {
                            return HttpException.internalServerError(res, err);
                        });
                    }
                });
            }).catch((err) => {
                db.close();
                return HttpException.internalServerError(res, err);
            });
        }).catch((err) => {
            db.close();
            return HttpException.internalServerError(res, err);
        });
    }

    getInvoiceDetails(req, res) {
        if (!PermissionManager.canExecute(Permissions.INVOICE_VIEW_DETAILS, req, res)) {
            return HttpException.forbidden(res);
        }

        const config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        const params = [req.session.store.id, req.params.invoiceId];

        let invoice = { invoiceData: null, invoiceItems: [] }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(`CALL invoiceDetails(${req.session.store.id},${decodeURIComponent(req.params.invoiceId)}); CALL invoiceItems(${req.session.store.id},${decodeURIComponent(req.params.invoiceId)})`).then((result) => {
            invoice.invoiceData = result[0] ? result[0] : null;
            invoice.invoiceItems = result[2] ? result[2] : [];
            res.send(invoice);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    updateInvoiceDeleteStatus(req, res) {
        // TO DO : Check the below permission, sometimes can add but cannot edit. If so, we need a new type
        if (!PermissionManager.canExecute(Permissions.INVOICE_ADD, req, res)) {
            return HttpException.forbidden(res);
        }

        const db = new Database(configuration.read().db);

        const params = [req.session.store.id, decodeURIComponent(req.params.invoiceId), req.session.username];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL invoiceDelete(?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been updated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getPaymentDueInvoices(req, res) {
        // TO DO : Add the permission
        const config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        let searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        let pageSize = req.query.size === 'undefined' ? null : req.query.size;
        let index = req.query.index === 'undefined' ? null : req.query.index;

        let query = null;

        if (searchText) {
            query = `CALL invoicePaymentDue(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL invoicePaymentDue(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchInvoices(req, res) {
        // TO DO : Add the permission
        const config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        const db = new Database(config);

        let searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        let pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        let index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        let query = null;

        if (searchText) {
            query = `CALL invoiceSearch(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL invoiceSearch(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}

module.exports = new InvoiceHandler();